<?php
/********************************************
 *
 * Arithmetic.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Helpers\Math;

/**
 * Class Arithmetic
 * @package PKRS\Helpers\Math
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Arithmetic
{

    /**
     * @param \SplFloat $number
     * @param \SplInt $precision
     * @param \SplInt $decimals
     * @param \SplString $dec_point
     * @param \SplString $thousands_sep
     * @return string
     */
    public static function getFormattedAndRounded(\SplFloat $number, \SplInt $precision = 2, \SplInt $decimals = 2, \SplString $dec_point = ".", \SplString $thousands_sep = " ")
    {
        return number_format(round($number, $precision), $decimals, $dec_point, $thousands_sep);
    }

    /**
     * @param \SplString $formattedNumber
     * @param \SplString $decPoint
     * @param \SplString $thousandsSeparator
     */
    public static function getFromFormatted(\SplString $formattedNumber, \SplString $decPoint = ".", \SplString $thousandsSeparator = " ")
    {
        $e = explode($decPoint, $formattedNumber);
        $e[0] = str_replace($thousandsSeparator, "", $e[0]);
    }

    /**
     * @param $val
     * @return float
     */
    public static function getFloatVal($val)
    {
        return (float)str_replace([",","."],".",str_replace(" ","",$val));
    }
}