<?php
/**************************************************************
 *
 * Strings.php, created 4.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Helpers\Transform;

use PKRS\Core\Service\Service;

/**
 * Class Strings
 * @package PKRS\Helpers\Transform
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Strings extends Service
{

    /**
     * @param $string
     * @return mixed
     */
    public function getUrlFromString($string)
    {
        return preg_replace('~[^\\pL0-9_]+~u', '-', $string);
    }

}