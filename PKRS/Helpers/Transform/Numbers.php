<?php
/**************************************************************
 *
 * Numbers.php, created 4.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Helpers\Transform;

use PKRS\Core\Service\Service;

/**
 * Class Numbers
 * @package PKRS\Helpers\Transform
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Numbers extends Service
{

    /**
     * @param $val
     * @param $min
     * @param $max
     * @return mixed
     */
    function limitBetween($val, $min, $max)
    {
        if ($val < $min) $val = $min;
        if ($val > $max) $val = $max;
        return $val;
    }

    /**
     * @param $number
     * @return string
     */
    function numberFormat($number)
    {
        return number_format($number, 2, ".", " ");
    }

    /**
     * @param $bytes
     * @param int $decimals
     * @return string
     */
    function humanFileSize($bytes, $decimals = 2)
    {
        $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . " " . @$size[$factor];
    }

}