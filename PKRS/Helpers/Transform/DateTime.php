<?php
/**************************************************************
 *
 * DateTime.php, created 4.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Helpers\Transform;

use PKRS\Core\Service\Service;

/**
 * Class DateTime
 * @package PKRS\Helpers\Transform
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class DateTime extends Service
{

    /**
     * @param $dateString
     * @param string $dateFormat
     * @return int
     */
    function toUnixTimestamp($dateString, $dateFormat = "d.m.Y H:i:s")
    {
        $time = \DateTime::createFromFormat($dateFormat, $dateString);
        $ch = $time instanceof \DateTime ? true : false;
        if ($ch) {
            return $time->getTimestamp();
        } else return time();
    }

    /**
     * @param $unixTimestamp
     * @param bool|false $onlyDate
     * @return bool|string
     */
    function toMysqlTimestamp($unixTimestamp, $onlyDate = false)
    {
        return date("Y-m-d " . ($onlyDate ? "00:00:00" : "H:i:s"), $unixTimestamp);
    }

    /**
     * @param $dateString
     * @param string $dateFormat
     * @param bool|false $onlyDate
     * @return bool|string
     */
    function toMysqlTimestampFromDate($dateString, $dateFormat = "d.m.Y H:i:s", $onlyDate = false)
    {
        return $this->toMysqlTimestamp($this->toUnixTimestamp($dateString, $dateFormat), $onlyDate);
    }

    /**
     * @param $mysqlTimestamp
     * @return int
     */
    function toUnixTimestampFromMysql($mysqlTimestamp)
    {
        return $this->toUnixTimestamp($mysqlTimestamp, "Y-m-d H:i:s");
    }

    /**
     * @param $seconds
     * @return string
     */
    function toHhMmSsFromSeconds($seconds)
    {
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;
        if ($hours < 10) $hours = "0" . $hours;
        if ($minutes < 10) $minutes = "0" . $minutes;
        if ($seconds < 10) $seconds = "0" . $seconds;
        return $hours . ":" . $minutes . ":" . $seconds;
    }

    /**
     * @param $timestamp
     * @param string $now
     * @param string $second
     * @param string $minute
     * @param string $hour
     * @param string $day
     * @param string $week
     * @param string $month
     * @param string $year
     * @return string
     */
    function toReadableTimeString($timestamp, $now = "Just now", $second = "second", $minute = "minute", $hour = "hour", $day = "day", $week = "week", $month = "month", $year = "year")
    {
        $diff = $timestamp;

        $intervals = [
            'year' => 31556926, 'month' => 2629744, 'week' => 604800, 'day' => 86400, 'hour' => 3600, 'minute' => 60
        ];

        if ($diff == 0) {
            return ' ' . $now . ' ';
        }

        if ($diff < 60) {
            return $diff == 1 ? $diff . ' ' . $second . ' ' : $diff . ' ' . $second . 's ';
        }

        if ($diff >= 60 && $diff < $intervals['hour']) {
            $diff = floor($diff / $intervals['minute']);
            return $diff == 1 ? $diff . ' ' . $minute . ' ' : $diff . ' ' . $minute . 's ';
        }

        if ($diff >= $intervals['hour'] && $diff < $intervals['day']) {
            $diff = floor($diff / $intervals['hour']);
            return $diff == 1 ? $diff . ' ' . $hour . ' ' : $diff . ' ' . $hour . 's ';
        }

        if ($diff >= $intervals['day'] && $diff < $intervals['week']) {
            $diff = floor($diff / $intervals['day']);
            return $diff == 1 ? $diff . ' ' . $day . ' ' : $diff . ' ' . $day . 's ';
        }

        if ($diff >= $intervals['week'] && $diff < $intervals['month']) {
            $diff = floor($diff / $intervals['week']);
            return $diff == 1 ? $diff . ' ' . $week . ' ' : $diff . ' ' . $week . 's ';
        }

        if ($diff >= $intervals['month'] && $diff < $intervals['year']) {
            $diff = floor($diff / $intervals['month']);
            return $diff == 1 ? $diff . ' ' . $month . ' ' : $diff . ' ' . $month . 's ';
        }

        if ($diff >= $intervals['year']) {
            $diff = floor($diff / $intervals['year']);
            return $diff == 1 ? $diff . ' ' . $year . ' ' : $diff . ' ' . $now . 's ';
        }
    }

}