<?php
/**
 * Created by PhpStorm.
 * User: pitrr
 * Date: 16.12.2015
 * Time: 22:54
 */

namespace PKRS\Helpers\Transform;


class Spintax
{
    public function process($text)
    {
        return preg_replace_callback(
            '/\{(((?>[^\{\}]+)|(?R))*)\}/x',
            array($this, 'replace'),
            $text
        );
    }
    public function replace($text)
    {
        $text = $this->process($text[1]);
        $parts = explode('|', $text);
        return $parts[array_rand($parts)];
    }
}