<?php
/**************************************************************
 *
 * Trees.php, created 13.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Helpers\Transform;

use PKRS\Core\Service\Service;

/**
 * Class Trees
 * @package PKRS\Helpers\Transform
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Trees extends Service{


    /**
     * @param $array
     * @param string $idKey
     * @param string $parentIdKey
     * @param null $nowParent
     * @param int $level
     * @return array
     */
    function getTreeByParentID($array, $idKey="item_id", $parentIdKey="parent_id", $nowParent = null, $level = 1){
        $return = [];
        foreach ($array as $item){
            if($item[ $parentIdKey ]==$nowParent || $nowParent === null){
                if ($nowParent === null) $nowParent = $item[$parentIdKey];
                $return[ $item[$idKey] ] = $item;
                $return[ $item[$idKey] ]["_level"] = $level;
                $return[$item[$idKey]]["_children"]= $this->getTreeByParentID($array,$idKey,$parentIdKey,$item[$idKey], $level+1);
            }
        }
        return $return;
    }

    /**
     * @param $array
     * @param string $showItemSub
     * @param string $childrenItem
     * @param string $startTag
     * @param string $endTag
     * @param string $subTagStart
     * @param string $subTagEnd
     * @param string $itemBefore
     * @param string $itemAfter
     * @return string
     */
    function getHtmlTreeByChildren($array, $showItemSub = "name", $childrenItem= "_children", $startTag = "<ul>", $endTag = "</ul>", $subTagStart = "<li>", $subTagEnd = "</li>", $itemBefore = "", $itemAfter = "" ){
        $return = $startTag;
        foreach($array as $item){
            if (is_string($item[$showItemSub]))
                $return .= $subTagStart.$itemBefore.$item[$showItemSub].$itemAfter;
            elseif (is_array($item[$showItemSub])){
                $return .= $subTagStart;
                foreach($item[$showItemSub] as $i){
                    $return .= $itemBefore.$i.$itemAfter;
                }
            }
            if (isset($item[$childrenItem]) && is_array($item[$childrenItem]))
                $return .= $this->getHtmlTreeByChildren($item[$childrenItem], $showItemSub, $childrenItem, $startTag, $endTag, $subTagStart, $subTagEnd, $itemBefore, $itemAfter);
            $return .= $subTagEnd;
        }
        return $return.$endTag;
    }

    /**
     * @param $tree
     * @param string $childrenArrayKey
     * @return array
     */
    function getNormalArrayFromTree($tree, $childrenArrayKey = "_children"){
        $return = [];
        foreach($tree as $item){
            $childs = null;
            if (isset($item[$childrenArrayKey])){
                $childs = $item[$childrenArrayKey];
                unset($item[$childrenArrayKey]);
            }
            $return[] = $item;
            if (is_array($childs)) $return = array_merge($return, $this->getNormalArrayFromTree($childs, $childrenArrayKey));
        }
        return $return;
    }

}

