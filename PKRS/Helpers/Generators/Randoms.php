<?php
/**************************************************************
 *
 * Randoms.php, created 13.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Helpers\Generators;

use PKRS\Core\Service\Service;

/**
 * Class Randoms
 * @package PKRS\Helpers\Generators
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Randoms extends Service{

    /**
     * Get random getNumber by start and end
     *
     * @param $start
     * @param $end
     * @return int
     */
    function getNumber($start, $end){
        return mt_rand($start, $end);
    }


    /**
     * Get random getNumber by length
     *
     * @param $len
     * @return int
     */
    function getNumberByLength($len){
        $start = "1".str_pad("",$len-1,"0");
        $end   = "9".str_pad("",$len-1,"9");
        return $this->getNumber($start, $end);
    }

    /**
     * Get random string by length
     *
     * @param $stringLength
     * @param string $extraChars
     * @return string
     */
    function getStringByLength($stringLength, $extraChars = ""){
        $chars =  implode(
            "",
            array_unique(
                str_split(
                    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'.trim($extraChars)
                )
            )
        );
        $str = '';
        $max = strlen($chars) - 1;
        for ($i=0; $i < $stringLength; $i++)
            $str .= $chars[mt_rand(0, $max)];
        return $str;
    }

}