<?php
/**************************************************************
 *
 * Validator.php, created 20.10.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Helpers\Validator;

use PKRS\Core\Service\Service;

/**
 * Class Validator
 * @package PKRS\Helpers\Validator
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Validator extends Service
{

    /**
     * @param $input
     * @param string $dateformat
     * @return bool
     */
    function isDateFormat($input, $dateformat = "d.m.Y H:i:s")
    {
        $time = \DateTime::createFromFormat($dateformat, $input);
        return $time instanceof \DateTime ? true : false;
    }

    /**
     * @param $str
     * @return bool
     */
    function isEmptyStr($str)
    {
        return trim($str) == "";
    }

    /**
     * @param $_POST_key
     * @return bool
     */
    function isEmpty_POST($_POST_key)
    {
        return !isset($_POST[$_POST_key]) || trim($_POST[$_POST_key]) == "";
    }

    /**
     * @param $email
     * @return bool
     */
    function isEmail($email)
    {
        return (bool)filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param $mysqlTimestamp
     * @return bool
     */
    function isMysqlTimestamp($mysqlTimestamp){
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $mysqlTimestamp, $matches)) {
            if (checkdate($matches[2], $matches[3], $matches[1])) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $mysqlTimestamp
     * @return bool
     */
    function isMysqlDate($mysqlDate){
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $mysqlDate, $matches)) {
            if (checkdate($matches[2], $matches[3], $matches[1])) {
                return true;
            }
        }
        return false;
    }

}