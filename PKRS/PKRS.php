<?php
/**************************************************************
 *
 * PKRS.php, created 7.11.14
 * Since: 0.1.3
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/


namespace PKRS {

    use PKRS\Core\Application;
    use PKRS\Core\FileSystem\File;
    use PKRS\Core\Service\ServiceContainer;
    use PKRS\Generator\Generator;


    /**
     * Class PKRS
     * @package PKRS
     *
     * @added ${DATE}
     * @since 0.2.0
     * @author Petr Klimeš <djpitrrs@gmail.com>
     */
    class PKRS
    {

        private $service;
        private static $instance;

        /**
         * @param null $application_dir
         * @param null $config_file
         */
        public function __construct($application_dir = null, $config_file = null)
        {
            include_once("Shortcodes.php");
            if ($application_dir === null) {
                $application_dir = realpath(dirname($_SERVER["SCRIPT_FILENAME"]));
            }
            try {
                if (!is_dir($application_dir)) {
                    if (mkdir(realpath(dirname($_SERVER["SCRIPT_FILENAME"])) . DIRECTORY_SEPARATOR . $application_dir)) {
                        define("APP_DIR", realpath(dirname($_SERVER["SCRIPT_FILENAME"])) . DIRECTORY_SEPARATOR . $application_dir . DIRECTORY_SEPARATOR);
                    } else throw new \Exception("Application dir: $application_dir not exists!");
                } else {
                    define("APP_DIR", rtrim(realpath($application_dir), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR);
                }
                define("PKRS_DIR", dirname(__FILE__) . DIRECTORY_SEPARATOR);

                include(dirname(__FILE__) . DIRECTORY_SEPARATOR . "Core" . DIRECTORY_SEPARATOR . "_includes" . DIRECTORY_SEPARATOR . "main.php");
            } catch (\Exception $e) {
                die("PKRS: Error in PKRS constructor - " . $e->getMessage());
            }

            $this->service = new ServiceContainer($config_file);
            $this->service()->getDebug()->enable(false);
            self::$instance = $this;
            if (!file_exists(ROOT_DIR . ".htaccess"))
                $this->fixHtaccess();
        }

        /**
         * @param null $dir
         * @return $this
         */
        function fixHtaccess($dir = null)
        {
            $f = new File(($dir ? $dir : ROOT_DIR).DS . ".htaccess");
            $f->open();
            $ht = 'RewriteEngine On' . PHP_EOL . '<FilesMatch "\.(htaccess|htpasswd|ini|psd|log|sh|neon)$">' . PHP_EOL . 'Deny From All' . PHP_EOL . '</FilesMatch>' . PHP_EOL . 'RewriteCond %{REQUEST_FILENAME} !-f' . PHP_EOL . 'RewriteRule . index.php [L]' . PHP_EOL;
            $f->write($ht);
            $f->close();
            return $this;
        }

        /**
         * @return PKRS
         */
        public static function getInstance()
        {
            return self::$instance;
        }

        /**
         * Function: debug
         * Since: 0.1.4
         * Description:
         * @param bool $condition (true = enabled, false = disabled, "IP ADRESS STRING" = enabled if IP match, array of IP ADRESS STRINGS)
         * @return $this
         */
        public function debug($condition = false)
        {
            $debug = $this->service->getDebug();
            $debug->enable($this->matchConditionOrIP($condition));
            return $this;
        }

        public function generateModel($ns = null, $table = null){
            $g = new \PKRS\Core\Model\Generator();
            $g->autoCreateDir = true;
            $g->rewriteOldFile = true;
            $g->setNamespace($ns ? $ns : "Db");
            if ($table)
                $g->generate($table);
            else $g->generateAllTables();
            return $this;
        }

        /**
         * @param $condition
         * @return bool
         */
        private function matchConditionOrIP($condition)
        {
            if (is_bool($condition)) return (!!$condition);
            else if (is_string($condition)) {
                // fix string boolean
                if (strtoupper($condition) == "TRUE" || strtoupper($condition) == "FALSE") {
                    return (strtoupper($condition) == "TRUE");
                } elseif (filter_var($condition, FILTER_VALIDATE_IP))
                    return (($_SERVER["HTTP_X_FORWARDED_FOR"] ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"]) == $condition);
                else return false;
            } else if (is_array($condition)) {
                foreach ($condition as $ip) {
                    if (!filter_var($ip, FILTER_VALIDATE_IP))
                        return false;
                }
                return (in_array($_SERVER["REMOTE_ADDR"], $condition));
            } else return false;
        }

        /**
         * @return ServiceContainer
         */
        public function service()
        {
            return $this->service;
        }

        /**
         * @return Core\Config\Config
         */
        public function config()
        {
            return $this->service->getConfig();
        }

        /**
         * @return Core\Router\Router
         */
        public function router()
        {
            return $this->service->getRouter();
        }

        /**
         * @param bool|true $enableGenerator
         * @throws Core\Exception\MainException
         */
        public function run($enableGenerator = true)
        {
            if ($this->matchConditionOrIP($enableGenerator)) {
                // installer enabled -> map installer application
                $router = $this->service->getRouter();
                $router->setBasePath(PKRS_WEB_PATH); // overide base path
                define("TOOLS_ENABLED", true);
                if (count($router->getRoutes()) == 0) { // no routes -> run installer
                    // no other routes -> redirect to /--generator (if we are not here)
                    $route = trim($_SERVER["REQUEST_URI"], "/");
                    if (file_exists(ROOT_DIR . ".htaccess")) {
                        if (strpos("--generator") != false) {
                            $_SESSION["mess"]["ok"][] = "Welcome to the PKRS generator! First: Create your new module.";
                            header("location:".PKRS_WEB_PATH."--generator");
                            die();
                        }
                    } else $this->fixHtaccess();
                    new Generator();
                    die();
                } else { // any routes defined -> generator
                    $route = trim($_SERVER["REQUEST_URI"], "/");
                    if (!file_exists(ROOT_DIR . ".htaccess"))
                        $this->fixHtaccess();
                    if (strpos("--generator") !== false) {
                        new Generator();
                        die();
                    }
                    // show message -> tools is still enabled
                    $this->service()->getMessages()->add("Generator is enabled!", "err");
                }
            } else {
                // disabole tools -> for security
                define("TOOLS_ENABLED", false);
            }
            $app = new Application($this->service);
            $app->run();
        }

    }

}