<?php
namespace PKRS\Generator;
use gossi\codegen\generator\CodeGenerator;
use gossi\codegen\model\PhpClass;
use gossi\codegen\model\PhpMethod;
use gossi\codegen\model\PhpParameter;
use gossi\codegen\model\PhpProperty;
use Nette\Neon\Neon;
use PKRS\Core\FileSystem\File;
use PKRS\Core\Service\ServiceContainer;
use PKRS\Helpers\Generators\Randoms;
use PKRS\PKRS;

/**
 * Class PresentersGenerator
 * @package PKRS\Generator
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class PresentersGenerator{

    var $f,$c;

    var $sc;

    var $modules;

    var $namespace;

    var $vars;

    /**
     * @param ServiceContainer $sc
     * @param array $rootVars
     * @param string $namespace
     */
    public function __construct(ServiceContainer $sc, array $rootVars = [], $namespace = "Presenters"){
        $this->sc = $sc;
        $this->modules = [];
        $this->vars = $rootVars;
        $this->namespace = $namespace;
        $this->rrmdir(APP_DIR.$this->namespace);
    }

    /**
     * @param $dir
     */
    private function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    /**
     * @param $name
     * @param bool|false $need_login
     * @param null $theme
     * @param null $layout
     * @return PresentersModuleGenerator
     */
    public function addModule($name, $need_login = false, $theme = null, $layout = null){
        $need_login = !!$need_login;
        if (!$theme)
            $theme = $name;
        $this->modules[$name] = $c = new PresentersModuleGenerator($name, $need_login, $theme, $this->namespace, $layout);
        return $c;
    }

    public function generate(){
        $this->rootPresenter($this->vars);
        foreach($this->modules as $name => $module){
            if ($module instanceof PresentersModuleGenerator) {
                if (!is_dir(THEMES_DIR.$name))
                    mkdir(THEMES_DIR.$name, 0777, true);
                //if (!is_file())
                $this->rootModulePresenter($name, $module->need_login, $module->theme);
                $module->generate();
            }
        }
        // index.php
        $p = PKRS::getInstance();
        if ($p instanceof PKRS){
            if (!($cfile = $p->config()->getConfigFile())){
                $ng = new Neon();
                $neon = $ng->encode($p->config()->getAll());
                $config_name = (new Randoms)->getStringByLength(20).".neon";
                $config_file = new File(APP_DIR.$config_name);
                $config_file->open();
                $config_file->write($neon);
                $config_file->close();
                $cfile = APP_DIR.$config_name;
            }
            $f = new File($_SERVER["SCRIPT_FILENAME"]);
            $f->open();
            $index = "<?php\ninclude('vendor/autoload.php');\n\$pkrs = new \\PKRS\\PKRS('".basename(APP_DIR)."','$cfile');\n\$pkrs->debug('".(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"]) . "');\n";
            $index.="\$r = \$pkrs->router();\n";
            $added  = [];
            foreach($this->routes() as $r){
                if (!isset($added[$r["name"]]))
                {
                    $index .= "\$r->map('".$r["method"]."','".$r["route"]."',".var_export($r["target"],true).",'".$r["name"]."');\n";
                    $added[$r["name"]] = $r;
                }

            }
            $index.="\$pkrs->run('".(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : $_SERVER["REMOTE_ADDR"]) . "');";
            $f->write($index);
            $f->close();
        } else die("Application not created");
    }

    /**
     * @return array
     */
    public function routes(){
        $routes = [];
        foreach($this->modules as $name => $module){
            if ($module instanceof PresentersModuleGenerator) {
                foreach ($module->routes() as $r){
                    $routes[] = $r;
                }
            }
        }
        return $routes;
    }

    public function structure(){
        echo "<ul>";
        echo "<li>".$this->namespace;
        if ($this->modules) {
            echo "<ul>";
            foreach ($this->modules as $name => $module) {
                $module->structure();
            }
            echo "</ul>";
        }
        echo "</li>";
        echo "</ul>";
    }

    /**
     * @param array $params
     * @throws \Exception
     */
    private function rootPresenter($params = []){
        if (!is_dir(APP_DIR.$this->namespace))
            mkdir(APP_DIR.$this->namespace);
        $this->f = new File(APP_DIR.$this->namespace.DS."RootPresenter.php");
        $this->f->open();
        $this->c = new PhpClass();
        $this->c->setAbstract(true);
        $this->c->setQualifiedName('\\'.$this->namespace.'\RootPresenter')
            ->setParentClassName('\PKRS\SPresenter');
        $properties = [];
        $constructor_body = "parent::__construct();".PHP_EOL;
        foreach($params as $name=>$type) {
            if (method_exists($this->sc,"get".ucfirst($type))) {
                $f = "get".$type;
                $o = $this->sc->$f();
                $properties[] = (new PhpProperty($name))->setType("\\".get_class($o));
                $constructor_body .= '$this->' . $name . ' = self::gc()->get'.ucfirst($type).'();' . PHP_EOL;
            } else throw new \Exception("Service $type not exists!");
        }
        $this->c->setProperties($properties);
        $constructor = PhpMethod::create('__construct');
        $constructor->setBody($constructor_body);
        $this->c->setMethod($constructor);
        $generator = new CodeGenerator();
        $php = $generator->generate($this->c);
        $this->f->write("<?php".PHP_EOL.$php);
        $this->f->close();
        //echo "<h1>ROOT</h1><pre>" . $php ."<pre><hr>";
    }

    /**
     * @param $module
     * @param $need_login
     * @param $theme
     */
    private function rootModulePresenter($module, $need_login, $theme){
        if (!is_dir(APP_DIR.$this->namespace.DS. $module))
            mkdir(APP_DIR.$this->namespace.DS. $module);
        $this->f = new File(APP_DIR.$this->namespace.DS. $module .DS."Root".$module."Presenter.php");
        $this->f->open();
        $this->c = new PhpClass();
        $this->c->setAbstract(true);
        $this->c->setQualifiedName('\\'.$this->namespace.'\\'.$module."\\Root".$module."Presenter")
            ->setParentClassName('\\'.$this->namespace.'\\RootPresenter');
        $_login = (PhpMethod::create('needLogin'));
        $_login->setDocblock("@return ".($need_login ? "true" : "false"));
        $_login->setBody("return ".($need_login ? "true" : "false").";");
        $this->c->setMethod($_login);
        $thm = (PhpMethod::create('setTheme'));
        $thm->setBody("return ".$theme.";");
        $this->c->setMethod($thm);
        $generator = new CodeGenerator();
        $php = $generator->generate($this->c);
        $this->f->write("<?php".PHP_EOL.$php);
        $this->f->close();
        //echo "<h2>ROOT/$module</h2><pre>" . $php ."<pre><hr>";
        // Module sections
    }

}