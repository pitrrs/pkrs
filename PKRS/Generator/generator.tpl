<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PKRS Generator</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.0/ace.js"></script>
    <style>
        pre{
            white-space: nowrap;
            display:
        }
        textarea.hidden{
            overflow:hidden;
            width:1px;
            height: 1px;
            border:none;
            background:transparent;
        }
    </style>
    {literal}
        <script type="text/javascript">
            jQuery(document).ready(function(){
                $(".nav.nav-pills li a").click(function(){
                    //$(this).find(".tab-pane li a:first").addClass("active");
                    $(this).parent(".navc").find(".navc").each(function(){
                        $(this).find(".nav-pills li a:first").trigger("click");
                    });
                });
                $('pre.html').each(function(i, block) {
                    var input = $(this).parent().find( "textarea[name='"+$(block).attr("name").replace("_editor","")+"']" )
                    var editor = ace.edit(block);
                    editor.setTheme("ace/theme/eclipse");
                    editor.setOptions({
                        maxLines: 100,
                        minLines:30,
                        useWorker: false
                    });
                    editor.getSession().setValue(input.text());
                    editor.getSession().setMode({path:"ace/mode/xml", inline:false});
                    editor.getSession().on("change", function () {
                        input.text(editor.getSession().getValue());
                    });
                });
                $('pre.php').each(function(i, block) {
                    var input = $(this).parent().find( "textarea[name='"+$(block).attr("name").replace("_editor","")+"']" )
                    var editor = ace.edit(block);
                    editor.setTheme("ace/theme/eclipse");
                    editor.setOptions({
                        maxLines: 100,
                        minLines:30,
                        useWorker: false
                    });
                    editor.getSession().setValue(input.text());
                    editor.getSession().setMode({path:"ace/mode/php", inline:true});
                    editor.getSession().on("change", function () {
                        input.text(editor.getSession().getValue());
                    });
                });
            });
        </script>
    {/literal}
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            {if $mess.ok || $mess.err}
                {foreach from=$mess.err item=m}
                    <div class="col-md-12 alert alert-danger">{$m}</div>
                {/foreach}
                {foreach from=$mess.ok item=m}
                    <div class="col-md-12 alert alert-success">{$m}</div>
                {/foreach}
            {/if}
            <div class="col-md-12 navc">
                    <ul class="nav nav-pills nav-justified">
                        {foreach from=$data item=i key=k name=a}
                        <li {if ($smarty.foreach.a.first && !isset($smarty.get.open_module)) || $smarty.get.open_module==$k }class="active" {/if} ><a href="#Module{$k}" data-toggle="tab">{$k}{if $i.root.need_login} (secured){/if}</a></li>
                        {/foreach}
                        <li {if count($data)==0 && $smarty.get.open_module!="_config"}class="active"{/if}><a href="#_create_new" data-toggle="tab">NEW MODULE</a></li>
                        <li {if $smarty.get.open_module=="_config"}class="active"{/if}><a href="#_config" data-toggle="tab">APP CONFIG</a></li>
                    </ul>

                    <div class="tab-content">
                        {foreach from=$data item=i key=k name=a}
                        <div class="tab-pane {if ($smarty.foreach.a.first && !isset($smarty.get.open_module)) || $smarty.get.open_module==$k }active{/if}" id="Module{$k}">
                            <div id='content' class="tab-content navc">

                                    <ul class="nav nav-pills nav-justified top_level">
                                        {foreach from=$i.others item=ii key=kk name=aa}
                                            <li {if ($smarty.foreach.aa.first && (!isset($smarty.get.open_presenter) || $smarty.get.open_module!=$k)) || $smarty.get.open_presenter==$kk }class="active" {/if} ><a href="#Presenter{$k}{$kk}" data-toggle="tab">{$kk}</a></li>
                                        {/foreach}
                                        <li {if (count($i.others)==0 && !isset($smarty.get.open_presenter))}class="active"{/if} ><a href="#_create_new_presenter_{$k}" data-toggle="tab">NEW PRESENTER</a></li>
                                        <li  {if $smarty.get.open_module==$k && $smarty.get.open_presenter == "settings"}class="active"{/if} ><a href="#settings{$k}" data-toggle="tab">MODULE SETTINGS</a></li>
                                    </ul>
                                    {foreach from=$i.others item=ii key=kk name=aa}
                                        <div class="tab-pane {if ($smarty.foreach.aa.first && (!isset($smarty.get.open_presenter) || $smarty.get.open_module!=$k) || $smarty.get.open_presenter==$kk )}active{/if}" id="Presenter{$k}{$kk}">
                                            <div class="tab-content">
                                                <div class="row navc">
                                                    <div class="col-md-2">
                                                        <h4>Presenter actions:</h4>
                                                        <ul class="nav nav-pills nav-stacked">
                                                            {foreach from=$ii item=iii key=kkk name=aaa}
                                                            <li {if ($smarty.foreach.aaa.first && (!isset($smarty.get.open_action) || $smarty.get.open_presenter!=$kk)) || $smarty.get.open_action==$iii.action}class="active"{/if} ><a href="#Module{$k}{$kk}{$iii.action}" data-toggle="tab"><strong>{$iii.action}</strong><br><i>/{$iii.route.route}</i><br><i>{$k}{$kk|replace:"Presenter":""}{$iii.action|ucfirst}</i></a></li>                                                            {/foreach}

                                                            <li {if count($ii) == 0}class="active"{/if} ><a href="#create{$k}{$kk}" data-toggle="tab">New action</a></li>
                                                        </ul>
                                                    </div>
                                                    <div class="tab-content col-md-10">
                                                        {foreach from=$ii item=iii key=kkk name=aaa}
                                                        <div class="tab-pane  {if ($smarty.foreach.aaa.first && (!isset($smarty.get.open_action) || $smarty.get.open_presenter!=$kk)) || $smarty.get.open_action==$iii.action}active{/if}" id="Module{$k}{$kk}{$iii.action}">
                                                            <h2>Edit {$k}\{$kk}->{$iii.action}()
                                                                <form method="post" action="?open_module={$k}&open_presenter={$kk}" class=" pull-right">
                                                                    <input type="hidden" name="action" value="delete_action">
                                                                    <input type="hidden" name="module" value="{$k}">
                                                                    <input type="hidden" name="presenter" value="{$kk}">
                                                                    <input type="hidden" name="aname" value="{$iii.action}">
                                                                    <input type="submit" class="btn btn-danger" value="Delete" onclick="return confirm('Are you sure to delete action {$k}\\{$kk}->{$iii.action}() with template {$k}\\{$iii.template}?')">
                                                                </form>
                                                            </h2>
                                                            <form method="post" action="?open_module={$k}&open_presenter={$kk}&open_action={$iii.action}">
                                                                <input type="hidden" name="action" value="edit_action">
                                                                <input type="hidden" name="module" value="{$k}">
                                                                <input type="hidden" name="presenter" value="{$kk}">
                                                                <input type="hidden" name="name" value="{$iii.action}">
                                                                <div class="row">
                                                                    <div class="form-group col-md-4">
                                                                        <label>Template filename</label>
                                                                        <input type="text" name="template" class="form-control" value="{$iii.template}">
                                                                    </div>
                                                                    <div class="form-group col-md-4">
                                                                        <label>Route (eq. pages, pages/[i:id], pages/[s:seo_url])</label>
                                                                        <input type="text" name="route" class="form-control" value="{$iii.route.route}">
                                                                    </div>
                                                                    <div class="form-group col-md-4">
                                                                        <label>Method (* || GET || PUT || POST || DELETE)</label>
                                                                        <input type="text" name="method" class="form-control" value="{$iii.route.method}">
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <label>PHP Code</label>
                                                                    <pre name="body_editor" class="form-control php" rows="20"></pre>
                                                                    <textarea name="body" class="form-control hidden">{$iii.body}</textarea>

                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label>Template body</label>
                                                                    <pre name="template_body_editor" class="form-control html" rows="20"></pre>
                                                                    <textarea name="template_body" class="form-control hidden">{$iii.template_body}</textarea>

                                                                </div>
                                                                </div>
                                                                <input type="submit" class="btn btn-primary" value="Save action">
                                                            </form>
                                                        </div>
                                                        {/foreach}

                                                        <div class="tab-pane  {if count($ii) == 0}active{/if}" id="create{$k}{$kk}">
                                                            <h2>Create new action in {$k}\{$kk}</h2>
                                                            <form method="post" action="?open_module={$k}&open_presenter={$kk}">
                                                                <input type="hidden" name="action" value="new_action">
                                                                <input type="hidden" name="module" value="{$k}">
                                                                <input type="hidden" name="presenter" value="{$kk}">
                                                                <div class="form-group">
                                                                    <label>Action name</label>
                                                                    <input type="text" class="form-control" name="name" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Template filename (if blank: ModuleAction.tpl will be used)</label>
                                                                    <input type="text" class="form-control" name="template" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Route (eq. pages, pages/[i:id], pages/[s:seo_url])</label>
                                                                    <input type="text" class="form-control" name="route" value="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Method (* || GET || PUT || POST || DELETE)</label>
                                                                    <input type="text" class="form-control" name="method" value="*">
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="submit" class="btn btn-primary" value="Create action">
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    {/foreach}
                                <div class="tab-pane  {if count($i.others)==0 && $smarty.get.open_presenter !="settings"}active{/if}" id="_create_new_presenter_{$k}">
                                    <h2>Create new presenter in {$k} module</h2>
                                    <form method="post" action="?open_module={$k}">
                                        <input type="hidden" name="action" value="new_presenter">
                                        <input type="hidden" name="module" value="{$k}">
                                        <div class="form-group">
                                            <label>Presenter name</label>
                                            <input type="text" name="name" class="form-control">
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Create presenter">
                                    </form>
                                </div>
                                <div class="tab-pane  {if $smarty.get.open_module==$k && $smarty.get.open_presenter == "settings"}active{/if} " id="settings{$k}">
                                    <h2>Settings module {$k}
                                    <form method="post" class="pull-right">
                                        <input type="hidden" name="action" value="module_delete">
                                        <input type="hidden" name="module" value="{$k}">
                                        <input type="submit" value="Delete module" class="btn btn-danger" onclick="return confirm('Are you sure to delete module {$k}?')">
                                    </form>
                                    </h2>
                                    <div class="row">
                                        <form method="post" action="?open_module={$k}&open_presenter=settings">
                                            <div class="col-md-4">
                                                <input type="hidden" name="action" value="edit_module">
                                                <input type="hidden" name="module" value="{$k}">
                                                <div class="form-group">
                                                    <label>Theme</label>
                                                    <input type="text" class="form-control" name="theme" value="{$i.root.theme}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Need login</label>
                                                    <select name="needLogin" class="form-control">
                                                        <option value="1" {if $i.root.need_login}selected{/if}>True</option>
                                                        <option value="0" {if !$i.root.need_login}selected{/if}>False</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label>Module main layout</label>
                                                    <pre name="layout_editor" class="form-control html" rows="20"></pre>
                                                    <textarea name="layout" class="form-control hidden">{$i.root.layout}</textarea>

                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="col-md-12">

                                                <div class="form-group">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>


                            </div>
                        </div>
                        {/foreach}
                        <div class="tab-pane {if count($data)==0 &&  $smarty.get.open_module!="_config"}active{/if}" id="_create_new">
                            <h2>Create new module</h2>
                            <form method="post">
                                <input type="hidden" name="action" value="new_module">
                                <div class="form-group">
                                    <label>Module name</label>
                                    <input type="text" name="name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Theme name</label>
                                    <input type="text" name="theme" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Need login</label>
                                    <select name="needLogin" class="form-control">
                                        <option value="1">TRUE</option>
                                        <option value="0">FALSE</option>
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Create module">
                            </form>
                        </div>
                        <div class="tab-pane {if $smarty.get.open_module=="_config"}active{/if}" id="_config">
                            <h2>Application configuration
                                <button class="btn btn-success" onclick="var key=prompt('Enter config group key:');if (key != null)$();return false;">Add group</button>
                            </h2>
                            <form method="post" action="?open_module=_config">
                                <div class="row">
                                <input type="hidden" name="action" value="update_config">
                                {foreach from=$config item=c key=g}
                                    <div class="col-md-12" id="config-group-{$g}">
                                        <h3>{$g} <button class="btn btn-success" onclick="var key=prompt('Enter config key:');if (key != null)$();return false;">Add item</button></h3>
                                        {foreach from=$c item=i key=ik}
                                            <div class="row" id="config-{$g}-{$ik}">
                                                <div class="form-group col-md-10">
                                                    <label>{$ik}</label>
                                                    {if is_bool($i)}
                                                        <select class="form-control" name="config[{$g}][{$ik}]">
                                                            <option value="true" {if $i}selected{/if}>TRUE</option>
                                                            <option value="false" {if !$i}selected{/if}>FALSE</option>
                                                        </select>
                                                    {else}
                                                    <input type="text" class="form-control" name="config[{$g}][{$ik}]" value="{$i}">
                                                    {/if}
                                                </div>
                                                <div class="col-md-2">
                                                    <br>
                                                    <button class="btn btn-danger" onclick="if (confirm('Are you sure?'))$('#config-{$g}-{$ik}').remove();return false;">Delete</button>
                                                </div>
                                            </div>
                                        {/foreach}
                                        <div class="clearfix"></div>
                                    </div>
                                {/foreach}
                                </div>
                                <input type="submit" class="btn btn-primary" value="Update config">
                            </form>
                        </div>
                    </div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
<br><br>
<br><br>
<br><br>
</body>
</html>