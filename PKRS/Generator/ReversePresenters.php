<?php
namespace PKRS\Generator;
use gossi\docblock\Docblock;
use PKRS\Core\Config\Config;

/**
 * Class ReversePresenters
 * @package PKRS\Generator
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class ReversePresenters {

    var $modules = [];

    var $root;

    var $routes = [];


    /**
     * @param string $namespace
     */
    public function __construct($namespace = "Presenters"){
        $config = new Config();
        $services = [];
        foreach($config->getServices() as $k=>$v){
            $cl = str_replace("/","\\",$v["class"]);
            $services[$cl] = $k;
        }

        // iterace pres namespace
        foreach(@glob(APP_DIR.$namespace.DS."*") as $item){
            if (is_dir($item)){
                $presenters = ["root"=>null,"others"=>[]]; // Presentery
                // iterace pres modul
                foreach(@glob($item.DS."*.php") as $p){
                    $presenter = []; // pro AKCE
                    $name = $namespace."\\".basename($item)."\\".basename($p,".php");
                    $cl = new \ReflectionClass("\\".$name);
                    if ($cl->isAbstract()){
                        // ROOT$methods = $cl->getMethods();
                        $m = $cl->getMethod("needLogin");
                        $doc = new Docblock($m->getDocComment());
                        $router = [];
                        foreach($doc->getSortedTags()->toArray() as $t){
                            //var_dump($t);exit;
                            $d = $t->getDescription();
                            if ($d === "true") $d=true;
                            if ($d === "false") $d = false;
                            $presenter[$t->getTagName()] = $d;
                        }
                        $m = $cl->getMethod("setTheme");
                        $doc = new Docblock($m->getDocComment());
                        $router = [];
                        foreach($doc->getSortedTags()->toArray() as $t){
                            //var_dump($t);exit;
                            $d = $t->getDescription();
                            if ($d === "true") $d=true;
                            if ($d === "false") $d = false;
                            $presenter[$t->getTagName()] = $d;
                        }
                        $presenters["root"] = $presenter;
                        $presenters["root"]["layout"] =  @file_get_contents(THEMES_DIR.basename($item).DS."_layout.tpl");
                    } else {
                        $methods = $cl->getMethods();
                        foreach($methods as $m){
                            if ($m->class == $name){
                                if ($m->isConstructor()) continue;
                                $doc = new Docblock($m->getDocComment());
                                $router = [];
                                foreach($doc->getSortedTags()->toArray() as $t){
                                    //var_dump($t);exit;
                                    if ($t->getTagName() == "needLogin" || $t->getTagName() == "theme" || $t->getTagName() == "route" || $t->getTagName() == "method")
                                        $router[$t->getTagName()] = $t->getDescription();
                                }
                                $body_array = file($p);
                                $body = "";
                                $template = "";
                                for($i=$m->getStartLine();$i<$m->getEndLine()-1;$i++){
                                    if (strpos($body_array[$i],"this->template")){
                                        $template = trim(trim(explode("=",str_replace(";","",$body_array[$i]))[1]),'""');
                                    }
                                    else $body .= $body_array[$i].PHP_EOL;
                                }

                                $presenter[] = [
                                    "route" =>$router,
                                    "template" => $template,
                                    "template_body" => @file_get_contents(THEMES_DIR.basename($item).DS.$template),
                                    "action"=>$m->name,
                                    "body" => $this->fix_brackets($body)
                                ];
                            }
                        }
                        $presenters["others"][basename($p,".php")] = $presenter;
                    }

                } // iterace pres modul


                $this->modules[basename($item)] = $presenters;
            } elseif (is_file($item)) {
                $name = $namespace."\\".basename($item,".php");
                $cl = new \ReflectionClass("\\".$name);

                $vars = [];
                foreach($cl->getProperties() as $v){
                    if ($v->class == $name){
                        $doc = new Docblock($v->getDocComment());
                        $vars[$v->getName()] = $services[$doc->getSortedTags()->get(0)->getType()];
                    }
                }
                $this->root = [
                    "vars" => $vars
                ];
            } // is file -> hlavni root presenter
        } // iterace pres namespace
        //echo "<pre>".var_export($this->modules,true)."</pre>";
    }

    /**
     * @param $body
     * @return mixed|string
     */
    private function fix_brackets($body){
        $opens = substr_count($body, "{");
        $closes = substr_count($body, "}");
        if ($opens == $closes) return $body;
        else if ($closes > $opens){
            // vice zavirani -> smazat odzadu
            $rev = strrev($body); // otocit
            $count = $closes - $opens;
            $rev = str_replace("}","",$rev,$count);
            $body = strrev($rev);
        } else if ($closes < $opens){
            $count = $opens - $closes;
            $body = str_replace("{","",$body, $count);
        }
        return $body;
    }
}