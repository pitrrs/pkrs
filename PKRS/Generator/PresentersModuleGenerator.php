<?php
namespace PKRS\Generator;
use gossi\codegen\generator\CodeGenerator;
use gossi\codegen\model\PhpClass;
use gossi\codegen\model\PhpMethod;
use gossi\codegen\model\PhpParameter;
use gossi\codegen\model\PhpProperty;
use PKRS\Core\FileSystem\File;

/**
 * Class PresentersModuleGenerator
 * @package PKRS\Generator
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class PresentersModuleGenerator{

    var $name, $need_login, $theme, $namespace,$layout;
    var $sections = [];

    /**
     * @param $name
     * @param $need_login
     * @param $theme
     * @param $namespace
     * @param null $layout
     */
    function __construct($name, $need_login, $theme, $namespace, $layout = null)
    {
        $this->name = $name;
        $this->need_login = $need_login;
        $this->theme = $theme;
        $this->namespace = $namespace;
        $this->layout = $layout."";
    }

    /**
     * @param $name
     * @return PresentersModuleSectionGenerator
     */
    public function addSection($name){
        $c = new PresentersModuleSectionGenerator($name, $this->namespace, $this->name);
        $this->sections[$name] = $c;
        return $c;
    }

    public function structure(){
        echo "<ul>";
        echo "<li>".$this->name;
        if ($this->sections) {
            echo "<ul>";
            foreach ($this->sections as $name => $module) {
                $module->structure();
            }
            echo "</ul>";
        }
        echo "</li>";
        echo "</ul>";
    }

    /**
     * @return array
     */
    public function routes(){
        $routes = [];
        foreach($this->sections as $name => $module){
            if ($module instanceof PresentersModuleSectionGenerator) {
                foreach ($module->routes() as $r){
                    $routes[] = $r;
                }
            }
        }
        return $routes;
    }

    public function generate(){
        if (!is_dir(APP_DIR."Themes".DS. $this->name))
            mkdir(APP_DIR."Themes".DS. $this->name,0755,true);
        if (!is_dir(APP_DIR.$this->namespace.DS. $this->name))
            mkdir(APP_DIR.$this->namespace.DS. $this->name);

        // ROOT
        $f = new File(APP_DIR.$this->namespace.DS. $this->name .DS."Root".$this->name."Presenter.php");
        $f->open();
        $c = new PhpClass();
        $c->setAbstract(true);

        $c->setQualifiedName('\\'.$this->namespace.'\\'.$this->name."\\Root".$this->name."Presenter")
            ->setParentClassName('\\'.$this->namespace.'\\RootPresenter');
        $_login = (PhpMethod::create('needLogin'));
        $_login->setBody("return ".($this->need_login ? "true" : "false").";");
        $_login->setDocblock("@needLogin ".($this->need_login ? "true" : "false")."");
        $c->setMethod($_login);
        $thm = (PhpMethod::create('setTheme'));
        $thm->setBody("return \"".$this->theme."\";");
        $thm->setDocblock("@theme ".$this->theme."");
        $c->setMethod($thm);
        $generator = new CodeGenerator();
        $php = $generator->generate($c);
        $f->write("<?php".PHP_EOL.$php);
        $f->close();
        //echo "<h3>ROOT/Root".$this->name."</h3><pre>" . $php ."<pre><hr>";
        // Sections
        foreach ($this->sections as $name=>$s){
            if ($s instanceof PresentersModuleSectionGenerator){
                $s->generate();
            }
        }

        $t = new File(THEMES_DIR.$this->name.DS."_layout.tpl");
        $t->open();
        if (!$this->layout)
            $layout = <<<HTML
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>My sitename</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container-fluid">
        {if \$messages}
        <div class="row">
            {foreach from=\$messages item=m key=t}
            <div class="col-md-12 alert alert-{if \$t=='ok'}success{elseif \$t=='err'}danger{else}info{/if}">{\$m}</div>
            {/foreach}
        </div>
        {/if}
        <div class="row">

            <div class="col-md-12">
                {block name="content"}{/block}
            </div>
        </div>
    </div>
</body>
</html>
HTML;
        else $layout = $this->layout;
        $t->write($layout);
        $t->close();

    }
}