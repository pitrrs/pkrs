<?php
namespace PKRS\Generator;
use gossi\codegen\generator\CodeGenerator;
use gossi\codegen\model\PhpClass;
use gossi\codegen\model\PhpMethod;
use gossi\codegen\model\PhpParameter;
use gossi\codegen\model\PhpProperty;

/**
 * Class PresentersModuleSectionActionGenerator
 * @package PKRS\Generator
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class PresentersModuleSectionActionGenerator {

    var $name, $template, $body, $route, $template_body;

    /**
     * @param $name
     * @param $module
     * @param array $route
     * @param null $template
     */
    public function __construct($name, $module, array $route = [], $template = null){
        $this->name = $name;
        $this->route = $route;
        $this->template = $template ? $template : $module.ucfirst($name);
        if (substr($this->template, strlen($this->template)-4,4) != ".tpl")
            $this->template = $this->template.".tpl";
    }

    /**
     * @param $code_body
     * @return $this
     */
    public function body($code_body){
        $code_body = $this->sanitize_body($code_body);
        if (is_array($code_body)){
            $code_body = implode(PHP_EOL,$code_body);
        }
        $this->body = $code_body;
        return $this;
    }

    /**
     * @param $body
     */
    public function template_body($body){
        $this->template_body = $body;
    }

    /**
     * @param $body
     * @return array
     */
    private function sanitize_body($body){
        if (!is_array($body)) $body = $this->sanitize_body(explode(PHP_EOL,$body));
        $c= null;
        foreach($body as $line=>$txt){
            $nc = substr_count("\t",$txt);
            if ($c === null || $c > $nc) $c = $nc;
        }
        foreach($body as $line=>$txt){
            if (trim($txt) == "") unset($body[$line]);
            else trim($body[$line],PHP_EOL);
            $body[$line] = str_replace("\t","",$body[$line],$c);
        }
        return $body;
    }

    /**
     * @return PhpMethod
     */
    public function generate(){
        $this->body = "\$this->template = \"".$this->template."\";".PHP_EOL.$this->body;
        $m = new PhpMethod($this->name);
        $m->addParameter(
            (new PhpParameter("params"))->setType("array")
        );
        $m->setBody($this->body);
        $m->setDocblock("@route ".$this->route["route"].PHP_EOL."@method ".$this->route["method"]);
        return $m;
    }



}