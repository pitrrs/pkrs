<?php
namespace PKRS\Generator;

use Nette\Neon\Neon;
use PKRS\Core\FileSystem\File;
use PKRS\Core\Service\ServiceContainer;
use PKRS\PKRS;

/**
 * Class Generator
 * @package PKRS\Generator
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class Generator {

    private $g;
    private $namespace;
    private $mess;
    private $pkrs;

    /**
     * @param string $namespace
     */
    public function __construct($namespace = "Presenters"){
        if (!defined("TOOLS_ENABLED") || TOOLS_ENABLED !== true) die("Unallowd access!");
        @session_start();
        $this->pkrs = PKRS::getInstance();
        if (!isset($_SESSION["mess"]))
            $_SESSION["mess"] = ["ok" => [],"err" => []];
        $this->namespace = $namespace;
        if ($this->pkrs->service()->getDatabase()->testConnection()) {
            $g = new \PKRS\Core\Model\Generator();
            $g->setNamespace("Db");
            $g->autoCreateDir = true;
            $g->rewriteOldFile = true;
            $g->generateAllTables();
        }
        $this->_process_save();
        $this->_view();
    }

    private function _process_save(){
        if (!isset($_SESSION["started"]) || $_SESSION["started"] !== true) return;
        if (isset($_POST["action"])){
            if(method_exists($this, "post_".$_POST["action"])){
                $this->g = new ReversePresenters($this->namespace);
                if (call_user_func([$this, "post_".$_POST["action"]])) {
                    $this->reconstruct();
                    header("location:".$_SERVER["REQUEST_URI"]);
                    die();
                }
            } else $_SESSION["mess"]["err"][] = "Not allowed action";
        }
    }

    private function _view(){
        $_SESSION["started"] = true;
        $sm = new \Smarty();
        //$sm->debugging = true;
        $this->g = new ReversePresenters($this->namespace);
        $sm->assign("data", $this->g->modules);
        $sm->assign("mess", $_SESSION["mess"]);
        $sm->assign("config", $this->pkrs->config()->getAll());
        $sm->display(__DIR__."/generator.tpl");
        $_SESSION["mess"] = [
            "ok" => [],
            "err" => []
        ];
    }


    private function reconstruct(){
        if (!$this->g->root["vars"]){
            $this->g->root["vars"] = [
                "db"=>"database",
                "messages"=>"messages",
                "config"=>"config"
            ];
        }
        $generator = new PresentersGenerator(new ServiceContainer(), $this->g->root["vars"], $this->namespace);
        foreach($this->g->modules as $module=>$data){
            $mObj = $generator->addModule($module, $data["root"]["needLogin"], $data["root"]["theme"], $data["root"]["layout"]);
            foreach($data["others"] as $presenterName=>$presenterData){
                $pObj = $mObj->addSection($presenterName);
                foreach($presenterData as $action){
                    $a = $pObj->addAction($action["action"], $action["route"]["route"], $action["route"]["method"], $action["template"]);
                    $a->body($action["body"]);
                    $a->template_body($action["template_body"]);
                }
            }
        }
        $generator->generate();
    }

    /**
     * @return bool
     */
    private function post_update_config(){
        if ($file = $this->pkrs->config()->getConfigFile()){
            $c = [];
            foreach($_POST["config"] as $k=>$g){
                $c[$k] = [];
                foreach($g as $kk=>$i){
                   if ($i == "true") $i = true;
                   if ($i == "false") $i = false;
                    $c[$k][$kk] = $i;
                }
            }
            $f = new File($file);
            $f->open();
            $n = new Neon();
            $f->write($n->encode($c));
            $f->close();
            $_SESSION["mess"]["ok"][] = "Config file updated";
            return true;
        } else {
            $_SESSION["mess"]["err"][] = "Config file is not isset!";
        }
        return false;
    }

    /**
     * @return bool
     */
    private function post_edit_action(){
        $module = $_POST["module"];
        $presenter = $_POST["presenter"];
        if ($name = $this->sanitize($_POST["name"], false)){
            if (isset($this->g->modules[$module])){
                if (isset($this->g->modules[$module]["others"][$presenter])) {
                    $finded = false;
                    foreach($this->g->modules[$module]["others"][$presenter] as &$a){
                        if ($a["action"] == $name) {
                            $finded = true;
                            $a = [
                                "route" => [
                                    "route" => $_POST["route"],
                                    "method" => $_POST["method"]
                                ],
                                "template" => $_POST["template"],
                                "action" => $name,
                                "body" => implode(PHP_EOL,array_map(function($i){return rtrim(rtrim(rtrim($i,"\t")),PHP_EOL);},explode(PHP_EOL,$this->fix_brackets($_POST["body"])))),
                                "template_body" => $_POST["template_body"]
                            ];

                        }
                    }
                    if ($finded)
                    {
                        $_SESSION["mess"]["ok"][] = "Action $name updated";
                        return true;
                    }
                    else $_SESSION["mess"]["err"][] = "Action $name not exists!";
                    return false;
                } else $_SESSION["mess"]["err"][] = "Presenter $name not exists!";
            } else $_SESSION["mess"]["err"][] = "Module $_POST[module] not exists!";
        } else $_SESSION["mess"]["err"][] = "$_POST[name] is not valid action name!";
        return false;
    }

    /**
     * @return bool
     */
    private function post_new_action(){
        $module = $_POST["module"];
        $presenter = $_POST["presenter"];
        if ($name = $this->sanitize($_POST["name"], false)){
            if (isset($this->g->modules[$module])){
                if (isset($this->g->modules[$module]["others"][$presenter])) {
                    $this->g->modules[$module]["others"][$presenter][] = [
                        "route" => [
                            "route" => $_POST["route"],
                            "method" => $_POST["method"]
                        ],
                        "template" => $_POST["template"],
                        "action" => $name,
                        "body" => ""
                    ];
                    $_SESSION["mess"]["ok"][] = "Action $name created";
                    return true;
                } else $_SESSION["mess"]["err"][] = "Presenter $name not exists!";
            } else $_SESSION["mess"]["err"][] = "Module $_POST[module] not exists!";
        } else $_SESSION["mess"]["err"][] = "$_POST[name] is not valid action name!";
        return false;
    }

    /**
     * @return bool
     */
    private function post_delete_action(){
        $module = $_POST["module"];
        $presenter = $_POST["presenter"];
        if ($name = $this->sanitize($_POST["aname"], false)){
            if (isset($this->g->modules[$module])){
                if (isset($this->g->modules[$module]["others"][$presenter])) {
                    $finded = false;
                    foreach($this->g->modules[$module]["others"][$presenter] as $index => $a){
                        if ($a["action"] == $name) {
                            $finded = true;
                            unlink(THEMES_DIR.$module.DS.$a["template"]);
                            unset($this->g->modules[$module]["others"][$presenter][$index]);
                            $_SESSION["mess"]["ok"][] = "Action $name deleted";
                            return true;
                        }
                    }
                    $_SESSION["mess"]["err"][] = "Action $name not exists!";
                    return false;
                } else $_SESSION["mess"]["err"][] = "Presenter $name not exists!";
            } else $_SESSION["mess"]["err"][] = "Module $_POST[module] not exists!";
        } else $_SESSION["mess"]["err"][] = "$_POST[name] is not valid action name!";
        return false;
    }

    /**
     * @return bool
     */
    private function post_new_presenter(){
        $module = $_POST["module"];
        if ($name = $this->sanitize($_POST["name"])){
            if (isset($this->g->modules[$module])){
                if (!isset($this->g->modules[$module]["others"][$name])) {
                    $this->g->modules[$module]["others"][$name] = [];
                    $_SESSION["mess"]["ok"][] = "Presenter $name created";
                    return true;
                } else $_SESSION["mess"]["err"][] = "Presenter $name exists!";
            } else $_SESSION["mess"]["err"][] = "Module $_POST[module] not exists!";
        } else $_SESSION["mess"]["err"][] = "$_POST[name] is not valid class name!";
        return false;
    }

    /**
     * @return bool
     */
    private function post_new_module(){
        if ($name = $this->sanitize($_POST["name"])) {
            if (!isset($this->g->modules[$name])){
                $this->g->modules[$name] = [
                    "root" => [
                        "needLogin" => !!$_POST["needLogin"],
                        "theme" => (!$_POST["theme"] ? $name : $_POST["theme"]),
                    ], "others" => []
                ];
                $_SESSION["mess"]["ok"][] = "Module $name created";
                return true;
            } else $_SESSION["mess"]["err"][] = "Module $_POST[name] exists!";
        } else $_SESSION["mess"]["err"][] = "$_POST[name] is not valid class name!";
        return false;
    }

    /**
     * @return bool
     */
    private function post_edit_module(){
        if ($name = $this->sanitize($_POST["module"])) {
            if (isset($this->g->modules[$name])){
                $this->g->modules[$name]["root"] = [
                        "needLogin" => !!$_POST["needLogin"],
                        "theme" => (!$_POST["theme"] ? $name : $_POST["theme"]),
                        "layout" => $_POST["layout"]
                ];
                $_SESSION["mess"]["ok"][] = "Module $name updated";
                return true;
            } else $_SESSION["mess"]["err"][] = "Module $_POST[name] not exists!";
        } else $_SESSION["mess"]["err"][] = "$_POST[name] is not valid class name!";
        return false;
    }


    /**
     * @return bool
     */
    private function post_module_delete(){
            if (isset($this->g->modules[$_POST["module"]])){
                $this->rrmdir(THEMES_DIR.$this->g->modules[$_POST["module"]]["root"]["theme"]);
                unset($this->g->modules[$_POST["module"]]);
                $_SESSION["mess"]["ok"][] = "Module $_POST[module] deleted";
                return true;
            } else $_SESSION["mess"]["err"][] = "Module $_POST[module] not exists!";
        return false;
    }

    /**
     * @param $input
     * @param bool|true $ucfirst
     * @return bool|string
     */
    private function sanitize($input, $ucfirst = true){
        if(preg_match( '/^([\w\d_\x7f-\xff][\w\d_\x7f-\xff]*)$/', $input, $match )){
            return $ucfirst ? ucfirst($match[0]) : $match[0];
        }
        return false;
    }

    /**
     * @param $body string
     * @return string
     */
    private function fix_brackets($body){
        $opens = substr_count($body, "{");
        $closes = substr_count($body, "}");
        if ($opens == $closes) return $body;
        else if ($closes > $opens){
            // vice zavirani -> smazat odzadu
            $rev = strrev($body); // otocit
            $count = $closes - $opens;
            $rev = str_replace("}","",$rev,$count);
            $body = strrev($rev);
        } else if ($closes < $opens){
            $count = $opens - $closes;
            $body = str_replace("{","",$body, $count);
        }
        return $body;
    }

    /**
     * @param $dir
     */
    private function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); else unlink($dir."/".$object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
}