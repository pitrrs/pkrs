<?php
namespace PKRS\Generator;
use gossi\codegen\generator\CodeGenerator;
use gossi\codegen\model\PhpClass;
use gossi\codegen\model\PhpMethod;
use gossi\codegen\model\PhpParameter;
use gossi\codegen\model\PhpProperty;
use PKRS\Core\FileSystem\File;

/**
 * Class PresentersModuleSectionGenerator
 * @package PKRS\Generator
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class PresentersModuleSectionGenerator {

    var $name, $module, $namespace;
    var $actions = [];

    /**
     * @param $name
     * @param $namespace
     * @param $module
     */
    function __construct($name, $namespace, $module)
    {
        if (substr($name,strlen($name) - strlen("Presenter"), strlen("Presenter")) == "Presenter"){
            $this->name = substr($name,0, strlen($name) - strlen("Presenter"));
        }
        else $this->name = $name;
        $this->namespace = $namespace;
        $this->module = $module;
    }

    /**
     * @param $name
     * @param $route
     * @param string $route_method
     * @param null $template
     * @return PresentersModuleSectionActionGenerator
     */
    function addAction($name, $route, $route_method = "*", $template = null){
        $c = new PresentersModuleSectionActionGenerator($name, $this->module, ["route"=>$route,"method"=>$route_method], $template);
        $this->actions[] = $c;
        return $c;
    }

    function generate(){
        $f = new File(APP_DIR.$this->namespace.DS. $this->module .DS.$this->name."Presenter.php");
        $f->open();
        $c = new PhpClass();
        $constructor = new PhpMethod("__construct");
        $constructor->setBody("parent::__construct();");
        $c->setMethod($constructor);
        $c->setQualifiedName("\\".$this->namespace."\\".$this->module."\\".$this->name."Presenter");
        $c->setParentClassName("Root".$this->module."Presenter");

        foreach($this->actions as $a){
            if ($a instanceof PresentersModuleSectionActionGenerator){
                $c->setMethod($a->generate());
                if (!file_exists(THEMES_DIR.$this->module.DS.$a->template)){
                    $t = new File(THEMES_DIR.$this->module.DS.$a->template);
                    $t->open();
                    $t->write("{extends file='_layout.tpl'}\n{block name='content'}\n<h2>Template action ".$a->name."</h2>\n{/block}");
                    $t->close();
                } else {
                    $t = new File(THEMES_DIR.$this->module.DS.$a->template);
                    $t->open();
                    $t->write($a->template_body);
                    $t->close();
                }
            }
        }
        $generator = new CodeGenerator();
        $php = $generator->generate($c);
        $f->write("<?php".PHP_EOL.$php);
        $f->close();
       // echo "<h4>ROOT/".$this->module."/".$this->name."</h4><pre>" . $php ."<pre><hr>";
    }
    public function structure(){
        echo "<ul>";
        echo "<li>".$this->name;
        if ($this->actions) {
            echo "<ul>";
            foreach ($this->actions as $a) {
                echo "<li>".$a->name."(); (".$a->route["method"]."  /".$a->route["route"]." )</li>";
            }
            echo "</ul>";
        }
        echo "</li>";
        echo "</ul>";
    }

    /**
     * @return array
     */
    public function routes(){
        $routes = [];
        foreach($this->actions as $name => $module){
            $routes[] = [
                "method" => $module->route["method"],
                "route" => $module->route["route"],
                "target" => ["ns" => $this->module, "c"=>$this->name."Presenter", "a"=>$module->name],
                "name" => $this->module.$this->name.ucfirst($module->name)
            ];
        }
        return $routes;
    }
}