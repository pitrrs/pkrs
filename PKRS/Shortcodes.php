<?php
/**
 * SHORTCODES FOR CLASSES
 */
namespace PKRS;

use PKRS\Core\Object\Object;
use PKRS\Core\Presenters\AjaxPresenter;
use PKRS\Core\Presenters\BasePresenter;

/**
 * Class SPresenter
 * @package PKRS
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
abstract class SPresenter extends BasePresenter{}

/**
 * Class SAjaxPresenter
 * @package PKRS
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
abstract class SAjaxPresenter extends AjaxPresenter{}

/**
 * Class SObject
 * @package PKRS
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
abstract class SObject extends Object{}