<?php
/**
 * Created by PhpStorm.
 * User: pitrr
 * Date: 29.12.2015
 * Time: 16:52
 */

namespace PKRS\Core\Installer;


use PKRS\PKRS;

class Install
{

    /**
     * @param \Composer\Script\Event $event
     */
    public static function postInstall(\Composer\Script\Event  $event){
        try {
            $installedPackage = $event->getComposer()->getConfig()->get("vendor-dir");
            $event->getIO()->write("DIRECTORY: ".$installedPackage);
        } catch (\Exception $e){
            $event->getIO()->write($e->getMessage());
        }
    }

}