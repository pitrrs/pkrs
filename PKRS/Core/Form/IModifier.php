<?php
/**
 * Created by PhpStorm.
 * User: Pitrrs
 * Date: 16.3.2015
 * Time: 16:55
 */

namespace PKRS\Core\Form;


/**
 * Interface IModifier
 * @package PKRS\Core\Form
 */
interface IModifier {

    /**
     * Return false on failed
     *
     * @param $inputValue
     * @return mixed|false
     */
    public function doAction($inputValue);

}