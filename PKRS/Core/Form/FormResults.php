<?php
/**************************************************************
 *
 * FormResults.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form;

use PKRS\Core\Exception\MainException;
use PKRS\Core\Object\Object;

/**
 * Class FormResults
 * @package PKRS\Core\Form
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class FormResults extends Object{

    private $data = [];

    /**
     * @param array $data
     */
    public function __construct($data = []){
        $this->data = $data;
        foreach ($this->data as $key=>$value){
            $this->$key = $value;
        }
    }

    /**
     * @return array
     */
    public function getAll(){
        return $this->data;
    }

    /**
     * @param $key
     * @return mixed
     * @throws MainException
     */
    public function __get($key){
        if (isset($this->data[$key])){
            return $this->data[$key];
        } else throw new MainException("FormResults: Key $key is not defined");
    }

    /**
     * @param $key
     * @param $value
     */
    public function __set($key, $value){
        $this->$key = $value;
    }
}