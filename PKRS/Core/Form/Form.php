<?php
namespace PKRS\Core\Form;

use PKRS\Core\Exception\MainException;
use PKRS\Core\Form\Items\Checkbox;
use PKRS\Core\Form\Items\ReCaptcha;
use PKRS\Core\Router\Router;
use PKRS\Core\Service\Service;

/**
 * Class Form
 * @package PKRS\Core\Form
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Form extends Service{
    /**
     * DEFAULT ITEMS
     */
    const I_CHECKBOX = "Checkbox";
    const I_DATETIME = "Datetime";
    const I_FILE = "File";
    const I_INT = "Integer";
    const I_TEXT = "Text";
    const I_TEXTAREA = "Textarea";
    const I_PASSWORD = "Password";
    const I_RECAPTCHA = "ReCaptcha";
    /**
     * DEFAULT RULES
     */
    const R_COMPARE = "Compare";
    const R_FILE_CHECK = "FileCheck";
    const R_FILE_IMAGE_SIZE = "FileImageSize";
    const R_FILE_MUST = "FileMust";
    const R_FILE_TYPE = "FileType";
    const R_IN_ARRAY = "InArray";
    const R_MAIL = "Mail";
    const R_MAX_INT = "MaxInt";
    const R_MAX_LENGTH = "MaxLength";
    const R_MIN_INT = "MinInt";
    const R_MIN_LENGTH = "MinLength";
    const R_MUST_FILL = "MustFill";
    const R_CUSTOM = "Custom";
    /**
     * DEFAULT MODIFIERS
     */
    const M_FILE_RELATIVE_URL = "FileRelativeUrl";
    const M_FILE_UPLOAD = "FileUpload";

    private $smarty, $smartyForms, $messages;
    private $breakOnFirstError, $contextError = false;

    protected $name, $id, $method,$target;
    /**
     * @var Item[]
     */
    protected $items = [];
    protected $xssKey,$xssValue,$xssValid = false;
    protected $xssError = "";
    protected $callback = [];
    protected $results = [];
    protected $autoRules = [];
    protected $itemGetter, $ruleGetter, $modifierGetter;

    /**
     * @param $name
     * @param string $id
     * @param string $method
     * @param string $XSSError
     * @return Form
     */
    public function newForm($name, $id="form_ID",$method = "POST", $XSSError = "XSS protection: Please submit your form again."){
        return new Form($name, $id, $method, $XSSError);
    }

    public function setTarget($targetRoute, array $params = []){
        $this->target = self::gc()->getRouter()->generate($targetRoute, $params);
        return $this;
    }

    public function setContextError($enable = true){
        $this->contextError = !!$enable;
        return $this;
    }

    private function html($submitText = "Submit"){
        $html = "";
        $nm = md5($this->id.$this->name);
        $html .= "<script type='text/javascript' src='".PKRS_WEB_PATH."Core/Form/validator.js'></script>";
        $html .= "<form name='$nm' method='$this->method' action='$this->target' enctype='multipart/form-data'>";
        $html .= "<input type='hidden' name='$this->id' value='$this->name'>";
        $rules = [];

        foreach ($this->items as $item){
            $html .= "<div class='form-group ".(!empty($item->errors) ? "has-error" : "")."'>".($item instanceof Checkbox ? "<div class='checkbox'>":"").$item->getHtml().($item instanceof Checkbox ? "</div>":"")."</div>";
            $jsRules = [];
            foreach ($item->rules as $rule) {
                $rules[] = [
                    "name"=>$item->name,
                    "rules"=>$rule->getJSValidator(),
                    "cmessage"=>$rule->getMessage()
                ];
            }

        }
        $html.="<input type='submit' value='$submitText' class='btn btn-primary'></form>";
        $html .= '<script type="text/javascript">
            var val_'.$nm.' = new FormValidator("'.$nm.'", ';
        $html .= json_encode($rules);
        $html .= <<<JSVALIDATOR
            ,function(errors, event){
                for (var i = 0, f = this.fields.length; i < f; i++) {
                    $(this.fields[i].element).closest(".form-group").removeClass("has-error");
                    $(this.fields[i].element).parent().find("span.errmsg").remove();
                }
                if (errors.length > 0) {
                    for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
                        $(errors[i].element).closest(".form-group").addClass("has-error");
                        $(errors[i].element).parent().find("span.errmsg").remove();
                        $(errors[i].element).parent().append("<span class='help-block errmsg'>"+errors[i].cmessage+"</span>" );
                    }

                }
            });
            </script>
JSVALIDATOR;

        return $html;
    }

    /**
     * @param null $name
     * @param string $id
     * @param string $method
     * @param string $XSSError
     */
    public function __construct($name = null, $id="form_ID",$method = "POST", $XSSError = "XSS protection: Please submit your form again."){
        if ($name !== null){
            $this->smarty = self::gc()->getView()->smarty();
            $this->messages = self::gc()->getMessages();
            $this->itemGetter = new ItemGetter();
            $this->ruleGetter = new RuleGetter();
            $this->modifierGetter = new ModifierGetter();
            $this->name = $name;
            $this->id = $id;
            $this->method = $method;
            $this->xssKey = sha1($name.md5($id).strrev($method));
            $this->xssError = $XSSError;
            if (isset($_SESSION["xss_forms"][$this->xssKey])){
                // je nastaven XSS, muze probehnout validace
                $this->xssValue = $_SESSION["xss_forms"][$this->xssKey];
                $this->xssValid = true;
            }
            else {
                // nastavím XSS protekci
                //$this->xssValue = sha1(uniqid($this->xssKey));
                //$_SESSION["xss_forms"][$this->xssKey] = $this->xssValue;
            }
            $this->smartyForms = $this->smarty->getTemplateVars("F");
            if (!is_array($this->smartyForms))
                $this->smartyForms = [];
            if (!isset($this->smartyForms[$this->name]))
                $this->smartyForms[$this->name] = [
                    "_xss" => ["key"=>$this->xssKey,"value"=>$this->xssValue],
                    "_error"=> []
                ];
        }
    }

    /**
     * @param bool|true $break
     * @return $this
     */
    public function setBreakOnFirstError($break = true){
        $this->breakOnFirstError = !!$break;
        return $this;
    }

    /**
     * @param $rule
     * @param $value
     * @param $message
     * @return $this
     * @throws MainException
     */
    public function autoInjectRule($rule, $value, $message){
        $class = "\\PKRS\\Core\\Form\\Rules\\".$rule;
        if (class_exists($class))
            $this->autoRules[] = ["rule"=>$class,"value"=>$value, "message"=>$message];
        else throw new MainException("Auto-inject Rule $rule not exists!");
        return $this;
    }

    /**
     * @return $this
     */
    public function clearAutoInjectRule(){
        $this->autoRules = [];
        return $this;
    }

    /**
     * @param $callable
     * @return $this
     */
    public function addCallback($callable){
        if (is_callable($callable)) $this->callback[] = $callable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSubmitted(){
        if (!$this->xssValid) return false;
        if ($this->method == "POST"){
            return (isset($_POST[$this->id]) && $_POST[$this->id] == $this->name);
        } else if ($this->method == "GET"){
            return (isset($_GET[$this->id]) && $_GET[$this->id] == $this->name);
        } else return false;
    }

    /**
     * @return bool
     * @throws MainException
     */
    public function isValidated(){
        if (!$this->isSubmitted()) return false;
        $ok = true;
        foreach ($this->items as $item){
            if ($item instanceof Item){
                $item->setMethod($this->method);
                $item->setValue();
                if ($item->validate($this->breakOnFirstError)){
                    foreach($item->modifiers as $mod){
                        if ($mod instanceof Modifier) {
                            $item->value = $mod->doAction($item->getValue());
                            if ($mod->failed) {
                                $ok = false;
                                $this->messages->add($mod->errMessage, "err");
                            }
                        }
                    }
                    $this->results[$item->getName()] = $item->getValue();
                } else {
                    $ok = false;
                    $this->results[$item->getName()] = "";
                    $e = $item->getError();
                    if (trim($e) !== "") $this->messages->add($e,"err");
                    $this->smartyForms[$this->name]["_error"][$item->getName()] = true;
                }
                $this->smartyForms[$this->name][$item->getName()] = $this->results[$item->getName()];
                if ($ok === false && $this->breakOnFirstError) break;
            }
        }
        return $ok;
    }

    /**
     * @function: ok
     * @since: 0.1.5
     * @description: Shortcut for doProcess()
     * @return bool
     */
    public function ok(){
        return $this->doProcess() instanceof FormResults;
    }

    /**
     * @return FormResults
     */
    public function getResults(){
        return new FormResults($this->results);
    }

    /**
     * @function: doProcess
     * @since: 0.1.5
     * @description: Final process form & assign to \Smarty variable
     * @return null|FormResults
     */
    public function doProcess($submitButtonText = "Submit"){
        $e = false;
        if ((!isset($_SESSION["xss_forms"][$this->xssKey]) || $_SESSION["xss_forms"][$this->xssKey]!=$this->xssValue) && $this->isSubmitted())
        {
            $this->messages->add($this->xssError,"err");
            $e = true;
        } elseif (!$this->isValidated() ) {
            $e = true;
        }
        foreach($this->items as $item){
            if ($item instanceof ReCaptcha){
                $this->smartyForms[$this->name][$item->name] = $item->getHtml();
            }
        }
        $this->smartyForms[$this->name]["_html"] = $this->html($submitButtonText);
        $this->smarty->assign("F",$this->smartyForms);
        unset($_SESSION["xss_forms"][$this->xssKey]);
        $_SESSION["xss_forms"][$this->xssKey] = sha1(uniqid($this->xssKey));
        if ($e) return null;
        foreach($this->callback as $callable){
            call_user_func($callable, $this->getResults());
        }

        return $this->getResults();
    }

    /**
     * Function: item
     * Since: 0.1.4
     * Description:
     * @param $item_type
     * @param $item_name
     * @param bool $is_multi
     * @return Item
     */
    public function addItem($item_type, $item_name, $is_multi = false, $label = null){
        $class = $this->itemGetter->$item_type;
        $object = new $class($item_name, $is_multi, $label);
        $this->items[] = $object;
        return $object;
    }

}