<?php
/**************************************************************
 *
 * Item.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form;

use PKRS\Core\Object\Object;

/**
 * Class Item
 * @package PKRS\Core\Form
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
abstract class Item extends Object implements IItem{

    /**
     * @var Rule[]
     */
    var $rules = [];
    /**
     * @var Modifier[]
     */
    var $modifiers = [];
    var $name,$value, $method,$label,$placeholder,$infotext;
    var $isMulti = false;
    var $errors,$atts = [];
    var $ruleGetter,$modifierGetter;

    /**
     * @param $name
     * @param bool|false $isMulti
     */
    function __construct($name, $isMulti = false, $label = null)
    {
        $this->name = $name;
        $this->label = $label;
        $this->isMulti = !!$isMulti;
        $this->ruleGetter = new RuleGetter();
        $this->modifierGetter = new ModifierGetter();
    }

    function setValue($value = null)
    {
        if ($value)
            $this->value = $value;
        else
            $this->value = $this->method == "POST" ? (isset($_POST[$this->name]) ? $_POST[$this->name] : $this->value) : (isset($_GET[$this->name]) ? $_GET[$this->name] : $this->value);
        return $this;
    }

    public function setPlaceholder($placeholder){
        $this->placeholder = $placeholder;
        return $this;
    }

    public function setInfoText($infoText){
        $this->infotext = $infoText;
        return $this;
    }

    public function setAttr($attr, $value){
        $this->atts[$attr] = $value;
        return $this;
    }

    /**
     * @param $method
     */
    function setMethod($method){
        $this->method = $method;
    }

    function getName(){
        return $this->name;
    }

    /**
     * @param $ruleName
     * @param $value
     * @param $message
     * @return $this
     */
    public function addRule($ruleName, $value, $message, $customJSValidator = null){
        $class = $this->ruleGetter->$ruleName;
        $object = new $class($value, $message, $customJSValidator);
        $this->rules[] = $object;
        return $this;
    }

    /**
     * @param Rule $customRuleObject
     * @return $this
     */
    function addCustomRule(Rule $customRuleObject){
        $this->rules[] = $customRuleObject;
        return $this;
    }

    /**
     * @param $modifierName
     * @param $value
     * @param $message
     * @return $this
     */
    function addModifier($modifierName, $value, $message){
        $class = $this->modifierGetter->$modifierName;
        $object = new $class($value, $message);
        $this->modifiers[] = $object;
        return $this;
    }

    /**
     * @param Modifier $customModifierObject
     * @return $this
     */
    function addCustomModifier(Modifier $customModifierObject){
        $this->modifiers[] = $customModifierObject;
        return $this;
    }

    /**
     * @param bool|false $breakOnFirstError
     * @return bool
     */
    public function validate($breakOnFirstError = false){
        $ok = true;
        foreach ($this->rules as $rule){
            if ($rule instanceof Rule){
                $rule->setValue($this->getValue());
                if (!$rule->validate()) {
                    $this->errors[] = $rule->getError();
                    $ok = false;
                    if ($breakOnFirstError) break;
                }
            }
        }
        return $ok;
    }

    /**
     * @return string
     */
    function getError()
    {
        if (is_array($this->errors))
            return implode("<br>",$this->errors);
        else return strval($this->errors);
    }
}