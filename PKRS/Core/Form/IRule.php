<?php
/**************************************************************
 *
 * IItem.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form;

/**
 * Interface IRule
 * @package PKRS\Core\Form
 */
interface IRule{

    /**
     * @param $value
     * @param $message
     */
    function __construct($value, $message);

    function validate();

    function getError();

    function getJSValidator();

}