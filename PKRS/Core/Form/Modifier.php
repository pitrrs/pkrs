<?php
namespace PKRS\Core\Form;


use PKRS\Core\Object\Object;

/**
 * Class Modifier
 * @package PKRS\Core\Form
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
abstract class Modifier extends Object implements IModifier {

    var $value;
    var $errMessage;
    var $failed = false;

    /**
     * @param $value
     * @param $err_message
     */
    final function __construct($value, $err_message){
        $this->value = $value;
        $this->errMessage = $err_message;
    }

}