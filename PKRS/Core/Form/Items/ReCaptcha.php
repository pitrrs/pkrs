<?php
/**************************************************************
 *
 * Text.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Items;

use PKRS\Core\Exception\MainException;
use \PKRS\Core\Form\Item;
use PKRS\Core\Service\Service;

/**
 * Class ReCaptcha
 * @package PKRS\Core\Form\Items
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class ReCaptcha extends Item{

    protected $conf,$errMessage,$code;

    final public function __construct($name, $errMessage, $label = null)
    {
        parent::__construct($name, false, $label);
        $this->errMessage = $errMessage;
        $config = Service::gc()->getConfig()->getConfGroup("recaptcha");
        if (!$config || !is_array($config) || !isset($config["key"]) || !isset($config["secret"]))
            throw new MainException("ReCaptcha needs 'recaptcha' config group with 'key' and 'secret' values. Register it on https://www.google.com/recaptcha/ and enter to your config file.");
        $this->conf = $config;
        $this->code = '<script src="https://www.google.com/recaptcha/api.js"></script><div class="g-recaptcha" data-sitekey="'.$this->conf["key"].'"></div>';
        $this->value = $this->code;
        $sm = Service::gc()->getView();
        $f = $sm->smarty->getTemplateVars("F");
        $f["recaptcha"] = $this->code;
        $sm->smarty->assign("F",$f);
    }

    function setValue($value = null)
    {
        $this->value = $this->code;
        return $this;
    }

    function getValue()
    {
        return $this->code;
    }

    function validate(){

        $recaptcha = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$this->conf["secret"].'&response=' . ($this->method == "POST" ? $_POST["g-recaptcha-response"] : $_GET["g-recaptcha-response"])));
        if (isset($recaptcha->success) && boolval($recaptcha->success)) return true;
        Service::gc()->getMessages()->add($this->errMessage,"err");
        return false;
    }

    function getHtml()
    {
      return $this->code;
    }
}