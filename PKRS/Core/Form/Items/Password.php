<?php
/**************************************************************
 *
 * Text.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Items;

use \PKRS\Core\Form\Item;

/**
 * Class Text
 * @package PKRS\Core\Form\Items
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Password extends Item{

    /**
     * @return string
     */
    function getValue()
    {
        return trim(str_replace(["\n", "\t"]," ",$this->value));
    }
    function getHtml()
    {
        $html = "<label class='control-label' for='$this->name'>$this->label</label><input type='password' class='form-control' name='$this->name'  id='item_$this->name' value='$this->value'";
        foreach($this->atts as $k=>$v)
            $html .= " $k=\"$v\" ";
        return $html ."> ";
    }
}