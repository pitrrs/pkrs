<?php
/**************************************************************
 *
 * Textarea.php, created 26.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Items;

use PKRS\Core\Form\Item;

/**
 * Class Textarea
 * @package PKRS\Core\Form\Items
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Textarea extends Item{

    /**
     * @return string
     */
    function getValue()
    {
        return "".$this->value;
    }
    function getHtml()
    {
        $html = "<label class='control-label' for='item_$this->name'>$this->label</label><textarea name='$this->name'  id='item_$this->name' class='form-control' placeholder='$this->placeholder'";
        foreach($this->atts as $k=>$v)
            $html .= " $k=\"$v\" ";
        $html .= ">$this->value</textarea>";
        return $html;
    }
}