<?php
/**************************************************************
 *
 * Text.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Items;

use \PKRS\Core\Form\Form;
use \PKRS\Core\Form\Item;
use PKRS\Core\Form\Rules\FileCheck;
use PKRS\Core\Service\Service;

/**
 * Class File
 * @package PKRS\Core\Form\Items
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class File extends Item{

    function setValue(){
        $all = Service::gc()->getTransformArrays()->getDiverseFiles($_FILES);
        $this->value = [
            "name" => $all["name"][$this->name],
            "type" => $all["type"][$this->name],
            "tmp_name" => $all["tmp_name"][$this->name],
            "size" => $all["size"][$this->name],
            "error" => $all["error"][$this->name],
        ];
        $this->isMulti = is_array($all["name"][$this->name]);
    }

    function getValue()
    {
        return $this->value;
    }

    /**
     * @return bool
     */
    function validate(){
        $injected = false;
        foreach($this->rules as $rule){
            if ($rule instanceof FileCheck){
                $injected = true;
            }
        }
        if (!$injected) $this->addRule(Form::R_FILE_CHECK, null, "Not valid input file data");
        return parent::validate();
    }

    function getHtml()
    {
        $html = "<label class='control-label' for='item_$this->name'>$this->label</label><input type='file' class='form-control' name='$this->name'  id='item_$this->name' value='$this->value' ";
        foreach($this->atts as $k=>$v)
            $html .= " $k=\"$v\" ";
        return $html ."> ";
    }
}