<?php
/**************************************************************
 *
 * Text.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Items;

use \PKRS\Core\Form\Item;

/**
 * Class Checkbox
 * @package PKRS\Core\Form\Items
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Checkbox extends Item{

    /**
     * @return array|bool
     */
    function getValue()
    {
        if ($this->isMulti && is_array($this->value)){
            $ret = [];
            foreach($this->value as $k=>$v){
                $ret[$k] = !!$v !== false ? $v : false;
            }
            return $ret;
        }
        return !!$this->value !== false ? $this->value : false ;
    }

    function getHtml()
    {
        $html = "<label class='control-label' for='item_$this->name'><input type='checkbox' name='$this->name'  id='item_$this->name' ".($this->value ? "value='$this->value'" : "");
        foreach($this->atts as $k=>$v)
            $html .= " $k=\"$v\" ";
        return $html ." > $this->label</label>";

    }
}