<?php
/**************************************************************
 *
 * Rule.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form;

use PKRS\Core\Object\Object;

/**
 * Class Rule
 * @package PKRS\Core\Form
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
abstract class Rule extends Object implements IRule{

    var $_value;
    var $value, $message;
    var $customJSValidator;

    /**
     * @param $value
     */
    public function setValue($value){
        $this->_value = $value;
    }

    public function getMessage(){
        return $this->message;
    }
}