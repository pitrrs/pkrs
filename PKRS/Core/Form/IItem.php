<?php
/**************************************************************
 *
 * IItem.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form;

/**
 * Interface IItem
 * @package PKRS\Core\Form
 */
interface IItem{

    /**
     * @param $name
     */
    function __construct($name, $isMulti, $lanel);

    function validate();

    function setValue();

    function getName();

    function getValue();

    /**
     * @param $ruleName
     * @param $value
     * @param $message
     * @return mixed
     */
    function addRule($ruleName, $value, $message);

    function getError();

    function getHtml();
}