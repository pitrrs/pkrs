<?php
/**************************************************************
 *
 * Mail.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Rules;

use PKRS\Core\Form\Rule;

/**
 * Class FileType
 * @package PKRS\Core\Form\Rules
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class FileType extends Rule{

    /**
     * @param null|array $value (array of file extensions)
     * @param $message
     */
    function __construct($value = null, $message, $customJSValidator = null)
    {
        if (is_array($value)) $value = array_map("strtolower",$value);
        else if (is_string($value)) $value = [strtolower($value)];
        else $value = [];
        $this->value = $value;
        $this->message = $message;
        $this->customJSValidator = $customJSValidator;
    }

    /**
     * @return bool
     */
    function validate()
    {
        if ($this->value === null || empty($this->value) || (is_array($this->_value) && isset($this->_value["error"]) && $this->_value["error"]==UPLOAD_ERR_NO_FILE)) return true;
        if (is_array($this->_value["type"])){
            foreach($this->_value["error"] as $k=>$e){
                if ($e == UPLOAD_ERR_NO_FILE) return false;
                $xt = strtolower(pathinfo($this->_value["name"][$k])["extension"]);
                if (!in_array($xt, $this->value)) return false;
            }
            return true;
        } else {
            if ($this->_value["error"] != UPLOAD_ERR_NO_FILE){
                $xt = strtolower(pathinfo($this->_value["name"])["extension"]);
                return in_array($xt, $this->value);
            } else return false;
        }
    }

    /**
     * @return null
     */
    function getError()
    {
        if (!$this->validate()) return $this->message;
        else return null;
    }


    function getJSValidator(){
        if ($this->customJSValidator !== null)
            return $this->customJSValidator;
        return "is_file_type[".implode(",",$this->value)."]";
    }
}