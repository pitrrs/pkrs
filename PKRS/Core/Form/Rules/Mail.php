<?php
/**************************************************************
 *
 * Mail.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Rules;

use PKRS\Core\Form\Rule;

/**
 * Class Mail
 * @package PKRS\Core\Form\Rules
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Mail extends Rule{

    /**
     * @param null $value
     * @param $message
     */
    function __construct($value = null, $message, $customJSValidator = null)
    {
        $this->value = $value;
        $this->message = $message;
        $this->customJSValidator = $customJSValidator;
    }

    /**
     * @return mixed
     */
    function validate()
    {
        return filter_var($this->_value, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @return null
     */
    function getError()
    {
        if (!$this->validate()) return $this->message;
        else return null;
    }


    function getJSValidator(){
        if ($this->customJSValidator !== null)
            return $this->customJSValidator;
        return "valid_email";
    }
}