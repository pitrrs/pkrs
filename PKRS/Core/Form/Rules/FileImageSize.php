<?php
/**************************************************************
 *
 * Mail.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Rules;

use PKRS\Core\Form\Rule;

/**
 * Class FileImageSize
 * @package PKRS\Core\Form\Rules
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class FileImageSize extends Rule{

    /**
     * @param array $value (array of image size array(0=>vidth,1=>height))
     * @param $message
     */
    function __construct($value = [-1,-1], $message, $customJSValidator = null)
    {
        $this->value = $value;
        $this->message = $message;
        $this->customJSValidator = $customJSValidator;
    }

    /**
     * @return bool
     */
    function validate()
    {
        if (is_string($this->_value["tmp_name"]) && file_exists($this->_value["tmp_name"])){
            $s = getimagesize($this->_value["tmp_name"]);
            return $s[0] == $this->value[0] && $s[1] == $this->value[1];
        } else if (is_array($this->_value["tmp_name"])){
            foreach ($this->_value["tmp_name"] as $tmp) {
                if (!file_exists($tmp)) return false;
                $s = getimagesize($tmp);
                if (!$s[0] == $this->value[0] && $s[1] == $this->value[1]){
                    return false;
                }
            }
            return true;
        } else return false;
    }

    /**
     * @return null
     */
    function getError()
    {
        if (!$this->validate()) return $this->message;
        else return null;
    }


    function getJSValidator(){
        if ($this->customJSValidator !== null)
            return $this->customJSValidator;
        return "";
    }
}