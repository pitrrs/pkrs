<?php
/**************************************************************
 *
 * Mail.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Rules;

use PKRS\Core\Exception\MainException;
use PKRS\Core\Form\Rule;

/**
 * Class Custom
 * @package PKRS\Core\Form\Rules
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Custom extends Rule{

    /**
     * @param $callable
     * @param $message
     * @throws MainException
     */
    function __construct($callable, $message, $customJSValidator = null)
    {
        if (!is_callable($callable))
            throw new MainException("Gived function/param to rule is not callable!");
        $this->value = $callable;
        $this->message = $message;
        $this->customJSValidator = $customJSValidator;
    }

    /**
     * @return mixed
     */
    function validate()
    {
        return call_user_func($this->value, $this->_value);
    }

    /**
     * @return null
     */
    function getError()
    {
        if (!$this->validate()) return $this->message;
        else return null;
    }


    function getJSValidator(){
        if ($this->customJSValidator !== null)
            return $this->customJSValidator;
        return "";
    }
}