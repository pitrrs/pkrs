<?php
/**************************************************************
 *
 * Mail.php, created 19.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form\Rules;

use PKRS\Core\Form\Rule;

/**
 * Class InArray
 * @package PKRS\Core\Form\Rules
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class InArray extends Rule{

    /**
     * @param array $array
     * @param $message
     */
    function __construct($array = [], $message, $customJSValidator = null)
    {
        $this->value = (array)$array;
        $this->message = $message;
        $this->customJSValidator = $customJSValidator;
    }

    /**
     * @return bool
     */
    function validate()
    {
        if (is_scalar($this->_value))
            return in_array($this->_value."", array_map(function($input){return "".$input;},$this->value));
        else if (is_array($this->_value)){
            $r = true;
            foreach($this->_value as $v){
                if (!in_array($v, $this->value)){
                    $r = false;
                    break;
                }
            }
            return $r;
        }
        return false;
    }

    /**
     * @return null
     */
    function getError()
    {
        if (!$this->validate()) return $this->message;
        else return null;
    }

    function getJSValidator(){
        if ($this->customJSValidator !== null)
            return $this->customJSValidator;
        return "";
    }
}