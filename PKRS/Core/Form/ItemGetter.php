<?php
/**************************************************************
 *
 * ItemGetter.php, created 26.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Form;

use PKRS\Core\Exception\MainException;
use \PKRS\Core\Form\Items as I;
use PKRS\Core\Object\Object;

/**
 * Class ItemGetter
 * @package PKRS\Core\Form
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class ItemGetter extends Object{

    /**
     * @param $modifierName
     * @return string
     * @throws MainException
     */
    final public function __get($modifierName){
        if (class_exists("\\PKRS\\Core\\Form\\Items\\".$modifierName)){
            return "\\PKRS\\Core\\Form\\Items\\".$modifierName;
        } else throw new MainException("Item $modifierName not exists!");
    }

}