<?php
/**
 * Created by PhpStorm.
 * User: Pitrrs
 * Date: 16.3.2015
 * Time: 17:01
 */

namespace PKRS\Core\Form\Modifiers;

use PKRS\Core\Form\Modifier;

/**
 * Class FileUpload
 * @package PKRS\Core\Form\Modifiers
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class FileUpload extends Modifier {

    /**
     * @param $inputValue
     * @return array|null|string
     */
    public function doAction($inputValue)
    {
        if (!is_dir($this->value)){
            if (!mkdir($this->value, 0777, true))
            {
                $this->failed = true;
                return $inputValue;
            }
        }
        $this->value = rtrim($this->value,DS).DS;
        if (is_string($inputValue["name"])){
            if ($inputValue["error"] == UPLOAD_ERR_NO_FILE) return null;
            $ext = pathinfo($inputValue["name"])["extension"];
            $target = $this->value.sha1(uniqid(rand(0,99999))).".".$ext;
            if (!$this->upload($inputValue["tmp_name"],$target)){
                $this->failed = true;
                return $inputValue;
            }
            return $target;
        } else if (is_array($inputValue["name"])){
            $targets = [];
            foreach ($inputValue["name"] as $k=>$v) {
                if ($v["error"] == UPLOAD_ERR_NO_FILE) continue;
                $ext = pathinfo($v)["extension"];
                $target = $this->value.sha1(uniqid(rand(0,99999))).".".$ext;
                if (!$this->upload($inputValue["tmp_name"][$k],$target)){
                    $this->failed = true;
                    return $inputValue;
                } else $targets[] = $target;
            }
            return $targets;
        } else {
            $this->failed = true;
            return $inputValue;
        }

    }

    /**
     * @param $source
     * @param $target
     * @return bool
     */
    private function upload($source, $target){
        return move_uploaded_file($source, $target);
    }
}