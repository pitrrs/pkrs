<?php
/**
 * Created by PhpStorm.
 * User: Pitrrs
 * Date: 16.3.2015
 * Time: 18:32
 */

namespace PKRS\Core\Form\Modifiers;


use PKRS\Core\Form\Modifier;

/**
 * Class FileRelativeUrl
 * @package PKRS\Core\Form\Modifiers
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class FileRelativeUrl extends Modifier{

    /**
     * @param $inputValue
     * @return array
     */
    public function doAction($inputValue)
    {
        $s = false;
        if (is_string($inputValue)) {
            $s = true;
            $inputValue = [$inputValue];
        }
        if (is_array($inputValue)) {
            foreach ($inputValue as $k => $v) {
                $inputValue[$k] = $this->checkAndReplace($v);
            }
        }
        return $s ? $inputValue[0] : $inputValue;
    }

    /**
     * @param $input_value
     * @return mixed
     */
    private function checkAndReplace($input_value){
        if (!is_string($input_value) || !file_exists($input_value)){
            $this->failed = true;
            return $input_value;
        } else {
            return str_replace(DS,"/",str_replace(ROOT_DIR,"/", $input_value));
        }
    }
}