<?php
/********************************************
 *
 * DatabseFluent.php, created 6.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Database;

use PKRS\Core\Debug\Debug;
use PKRS\Core\Object\Object;

/**
 * Class DatabaseFluent
 * @package PKRS\Core\Database
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class DatabaseFluent extends Object
{

    private $fluent;
    private $id;
    private $atts = [];

    /**
     * @param \PDOStatement $fluent
     * @param string $id
     */
    public function __construct(\PDOStatement $fluent, $id = "")
    {
        $this->fluent = $fluent;
        $this->id = $id;
    }

    /**
     * @return \PDOStatement
     */
    public function fluent()
    {
        return $this->fluent;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function int($name, $value)
    {
        $value = intval($value);
        $this->atts[$name] = $value;
        $this->fluent->bindParam(":" . $name, $value, \PDO::PARAM_INT);
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function float($name, $value)
    {
        $value = floatval($value);
        $this->atts[$name] = $value;
        $this->fluent->bindParam(":" . $name, $value, \PDO::PARAM_STR);
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function bool($name, $value)
    {
        $value = !!($value);
        $this->atts[$name] = $value;
        $this->fluent->bindParam(":" . $name, $value, \PDO::PARAM_BOOL);
        return $this;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    public function str($name, $value)
    {
        $this->atts[$name] = $value;
        $this->fluent->bindParam(":" . $name, $value, \PDO::PARAM_STR);
        return $this;
    }

    /**
     * @return bool
     */
    public function execute()
    {
        $res = $this->fluent->execute();
        Debug::logMysqlResult($res, $this->id, $this->atts);
        if (Debug::$enabled){

        }
        return $res;
    }

    /**
     * @param bool|true $array
     * @return mixed
     */
    public function row($array = true)
    {
        $this->fluent->execute();
        if ($array) {
            $res = $this->fluent->fetch(\PDO::FETCH_ASSOC);
        } else
            $res = $this->fluent->fetch(\PDO::FETCH_OBJ);
        //$call = debug_backtrace(null,1);
        Debug::logMysqlResult($res, $this->id, $this->atts);
        return $res;
    }

    /**
     * @param bool|true $array
     * @param bool|true $log
     * @return array
     */
    public function all($array = true, $log = true)
    {
        $this->fluent->execute();
        if ($array)
            $res = $this->fluent->fetchAll(\PDO::FETCH_ASSOC);
        else
            $res = $this->fluent->fetchAll(\PDO::FETCH_OBJ);
        if ($log)
            Debug::logMysqlResult($res, $this->id, $this->atts);
        return $res;
    }

    /**
     * @param bool|false $as_int
     * @return int|string
     */
    public function one($as_int = false)
    {
        $this->fluent->execute();
        $res = $as_int ? intval($this->fluent->fetchColumn()) : $this->fluent->fetchColumn();
        Debug::logMysqlResult($res, $this->id, $this->atts);
        return $res;
    }
    
        /**
     * returns an associated array by $key => $value
     * If $value === NULL => returns complete row
     * If $value === one key => returns single array
     * If $vallue === array => returns multidimensional array with results
     *
     * Returns false if key is not finded in array
     *
     * @param $key
     * @param null $value
     * @return array|bool
     */
    public function assoc($key, $value = null){
        $res = $this->all(true, false);
        $ret = [];
        foreach($res as $item){
            if (!isset($item[$key])) return false;
            if ($value === null){
                $ret[ $item[$key] ] = $item;
            } elseif (is_string($value)){
                if (!isset($item[$value])) return false;
                $ret[ $item[$key] ] = $item[$value];
            } elseif(is_array($value)){
                $ret[$item[$key]] = [];
                foreach ($value as $val){
                    if (!isset($item[$val])) return false;
                    $ret[$item[$key]][$val] = $item[$val];
                }
            }
        }
        Debug::logMysqlResult($ret, $this->id, $this->atts);
        return $ret;
    }

}