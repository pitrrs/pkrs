<?php
namespace PKRS\Core\Database;


class Pagination{
    var $limit = 8;
    var $pageNow = 1;
    var $offset = 0;
    var $pages = 0;
    var $route, $route_params;
    private $name;
    /**
     * @var Pagination[]
     */
    private static $instances = [];

    public function toSmarty(\Smarty $smarty){
        $smarty->assign([
            "_p_limit" => $this->limit,
            "_p_pageNow" => $this->pageNow,
            "_p_offset" => $this->offset,
            "_p_pages" => $this->pages,
            "_p_route" => $this->route,
            "_p_route_params" => $this->route_params,
        ]);
    }

    public function __construct($name, $route, $count, $limit = 8, $route_params = [])
    {
        $this->name = $name;
        $this->route = $route;
        $this->route_params = $route_params;
        $this->calc($count, $limit);
    }

    public function sqlLimit()
    {
        return " LIMIT " . $this->offset.",".$this->limit;
    }

    public function calc($count, $limit = 8)
    {
        $now = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
        if ($now<1) $now = 1;
        $this->limit = $limit;
        $this->pageNow = $now;
        $this->pages = ceil($count / $this->limit);
        $this->offset = ($this->pageNow - 1)*$this->limit;
        self::$instances[$this->name] = $this;
    }

    /**
     * @param $name
     * @return Pagination|false
     */
    public static function instance($name)
    {
        if (isset(self::$instances[$name]))
            return self::$instances[$name];
        return false;
    }
}