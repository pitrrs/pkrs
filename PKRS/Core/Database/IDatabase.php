<?php
/**************************************************************
 *
 * IDatabase.php, created 13.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Database;

/**
 * Interface IDatabase
 * @package PKRS\Core\Database
 */
interface IDatabase{

    /**
     *
     */
    public function __construct();

    /**
     * @return Database
     */
    public static function getInstance();

    /**
     * @return \PDO
     */
    public function getConnection();

    /**
     * @return \PDOStatement
     */
    public function getLastFluent();

    /**
     * @param $query string
     * @return \PKRS\Core\Database\DatabaseFluent
     */
    public function query($query);

    /**
     * @return int
     */
    public function getLastId();

}