<?php
/********************************************
 *
 * Database.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Database;

use PKRS\Core\Database\DatabaseFluent;
use PKRS\Core\Debug\Debug;
use PKRS\Core\Service\Service;

/**
 * Class Database
 * @package PKRS\Core\Database
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Database extends Service implements IDatabase
{

    private static $selfie;
    var $conn;
    private $last_fluent = null;

    /**
     *
     */
    public function __construct()
    {

        self::$selfie = $this;
    }

    private function connect(){
        $config = self::gc()->getConfig();
        try {
            if ($config->getConf("database", "dsn")){
                $this->conn = new \PDO($config->getConf("database", "dsn"), $config->getConf("database", "user"), $config->getConf("database", "pass"), [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
            } else $this->conn = new \PDO($config->getConf("database", "type") . ':dbname=' . $config->getConf("database", "name") . ';host=' . $config->getConf("database", "host"), $config->getConf("database", "user"), $config->getConf("database", "pass"), [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
            self::gc()->getHooks()->execute("database","on_connect");
        } catch (\PDOException $e) {
            throw new \PKRS\Core\Exception\DbException("Cant connect to database (" . $e->getMessage() . ")");
        }
    }

    /**
     * Function: testConnection
     * Since: 0.1.3
     * Description: Test database connection
     *
     * @param array $credentials
     * @return bool
     */
    public static function testConnection(array $credentials = []){
        try {
            if (empty($credentials)){
                $config = self::gc()->getConfig();
                $conn = new \PDO($config->getConf("database", "type") . ':dbname=' . $config->getConf("database", "name") . ';host=' . $config->getConf("database", "host"), $config->getConf("database", "user"), $config->getConf("database", "pass"), [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
                $conn = null;
                return true;
            } else {
                $conn = new \PDO($credentials["type"] . ':dbname=' . $credentials["name"] . ';host=' . $credentials["host"], $credentials["user"],$credentials["pass"], [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]);
                $conn = null;
                return true;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Close connection
     */
    public function __destruct(){
        $this->conn = null;
    }

    /**
     * @return Database
     */
    public static function getInstance()
    {
        return self::$selfie;
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->conn;
    }

    /**
     * @return \PDOStatement
     */
    public function getLastFluent()
    {
        return $this->last_fluent;
    }

    /**
     * @param $query string
     * @return DatabaseFluent
     */
    public function query($query, Pagination $pagination = null)
    {
        if ($this->conn == null)
            $this->connect();
        if ($pagination instanceof Pagination){
            $query .= $pagination->sqlLimit();
        }
        self::gc()->getHooks()->execute("database","on_query");
        $id = uniqid("query");
        Debug::logMysql($query, $id);
        $fluent = $this->conn->prepare($query);
        $this->last_fluent = $fluent;
        return new DatabaseFluent($fluent, $id);
    }

    /**
     * @return mixed
     */
    public function getLastId()
    {
        if ($this->conn == null)
            return null;
        return $this->conn->lastInsertId();
    }

    public function getPagination($name, $route, $count, $limit = 8, $route_params = [])
    {
        return new Pagination($name, $route, $count, $limit, $route_params );
    }

}
