<?php
/********************************************
 *
 * Application.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core;

use PKRS\Core\Exception\FileException;
use PKRS\Core\Exception\MainException;
use PKRS\Core\Headers\Redirects;
use PKRS\Core\Service\ServiceContainer;
use PKRS\Core\User\Auth;

/**
 * Class Application
 * @package PKRS\Core
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Application
{

    private $config;
    private $router;
    private $serviceContainer;

    /**
     * @param ServiceContainer $serviceContainer
     */
    public function __construct(ServiceContainer $serviceContainer)
    {
        $this->serviceContainer = $serviceContainer;
        $this->config = $serviceContainer->getConfig();
        $this->router = $serviceContainer->getRouter();
        $serviceContainer->getHooks()->execute("application", "on_create");
    }

    public function run()
    {
        try {
            $this->serviceContainer->getHooks()->execute("application", "on_start");
            $route = $this->router->match();
            if (!$route) {
                if (defined("DEV") && DEV) {
                    throw new MainException("Aplication: Route " . trim($_SERVER["REQUEST_URI"], "/") . " (method $_SERVER[REQUEST_METHOD]) not exists");
                }
                Redirects::e404($this->config->getConf("application", "base_path"), $this->config->getConf("application","page_404"));
            } else {
                $route["target"]["ns"] = rtrim(str_replace("/","\\",$route["target"]["ns"]),"\\");
                if (substr($route["target"]["ns"],0,1) != "\\")
                    $className = "\\Presenters\\" . $route["target"]["ns"] . "\\" . $route["target"]["c"];
                else $className = $route["target"]["ns"]."\\".$route["target"]["c"];
                $method_name = $route["target"]["a"];
                define("ROUTE_CALL_CLASS",$className);
                define("ROUTE_CALL_METHOD",$method_name);
                if (class_exists($className) && method_exists($className,"_create")) {
                    $c = $className::_create($route["params"]);
                    if (is_subclass_of($c, "\\PKRS\\Core\\Presenters\\BasePresenter")) {
                        if (method_exists($c, $method_name)) {
                            if (method_exists($c, "privatePostConstruct"))
                                $c->privatePostConstruct();
                            if (method_exists($c, "postConstruct"))
                                $c->postConstruct();
                            $c->$method_name($route["params"]);
                            if (method_exists($c, "beforeDisplay"))
                                $c->beforeDisplay();
                            if (method_exists($c, "display"))
                                $c->display();
                            Auth::deinit();
                        } else {
                            $className::_handleError(new \Exception("Method $method_name not exists in $className!"));
                        }
                    } else {
                        if (defined("DEV") && DEV) {
                            throw new FileException("Application: Class $className is not instance of \\PKRS\\Controller\\Controller()");
                        } else Redirects::e404($this->config->getConf("application", "base_path"), $this->config->getConf("application", "page_404"));
                    }
                } else {
                    if (defined("DEV") && DEV) {
                        throw new FileException("Application: Class $className not exists");
                    } else Redirects::e404($this->config->getConf("application", "base_path"), $this->config->getConf("application", "page_404"));
                }
            }
        } catch (\Exception $e) {
            throw new MainException($e->getMessage(), $e->getCode(), $e);
        }
        $this->serviceContainer->getHooks()->execute("application", "on_exit");
    }

}