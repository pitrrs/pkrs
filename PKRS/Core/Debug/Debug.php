<?php
/********************************************
 *
 * Debug.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Debug;

use PKRS\Core\FileSystem\File;
use PKRS\Core\Service\Service;

/**
 * Class Debug
 * @package PKRS\Core\Debug
 */
class Debug extends Service
{

    static $enabled = false;
    static $logs = [];
    static $mysql = [];
    static $ajax = [];
    static $detailed = [];
    static $header = false;
    static $footer = false;
    static $dumps = [];
    static $dumpsExtra = [];

    /**
     * Cosntructor
     */
    public function __construct()
    {
        self::gc()->getHooks()->registerAction("application", "on_error", ["\\PKRS\\Core\\Debug\\Debug", "show"], [], 0);
        self::gc()->getHooks()->registerAction("application", "on_exit", ["\\PKRS\\Core\\Debug\\Debug", "show"], [], 0);
        if (defined("DEV") && DEV === true && !IS_AJAX) {
            self::$enabled = true;
        }
        $php_old_error_handler = set_error_handler("pkrsErrorHandler", E_ALL);
    }

    /**
     * @function: add_dump
     * @since: 0.1.5
     * @description: Add custom dump variable (if $extra is true, creates extra panel)
     * @param $name
     * @param $var
     * @param bool $extra
     * @return void
     */
    static function addDump($name, $var, $extra = false)
    {
        if (self::$enabled === false) return;
        if ($extra)
            self::$dumpsExtra[] = ["name" => $name, "var" => $var];
        else
            self::$dumps[] = ["name" => $name, "var" => $var];
    }

    /**
     * @function: logMysql
     * @since: 0.1.5
     * @description: Log mysql query
     * @param $query
     * @param $id
     * @return void
     */
    static function logMysql($query, $id)
    {
        if (self::$enabled === false) return;
        self::$mysql[$id] = ["query" => $query, "result" => null, "time" => microtime(true)];
        $trace = debug_backtrace(null, 2);
        self::$mysql[$id]["trace"] = @end($trace);
    }

    /**
     * @function: logMysqlResult
     * @since: 0.1.5
     * @description: Log mysql results
     * @param $result
     * @param $id
     * @return void
     */
    static function logMysqlResult($result, $id, $params = [])
    {
        if (self::$enabled === false) return;
        self::$mysql[$id]["result"] = $result;
        self::$mysql[$id]["time"] = round((microtime(true) - self::$mysql[$id]["time"]), 4);
        self::$mysql[$id]["params"] = $params;
    }

    /**
     * @function: log
     * @since: 0.1.5
     * @description: Log message to file
     * @param $message
     * @return void
     */
    static function log($message)
    {
        self::$logs["normal"][] = $message;
        $file = new File(APP_DIR . "PKRS_error.log");
        $file->open("a+");
        $file->write(date("d.m.Y H:i:s") . " - : " . $message . PHP_EOL);
        $file->close();
        $file = null;
    }

    /**
     * @function: logError
     * @since: 0.1.5
     * @description: Log php error
     * @param $message
     * @param $code
     * @param \Exception $exception
     * @return void
     */
    static function logError($message, $code, \Exception $exception = null)
    {
        self::$logs["error"][] = ["m" => $message, "c" => $code, "e" => $exception];
        $file = new File(APP_DIR . "PKRS_error.log");
        $file->open("a+");
        $file->write(date("d.m.Y H:i:s") . " - " . $code . ": " . $message . ($exception instanceof \Exception ? " (" . $exception->getFile() . ", line " . $exception->getLine() . ")" : "") . PHP_EOL);
        $file->close();
    }

    /**
     * @function: enable     *
     * @description: Enable / disable debugger
     * @since: 0.1.4
     * @param bool $enabled
     * @return void
     */
    static function enable($enabled = true)
    {
        self::$enabled = $enabled;
    }

    /**
     * Function: handler_ajax
     * Since: 0.1.4
     * Description: Ajax error handler
     * @param $errno
     * @param $errstr
     * @param $errfile
     * @param $errline
     * @param array $context
     * @return void
     */
    static function handlerAjax($errno, $errstr, $errfile, $errline, $context = [])
    {
        self::$ajax[] = ["errno" => $errno, "errstr" => $errstr, "file" => $errfile, "line" => $errline, "context" => $context];
    }

    /**
     * Function: handler
     * Since: 0.1.4
     * Description: Error handler
     * @param $errno
     * @param $errstr
     * @param $errfile
     * @param $errline
     * @param array $context
     * @return bool|null
     */
    static function handler($errno, $errstr, $errfile, $errline, $context = [])
    {
        if (IS_AJAX) return null;
        self::$detailed[] = ["errno" => $errno, "errstr" => $errstr, "file" => $errfile, "line" => $errline, "context" => $context];
        if (self::$enabled) {
            if ($errno != E_PARSE || $errno != E_ERROR || $errno != E_WARNING || $errno != E_NOTICE) {
                return null;
            }
            if (!self::$header) {
                //ob_get_clean();
                self::$header = true;
                echo "<html><head><title>PKRS Error page</title>";
                echo "<style type='text/css'>" . file_get_contents(dirname(__FILE__) . DS . "debug.css") . "</style>";
                echo "<script type='text/javascript'>" . file_get_contents(dirname(__FILE__) . DS . "debug.js") . "</script>";
                echo "</head><body class='debug_page'><h1>PKRS Debugger :: Error occured!</h1>";
            }
            echo "<div class='debugger_cont'>";
            echo "<h2>Stack trace:</h2>";
            self::echoStack(false, ($errno ? self::FriendlyErrorType($errno) . ": " : "") . $errstr, $errfile, $errline, "", "", "");
            $f = 0;
            foreach (debug_backtrace() as $trace) {
                self::echoStack($f == 0 ? true : false, @$trace["message"], @$trace["file"], @$trace["line"], @$trace["class"], @$trace["class"], @$trace["args"]);
                $f++;
            }
            echo "</div>";
            if (!self::$footer) {
                self::$footer = true;
                self::echoServerUsage();
            }
            exit;
        } else {
            if ($errno == E_PARSE || $errno == E_ERROR) {
                ob_get_clean();
                echo "<h1>Sorry, here is error :(</h1><p>" . $errstr . "</p>";
                exit;
            }
            return true;
        }
    }

    /**
     * Function: echoStack
     * Since: 0.1.4
     * Description: Echo part of file where is error ocurred
     * @param $opened
     * @param $message
     * @param string $file
     * @param int $line
     * @internal param string $function
     * @internal param string $class
     * @internal param string $args
     * @return void
     */
    private static function echoStack($opened, $message, $file = "", $line = 0, $_p1 = null, $_p2 = null, $_p3 = null, $_p4 = null)
    {
        if (IS_AJAX && !self::$enabled) return;
        if (!file_exists($file) && strstr($file, "eval()")) {
            echo "<div class='debugger_stack " . ($opened ? "debugger_stack_opened" : "") . "'>";
            echo "<h4>" . $message . "</h4>";

            echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>" . $file . " (line " . $line . ")</h4></div><div class='debugger_file_content'>";
            // test repair (eval() problem) -> eq.: /home/www/..../smarty/libs/sysplugins/smarty_internal_templatebase.php(157) : eval()\'d code
            if ($_p1) echo "<span class='debugger_line'>\$_p1: " . var_export($_p1, true) . "</span>";
            if ($_p2) echo "<span class='debugger_line'>\$_p1: " . var_export($_p2, true) . "</span>";
            if ($_p3) echo "<span class='debugger_line'>\$_p1: " . var_export($_p3, true) . "</span>";
            if ($_p4) echo "<span class='debugger_line'>\$_p1: " . var_export($_p4, true) . "</span>";

            $test = explode(".php(", $file);
            if (count($test) > 0) {

                $file = $test[0] . ".php";
                $data = file($file);

                $line = intval(explode(")", $test[1])[0]);

                if ($line <= count($data)) {
                    foreach ($data as $c => $l) {
                        if ($c < ($line + 12) && $c > ($line - 14))
                            echo "<span class='debugger_line" . (($c + 1) == $line ? " debugger_selected" : "") . "'>" . ($c + 1) . ": " . htmlentities($l,ENT_QUOTES) . "</span>";
                    }
                }

            }
            echo "</div></div>";

        } else {
            echo "<div class='debugger_stack " . ($opened == true ? "debugger_stack_opened" : "") . "'>";
            echo "<h4>" . $message . "</h4>";
            if (file_exists($file)) {
                $data = file($file);
                if ($line <= count($data)) {
                    echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>" . $file . " (line " . $line . ")</h4></div><div class='debugger_file_content'>";
                    foreach ($data as $c => $l) {
                        if ($c < ($line + 10) && $c > ($line - 12))
                            echo "<span class='debugger_line" . (($c + 1) == $line ? " debugger_selected" : "") . "'>" . ($c + 1) . ": " . htmlentities($l,ENT_QUOTES) . "</span>";
                    }
                    echo "</div></div>";
                }
            }
        }
        echo "</div>";
    }

    /**
     * Function: FriendlyErrorType
     * Since: 0.1.5
     * Description: Show type of PHP Error
     * @param $type
     * @return string
     */
    private static function FriendlyErrorType($type)
    {
        switch ($type) {
            case E_ERROR: // 1 //
                return 'E_ERROR';
            case E_WARNING: // 2 //
                return 'E_WARNING';
            case E_PARSE: // 4 //
                return 'E_PARSE';
            case E_NOTICE: // 8 //
                return 'E_NOTICE';
            case E_CORE_ERROR: // 16 //
                return 'E_CORE_ERROR';
            case E_CORE_WARNING: // 32 //
                return 'E_CORE_WARNING';
            case E_COMPILE_ERROR: // 64 //
                return 'E_COMPILE_ERROR';
            case E_COMPILE_WARNING: // 128 //
                return 'E_COMPILE_WARNING';
            case E_USER_ERROR: // 256 //
                return 'E_USER_ERROR';
            case E_USER_WARNING: // 512 //
                return 'E_USER_WARNING';
            case E_USER_NOTICE: // 1024 //
                return 'E_USER_NOTICE';
            case E_STRICT: // 2048 //
                return 'E_STRICT';
            case E_RECOVERABLE_ERROR: // 4096 //
                return 'E_RECOVERABLE_ERROR';
            case E_DEPRECATED: // 8192 //
                return 'E_DEPRECATED';
            case E_USER_DEPRECATED: // 16384 //
                return 'E_USER_DEPRECATED';
        }
        return "";
    }

    private static function  echoServerUsage()
    {
        if (IS_AJAX) return;
        echo "<div class='debugger_server'><div class='debugger_content'>";

        echo "<div class='debugger_block'><div class='debugger_block_name' onclick='javascript:changeClass(this.parentNode,\"debugger_opened\")'>GLOBAL (" . self::getMemoryUsage() . " MB)</div>";
        echo "<div class='debugger_block_content'><h2>GLOBAL vars</h2><pre>";

        echo "<div class='debugger_stack'>";
        echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>RAM & Processor usage</h4></div><div class='debugger_file_content'>";
        echo "Memory usage (RAM): ";
        echo self::getMemoryUsage();
        echo " MB<br>";
        echo "Memory limit (RAM): ";
        echo ini_get("memory_limit");
        echo "B<br>";
        echo "Page generation time (Sec.): " . (microtime(true) - APP_START) . "<br>";
        echo "</div></div>";
        echo "</div>";

        if (isset($_SESSION["redirect_log"]) && !empty($_SESSION["redirect_log"])) {
            foreach ($_SESSION["redirect_log"] as $k => $v) {
                echo "<div class='debugger_stack'>";
                echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>#$k HTTP Redirect ($v[from_path] -> $v[to_path], method " . $v["SERVER"]["REQUEST_METHOD"] . ")</h4></div><div class='debugger_file_content'>";
                echo self::dumpToTable($v);
                echo "</div></div>";
                echo "</div>";
            }
        }
        $_SESSION["redirect_log"] = [];

        echo "<div class='debugger_stack'>";
        echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>\$_SERVER vars</h4></div><div class='debugger_file_content'>";
        echo self::dumpToTable($_SERVER);
        echo "</div></div>";
        echo "</div>";

        echo "<div class='debugger_stack'>";
        echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>\$_POST vars</h4></div><div class='debugger_file_content'>";
        echo self::dumpToTable($_POST);
        echo "</div></div>";
        echo "</div>";

        echo "<div class='debugger_stack'>";
        echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>\$_SESSION vars</h4></div><div class='debugger_file_content'>";

        if (isset($_SESSION)) echo self::dumpToTable($_SESSION);
        else echo "\$_SESSION not started";
        echo "</div></div>";
        echo "</div>";

        echo "<div class='debugger_stack'>";
        echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>\$_COOKIE vars</h4></div><div class='debugger_file_content'>";
        echo self::dumpToTable($_COOKIE);
        echo "</div></div>";
        echo "</div>";

        echo "<div class='debugger_stack'>";
        echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>Defined constants</h4></div><div class='debugger_file_content'>";
        echo self::dumpToTable(get_defined_constants(true)["user"]);
        echo "</div></div>";
        echo "</div>";

        echo "</pre></div>";
        echo "</div>";

        if (count(self::$mysql) > 0) {
            echo "<div class='debugger_block'><div class='debugger_block_name' onclick='javascript:changeClass(this.parentNode,\"debugger_opened\")'>MySQL (" . count(self::$mysql) . ")</div>";
            echo "<div class='debugger_block_content'><h2>MySQL Queries (count " . count(self::$mysql) . ")</h2><pre>";
            foreach (self::$mysql as $m) {
                $m["query"] = str_replace( array_map(function($i){return ":".$i;},array_keys($m["params"])), array_map(function($i, $k){return "<abbr title='$k'>$i</abbr>";},array_values($m["params"]), array_keys($m["params"])), $m["query"] );
                echo "<div class='debugger_stack'>";
                echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>Execution $m[time] seconds, " . count($m["result"]) . " results<br>$m[query]</h4></div><div class='debugger_file_content'>";
                echo "<h4>Params:</h4>";
                echo self::dumpToTable($m["params"]);
                echo "<br><h4>Results:</h4>";
                echo self::dumpToTable($m["result"]);
                if (is_array($m["trace"]) && isset($m["trace"]["file"]) && isset($m["trace"]["line"])) {
                    echo self::echoStack(true,"Called from",$m["trace"]["file"], $m["trace"]["line"]);
                }
                echo "</div></div>";
                echo "</div>";
            }
            echo "</pre></div>";
            echo "</div>";
        }
        if (count(self::$dumps) > 0) {
            echo "<div class='debugger_block'><div class='debugger_block_name' onclick='javascript:changeClass(this.parentNode,\"debugger_opened\")'>VAR_DUMPS (" . count(self::$dumps) . ")</div>";
            echo "<div class='debugger_block_content'><h2>VAR_DUMPS (count " . count(self::$dumps) . ")</h2><pre>";
            foreach (self::$dumps as $m) {
                echo "<div class='debugger_stack'>";
                echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>$m[name]</h4></div><div class='debugger_file_content'>";
                echo self::dumpToTable($m["var"]);
                echo "</div></div>";
                echo "</div>";
            }
            echo "</pre></div>";
            echo "</div>";
        }
        foreach (self::$dumpsExtra as $m) {
            echo "<div class='debugger_block'><div class='debugger_block_name' onclick='javascript:changeClass(this.parentNode,\"debugger_opened\")'>MY: $m[name]</div>";
            echo "<div class='debugger_block_content'><h2>$m[name]</h2><pre>";
            echo "<div class='debugger_stack debugger_stack_opened'>";
            echo "<div class='debugger_file'><div class='debugger_file_header' onclick='javascript:changeClass(this.parentNode.parentNode,\"debugger_stack_opened\")'><h4>$m[name]</h4></div><div class='debugger_file_content'>";
            echo self::dumpToTable($m["var"]);
            echo "</div></div>";
            echo "</div>";
            echo "</pre></div>";
            echo "</div>";
        }
        if (count(self::$detailed) > 0) {
            echo "<div class='debugger_block'><div class='debugger_block_name' onclick='javascript:changeClass(this.parentNode,\"debugger_opened\")'>WARNINGS (" . count(self::$detailed) . ")</div>";
            echo "<div class='debugger_block_content'><h2>WARNINGS LOG (count " . count(self::$detailed) . ")</h2>";
            echo "<pre>";
            foreach (self::$detailed as $det) {
                self::echoStack(false, self::FriendlyErrorType($det["errno"]) . ": " . htmlentities($det["errstr"]), $det["file"], $det["line"]);
            }
            echo "</pre>";
            echo "</div>";
            echo "</div>";
        }
        echo "<div class='debugger_block' id='debugger_ajax'><div class='debugger_block_name' onclick='javascript:changeClass(this.parentNode,\"debugger_opened\")'>AJAX (<span id='debugger_ajax_count'>0</span>)</div>";
        echo "<div class='debugger_block_content'><h2>AJAX Requests (count <span id='debugger_ajax_count2'>0</span>)</h2><div id='debugger_ajax_content'><pre></pre></div>";

        echo "</div>";
        echo "</div>";

        echo "</div></div>";
    }

    /**
     * Function: getMemoryUsage
     * Since: 0.1.5
     * Description: Returns RAM memory usage
     * @param int $decimals
     * @return string
     */
    private static function getMemoryUsage($decimals = 2)
    {
        $result = 0;

        if (function_exists('memory_get_usage')) {
            $result = memory_get_usage() / 1024;
        } else {
            if (function_exists('exec')) {
                $output = [];

                if (substr(strtoupper(PHP_OS), 0, 3) == 'WIN') {
                    exec('tasklist /FI "PID eq ' . getmypid() . '" /FO LIST', $output);

                    $result = preg_replace('/[\D]/', '', $output[5]);
                } else {
                    exec('ps -eo%mem,rss,pid | grep ' . getmypid(), $output);

                    $output = explode('  ', $output[0]);

                    $result = $output[1];
                }
            }
        }

        return number_format(intval($result) / 1024, $decimals, '.', '');
    }

    /**
     * Function: dumpToTable
     * Since: 0.1.5
     * Description: Generate (recursive) table with variable data
     * @param $data
     * @param int $level
     * @return string
     */
    private static function dumpToTable($data, $level = 0)
    {
        $retval = '<table class="debug_table ' . ($level > 2 || ($level > 0 && is_object($data)) ? "collapese" : "") . '">';
        if (is_numeric($data)) $retval .= "Number: $data";
        elseif (is_string($data)) $retval .= "String: '" . htmlspecialchars($data) . "'";
        elseif (is_null($data)) $retval .= "NULL";
        elseif ($data === true) $retval .= "TRUE";
        elseif ($data === false) $retval .= "FALSE";
        elseif (is_array($data)) {
            $retval .= "<thead><tr><th colspan='2' onclick=\"changeClass(this.parentNode.parentNode.parentNode,'collapese')\">Array (" . count($data) . ")</th></tr></thead><tbody>";
            if (count($data) > 0) {
                foreach ($data AS $key => $value) {
                    $retval .= "<tr><td><strong>$key</strong></td><td>";
                    $retval .= self::dumpToTable($value, $level + 1) . "</td></tr>";
                }
            } else {
                $retval .= "<tr><td colspan='2'><i>Empty...</i></td></tr>";
            }
            $retval .= "</tbody>";
        } elseif (is_object($data)) {
            $retval .= "<thead><tr><th colspan='2' onclick=\"changeClass(this.parentNode.parentNode.parentNode,'collapese')\">Object (" . get_class($data) . ")</th></tr></thead><tbody>";
            $i = 0;
            $r_class = new \ReflectionClass($data);
            foreach ($r_class->getConstants() AS $key => $value) {
                $i++;
                $retval .= "<tr><td>const <strong>" . $key . "</strong></td><td>";
                $retval .= self::dumpToTable($value, $level + 1) . "</td></tr>";
            }
            $vars = get_object_vars($data);
            foreach ($r_class->getProperties() AS $value) {
                $i++;
                if ($value->isPrivate() || $value->isProtected())
                    $value->setAccessible(true);
                $retval .= "<tr><td> " . self::getPropertyVisibility($value) . " <strong>\$" . $value->getName() . "</strong></td><td>";
                if (!isset($vars[$value->getName()])) $retval .= "<i>unknown value... </i>";
                else $retval .= "" . self::dumpToTable($value->getValue($data), $level + 1) . "</td></tr>";
            }
            foreach ($r_class->getMethods() AS $func) {
                $i++;
                if ($func->getFileName() != $r_class->getFileName()) continue;
                $retval .= "<tr><td><strong>function " . $func->getShortName() . "</strong></td><td>";
                $filename = $func->getFileName();
                $start_line = $func->getStartLine() - 1;
                $end_line = $func->getEndLine();
                $retval .= "<div style='overflow-x:auto'>" . (!!$filename && file_exists($filename) ? self::echoCode(false, $filename, $start_line, $end_line - $start_line) : "") . "</div><div style='clear:both'></div></td></tr>";
            }
            if ($i == 0) {
                $retval .= "<tr><td colspan='2'><i>Empty...</i></td></tr>";
            }
        }
        return $retval . "</table>";
    }

    /**
     * Function: getPropertyVisibility
     * Since: 0.1.5
     * Description: Get visibility type of ReflectionProperty
     * @param \ReflectionProperty $p
     * @return string
     */
    private static function getPropertyVisibility(\ReflectionProperty $p)
    {
        if ($p->isPrivate()) return "private";
        if ($p->isProtected()) return "protected";
        if ($p->isPublic()) return "public";
        if ($p->isStatic()) return "static";
        return "var";
    }

    /**
     * Function: echoCode
     * Since: 0.1.5
     * Description: return part of source code from file
     * @param $opened
     * @param string $file
     * @param int $start
     * @param int $len
     * @return null|string
     */
    private static function echoCode($opened, $file = "", $start = 0, $len = 10)
    {
        $r = "";
        if (IS_AJAX && !self::$enabled) return null;
        $r .= "<table class='debug_table" . ($opened ? "" : " collapese") . "'>";
        if ($file != "") {
            if (file_exists($file)) {
                $data = file($file);
                if ($start <= count($data)) {
                    $r .= "<thead>
                        <tr>
                            <th colspan='2' onclick='javascript:changeClass(this.parentNode.parentNode.parentNode,\"collapese\")'>Source code: " . basename($file) . " (line " . ($start + 1) . ")</th>
                        </tr>
                        </thead>
                        <tbody>";
                    foreach ($data as $pointer => $l) {
                        if ($pointer >= $start && $pointer < ($start + $len))
                            $r .= "<tr style='border:none'><td style='border:none'><span style='white-space: nowrap;'>" . ($pointer + 1) . ":</span></td><td style='border:none;white-space:pre'>" . $l . "</td></tr>";
                    }
                    $r .= "</tbody>";
                }
            }
        }
        $r .= "</table>";
        return $r;
    }

    /**
     * Function: show
     * Since: 0.1.4
     * Description: Display error page or only debug bars
     * @return bool|null
     */
    static function show()
    {
        if (IS_AJAX) return null;
        if (self::$enabled) {
            if (!empty(self::$logs["error"])) {

                if (!self::$header) {
                    //ob_get_clean();
                    ob_clean();
                    self::$header = true;
                    echo "<html><head><title>PKRS Error page</title>";
                    echo '<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>';
                    echo "<style type='text/css'>" . file_get_contents(dirname(__FILE__) . DS . "debug.css") . "</style>";
                    echo "<script type='text/javascript'>" . file_get_contents(dirname(__FILE__) . DS . "debug.js") . "</script>";
                    echo "</head><body class='debug_page'><h1>PKRS Debugger :: Error occured!</h1>";
                }

                echo "<div class='debugger_cont'>";
                foreach (self::$logs["error"] as $e) {
                    if ($e["e"] instanceof \Exception) {
                        self::echoStack(false, self::FriendlyErrorType($e["e"]->getCode()) . ": " . $e["e"]->getMessage(), $e["e"]->getFile(), $e["e"]->getLine(), "", "", "");
                        $f = 0;
                        foreach ($e["e"]->getTrace() as $trace) {
                            self::echoStack($f == 0, "", isset($trace["file"]) ? $trace["file"] : "", isset($trace["line"]) ? $trace["line"] : "", isset($trace["function"]) ? $trace["function"] : "", isset($trace["class"]) ? $trace["class"] : "", isset($trace["args"]) ? $trace["args"] : "");
                            $f++;
                        }
                    }
                }

                echo "</div>";
                if (!self::$footer) {
                    self::$footer = true;
                    self::echoServerUsage();
                }
                $_SESSION["redirect_log"] = [];

                exit;
            } else {
                if (!self::$header) {
                    self::$header = true;
                    echo "<style type='text/css'>" . file_get_contents(dirname(__FILE__) . DS . "debug.css") . "</style>";
                    echo "<script type='text/javascript'>" . file_get_contents(dirname(__FILE__) . DS . "debug.js") . "</script>";
                }
                if (!self::$footer) {
                    self::$footer = true;
                    self::echoServerUsage();
                }
            }
            $_SESSION["redirect_log"] = [];
        } else {
            if (!empty(self::$logs["error"])) {
                ob_get_clean();
                echo "<h1>Sorry, here is error :(</h1>";

            }
            $_SESSION["redirect_log"] = [];
            exit;
        }
        return true;
    }

}
