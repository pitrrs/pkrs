<?php
/********************************************
 *
 * MainException.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Exception;

use Exception;
use PKRS\Core\Debug\Debug;
use PKRS\Core\Headers\Status;
use PKRS\Core\Hooks\Hooks;
use PKRS\Core\Service\Service;
use PKRS\Core\Service\ServiceContainer;

/**
 * Class MainException
 * @package PKRS\Core\Exception
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class MainException extends Exception
{

    /**
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = "", $code = 0, Exception $previous = null)
    { 
        Status::send_code(500);
        $e = parent::__construct($message, $code, $previous);
        try {
            $c = Service::gc();
            if ($c instanceof ServiceContainer) {
                if ($c->getDebug() instanceof Debug) {
                    $c->getDebug()->logError($message, $code, $this);
                    if ($c->getHooks() instanceof Hooks)
                        $c->getHooks()->execute("application", "on_error");
                }
            } else die("Master (Service created) error: " . $e->getMessage());
        } catch (Exception $e) {
            die("Master (service not created) fatal error: " . $e->getMessage()." (".$e->getLine().": ".$e->getFile().")");
        }
    }

}