<?php
/**************************************************************
 *
 * Service.php, created 4.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Service;

use PKRS\Core\Object\Object;

/**
 * Class Service
 * @package PKRS\Core\Service
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Service extends Object
{

    private static $serviceContainer;

    /**
     * @param ServiceContainer $serviceContainer
     */
    public static function setContainer(ServiceContainer $serviceContainer)
    {
        if (!self::$serviceContainer)
            self::$serviceContainer = $serviceContainer;
    }

    /**
     * @return ServiceContainer
     */
    public static function gc()
    {
        return self::$serviceContainer;
    }
}