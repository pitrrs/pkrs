<?php
/**************************************************************
 *
 * ServiceContainer.php, created 3.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Service;

use Nette\Neon\Exception;
use PKRS\Core\Config\Config;
use PKRS\Core\Debug\Debug;
use PKRS\Core\Exception\FileException;
use PKRS\Core\Exception\MainException;
use PKRS\Core\Exception\ServiceException;
use PKRS\Core\Form\Form;
use PKRS\Core\Modules\ExternalModule;
use PKRS\Core\Modules\PHPModule;
use ReflectionClass;

/**
 * Class ServiceContainer
 * @package PKRS\Core\Service
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class ServiceContainer extends Service
{

    private $services = [];
    private $objects = [];
    private $config;
    static $self;
    private $externalModules = [];

    /**
     * @param string $config_file (path to application config ini file)
     */
    public function __construct($config_file = null)
    {
        try {
            self::setContainer($this);
            self::$self = $this; // static access
            $this->config = new Config($config_file);
            if (!defined("DEV"))
                define("DEV", strtolower($this->config->getConf("application", "debug")) === "true" || $this->config->getConf("application", "debug") === true);
            $this->runJsCssConsolidateService();
            foreach ($this->config->getConfGroup("external_modules") as $module => $config) {
                // create module instance and init
                $moduleClass = "\\$module\\$module";
                if (class_exists($moduleClass)) {
                    $moduleObject = new $moduleClass;
                    if ($moduleObject instanceof ExternalModule) {
                        $moduleReflection = new ReflectionClass($moduleObject);
                        $moduleDirectory = dirname($moduleReflection->getFileName()) . DS;
                        $this->externalModules[$module] = ["object" => $moduleObject, "config"=>$config, "dir" => $moduleDirectory];
                        if (file_exists($moduleDirectory . "config.neon")) {
                            $this->config->loadConfig($moduleDirectory . "config.neon", null, true);
                        }
                    } else throw new \Exception("Defined external module $module is not instance of ExternalModule!");
                } else throw new \Exception("Defined external module $module not exists!");
            }
            $this->services = $this->config->getServices();
            // init debuger
            $this->getDebug();
            // prevent new loading class
            $this->objects["config"] = $this->config;
            $this->objects["serviceContainer"] = $this;
            $this->objects["debug"] = new Debug();
            $hooks = $this->getHooks();
            $hooks->registerAction("presenters", "on_create", ["\\PKRS\\Core\\Modules\\PHPModule", "runModules"], [$this, (array)@$this->config->getConfGroup("modules")], 0);
            // $hooks->registerAction("modules", "on_create", array("\\PKRS\\Core\\Modules\\PHPModule", "checkInstance"), array($this, (array)@$this->config->getConfGroup("modules")), 0);
            $hooks->execute("service", "on_create");

            foreach ($this->externalModules as $module => $data) {
                // init module
                $data["object"]->run($data["config"]);
            }
        } catch (\Exception $e){
            die("Master error in service construction: ".$e->getMessage());
        }
    }

    /**
     * @return ServiceContainer
     */
    public static function getInstance()
    {
        return self::$self;
    }

    /**
     * @param $name
     * @param string $prefix
     * @return PHPModule
     */
    public function getPhpModule($name, $prefix = "")
    {
        return PHPModule::getModule($name, $prefix);
    }

    /**
     * @param $moduleName
     * @return bool|IExternalModule
     */
    public function getExternalModule($moduleName){
        if (!isset($this->externalModules[$moduleName]))
            return false;
        else return $this->externalModules[$moduleName]["object"];
    }

    /**
     * @return ServiceContainer
     */
    public function getServiceContainer()
    {
        return $this;
    }

    /**
     * @return \PKRS\Core\Hooks\Hooks
     */
    public function getHooks()
    {
        return $this->getService("hooks");
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->getService("config");
    }

    /**
     * @return Debug
     */
    public function getDebug()
    {
        return $this->getService("debug");
    }

    /**
     * @return \PKRS\Core\Config\DbConfig
     */
    public function getDbConfig()
    {
        return $this->getService("db_config");
    }

    /**
     * @return \PKRS\Core\Mailer\Mailer
     */
    public function getMailer()
    {
        return $this->getService("mailer")->reset_mailer();
    }

    /**
     * @return \PKRS\Core\Database\Database
     */
    public function getDatabase()
    {
        return $this->getService("database");
    }

    /**
     * @return \PKRS\Core\Router\Router
     */
    public function getRouter()
    {
        return $this->getService("router");
    }

    /**
     * @return \PKRS\Core\Session\Session
     */
    public function getSession()
    {
        return $this->getService("session");
    }

    /**
     * @return \PKRS\Core\Session\Cookies
     */
    public function getCookies()
    {
        return $this->getService("cookies");
    }

    /**
     * @param $name
     * @param string $id
     * @param string $method
     * @param string $XSS_Error
     * @return Form
     */
    public function getForm($name, $id = "form_ID", $method = "POST", $XSS_Error = "XSS protection: Please submit your form again.")
    {
        return new Form($name, $id, $method, $XSS_Error);
    }

    /**
     * @return \PKRS\Core\User\User
     */
    public function getUser()
    {
        return $this->getService("user");
    }

    /**
     * @return \PKRS\Core\User\Messages
     */
    public function getMessages()
    {
        return $this->getService("messages");
    }

    /**
     * Function: getLang
     * Since: 0.1.4
     * Description: Get lang service
     *
     * @return \PKRS\Core\Lang\Lang
     */
    public function getLang()
    {
        return $this->getService("lang");
    }

    /**
     * @return \PKRS\Core\View\Smarty
     */
    public function getView()
    {
        return $this->getService("view");
    }

    /**
     * @return \PKRS\Helpers\Transform\Arrays
     */
    public function getTransformArrays()
    {
        return $this->getService("transform_arrays");
    }

    /**
     * @return \PKRS\Helpers\Transform\DateTime
     */
    public function getTransformDateTime()
    {
        return $this->getService("transform_datetime");
    }

    /**
     * @return \PKRS\Helpers\Transform\Numbers
     */
    public function getTransformNumbers()
    {
        return $this->getService("transform_numbers");
    }

    /**
     * @return \PKRS\Helpers\Transform\Strings
     */
    public function getTransformStrings()
    {
        return $this->getService("transform_strings");
    }

    /**
     * @return \PKRS\Helpers\Validator\Validator
     */
    public function getValidator()
    {
        return $this->getService("validator");
    }

    /**
     * @return \PKRS\Helpers\Generators\Randoms
     */
    public function getGeneratorsRandoms()
    {
        return $this->getService("generators_randoms");
    }

    /**
     * @return \PKRS\Helpers\Transform\Trees
     */
    public function getTransformTrees()
    {
        return $this->getService("transform_trees");
    }

    /**
     * @param $type
     * @return object
     */
    private function getService($type)
    {
        if (in_array("hooks", array_keys($this->objects))) {
            $this->objects["hooks"]->execute("service", "on_get_always", ["name" => $type]);
            if (!in_array($type, array_keys($this->objects))) {
                $this->objects["hooks"]->execute("service", "on_get_new", ["name" => $type]);
            }
        }

        $type = strtolower($type);
        if (isset($this->objects[$type]))
            return $this->objects[$type];
        else return $this->createService($type);
    }

    /**
     * @param string $name - Service name
     * @return null|object
     * @throws FileException
     * @throws ServiceException
     */
    private function createService($name)
    {
        $name = strtolower($name); // fix - service name only small letters
        if (!in_array($name, array_keys($this->services))) { // is service defined?
            return null; // not defined -> return null
        } else { // yes, service defined
            if (in_array($name, array_keys($this->objects))) {
                // we have object - return it
                return $this->objects[$name];
            } else {
                // object not created - create it
                $service = $this->services[$name];
                $class = "\\" . str_replace("/", "\\", trim(trim($service["class"], "/"), "\\")); // fix \ escaping
                if (!class_exists($class, true))
                    throw new FileException("Service: dependency class $class not exists!");
                $params = [];
                foreach ($service["params"] as $param) {
                    foreach ($param["dependency"] as $dependency) {
                        if ($param["type"] == "service") {
                            if ($dependency == "serviceContainer") $object = $this; // avoid bug problem
                            else {
                                $object = $this->getService($dependency);
                            }
                        } elseif ($param["type"] == "class") {
                            $cl = "\\" . str_replace("/", "\\", trim(trim($dependency, "/"), "\\"));
                            if (class_exists($cl, true)) {
                                if ($name == "user") {
                                    $clu = "\\" . str_replace("/", "\\", trim(trim($this->config->getConf("user", "model"), "/"), "\\"));
                                    $object = new $clu();
                                    //$object = new $cl($clui);
                                } else $object = new $cl();
                            } else throw new ServiceException("Service: dependency class $cl not exists!");
                        } elseif ($param["type"] == "this")
                            $object = $this;
                        else {
                            throw new ServiceException("Service: dependency class not exists!");
                        }
                        $params[] = $object;
                    }
                }
                try {
                    $reflection = new ReflectionClass($class);
                    // detect overiding -> must be child of original class!
                    if (isset($service["origin_data"])) {
                        $cl = "\\" . str_replace("/", "\\", trim(trim($service["origin_data"]["class"], "/"), "\\"));
                        if (!$reflection->isSubclassOf($cl)) {
                            throw new ServiceException("Service $name (Overide $service[class]) is not instance of overided class $cl");
                        }
                    }
                    if (!$reflection->isSubclassOf("\\PKRS\\Core\\Service\\Service")) {
                        throw new ServiceException("Service $name is not instance of \\PKRS\\Core\\Service\\Service()");
                    }
                    if ($reflection->hasMethod("__construct") && is_array($params))
                        $object = $reflection->newInstanceArgs($params);
                    else $object = $reflection->newInstance();
                } catch (ServiceException $e) {
                    var_dump($e);
                    exit;
                }
                // register service object
                $this->objects[$name] = $object;
                // return new object
                return $this->objects[$name];
            }
        }
    }

    /**
     * Consolidating JS and CSS service
     *
     * detected by $_GET request
     */
    private function runJsCssConsolidateService()
    {
        if (isset($_GET["__files_"])) {
            $cache = $this->config->getConf("application", "consolidate_scripts_caching") == "true";
            if (isset($_GET["__load_consolidated_"]) && $_GET["__load_consolidated_"] == "js") {
                header("Content-type:text/javascript");
                if ($cache && file_exists(ROOT_DIR . $this->config->getConf("application", "cache_dir") . DS . sha1($_GET["__files_"]) . ".js")) {
                    echo file_get_contents(ROOT_DIR . $this->config->getConf("application", "cache_dir") . DS . sha1($_GET["__files_"]) . ".js");
                    exit;
                }
                $ech = "";
                foreach (explode(",", $_GET["__files_"]) as $f) {
                    if (substr($f, 0, 2) == "//")
                        $ech .= file_get_contents("http:" . $f);
                    else if (substr($f, 0, 1) == "/")
                        $ech .= file_get_contents("http://" . $_SERVER["HTTP_HOST"] . "/" . $f);
                    else $ech .= file_get_contents($f);
                }
                if ($cache) {
                    $h = fopen(ROOT_DIR . $this->config->getConf("application", "cache_dir") . DS . sha1($_GET["__files_"]) . ".js", "w+");
                    fwrite($h, $ech);
                    fclose($h);
                }
                echo $ech;
                exit;
            }
            if (isset($_GET["__load_consolidated_"]) && $_GET["__load_consolidated_"] == "css") {
                header("Content-type:text/css");
                if ($cache && file_exists(ROOT_DIR . $this->config->getConf("application", "cache_dir") . DS . sha1($_GET["__files_"]) . ".css")) {
                    echo file_get_contents(ROOT_DIR . $this->config->getConf("application", "cache_dir") . DS . sha1($_GET["__files_"]) . ".css");
                    exit;
                }
                $ech = "";
                foreach (explode(",", $_GET["__files_"]) as $f) {
                    if (substr($f, 0, 2) == "//")
                        $ech .= file_get_contents("http:" . $f);
                    else if (substr($f, 0, 1) == "/")
                        $ech .= file_get_contents("http://" . $_SERVER["HTTP_HOST"] . "/" . $f);
                    else $ech .= file_get_contents($f);
                }
                if ($cache) {
                    $h = fopen(ROOT_DIR . $this->config->getConf("application", "cache_dir") . DS . sha1($_GET["__files_"]) . ".js", "w+");
                    fwrite($h, $ech);
                    fclose($h);
                }
                echo $ech;
                exit;
            }
        }
    }
}