<?php
/**************************************************************
 *
 * Hooks.php, created 5.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Hooks;

use PKRS\Core\Exception\ServiceException;
use PKRS\Core\Service\Service;

/**
 * Class Hooks
 * @package PKRS\Core\Hooks
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Hooks extends Service
{
    const SECTION_ALL = "all";
    const ACTION_ALL_BEFORE_HOOK_EXECUTE = "before_hook_execute";
    const ACTION_ALL_AFTER_HOOK_EXECUTE = "after_hook_execute";

    const SECTION_APPLICATION = "application";
    const ACTION_APPLICATION_ON_CREATE = "on_create";
    const ACTION_APPLICATION_ON_START = "on_start";
    const ACTION_APPLICATION_ON_EXIT = "on_exit";
    const ACTION_APPLICATION_ON_ERROR = "on_error";

    private $service;
    private $hooks = [];

    /**
     * @throws \PKRS\Core\Exception\ConfigException
     * @throws \PKRS\Core\Exception\FileException
     */
    public function __construct()
    {
        $this->service = self::gc();
        if (file_exists(dirname(__FILE__) . DS . "allowedHooks.neon")) {
            $hooks = $this->service->getConfig()->parseConfig(dirname(__FILE__) . DS . "allowedHooks.neon");
            foreach ($hooks as $section => $value)
                foreach ($value as $action => $val) {
                    $this->registerHook($section, $action);
                }
        }
    }

    /**
     * Function: registerHook
     * Since: 0.1.3
     * Description: Register new hook
     *
     * @param $section
     * @param $action
     * @return bool
     */
    public function registerHook($section, $action)
    {
        if (isset($this->hooks[$section][$action])) {
            return false;
        } else {
            if (!isset($this->hooks[$section])) $this->hooks[$section] = [];
            $this->hooks[$section][$action] = [
                0 => [],
                1 => [],
                2 => [],
                3 => [],
                4 => [],
                5 => [],
                6 => [],
                7 => [],
                8 => [],
                9 => [],
                10 => [],
            ];
            return true;
        }
    }

    /**
     * Function: registerAction
     * Since: 0.1.3
     * Description: Register hook action
     *
     * @param $section
     * @param $action
     * @param $callable
     * @param array $params
     * @param int $level
     * @return array
     * @throws \PKRS\Core\Exception\ServiceException
     */
    public function registerAction($section, $action, $callable, $params = [], $level = 5)
    {
        if (!is_callable($callable)) throw new ServiceException("Register hook: " . var_export($callable, true) . " is not callable!");
        $level = self::gc()->getTransformNumbers()->limitBetween($level, 0, 10);
        if (!isset($this->hooks[$section][$action])) $this->registerHook($section, $action);
        if (!isset($this->hooks[$section][$action][$level])) $this->hooks[$section][$action][$level] = [];
        return $this->hooks[$section][$action][$level][] = ["callable" => $callable, "params" => $params];
    }

    /**
     * Function: execute
     * Since: 0.1.3
     * Description: Public execute hook actions
     *
     * @param $section
     * @param $action
     * @param array $input
     * @return mixed
     */
    public function execute($section, $action, $input = [])
    {
        $this->_execute("all", "before_hook_execute", func_get_args());
        $return = $this->_execute($section, $action, $input);
        $this->_execute("all", "after_hook_execute", func_get_args());
        return $return;
    }

    /**
     * Function: _execute
     * Since: 0.1.3
     * Description: Final execute hook actions
     *
     * @param $section
     * @param $action
     * @param array $input
     * @return array()
     */
    private function _execute($section, $action, $input = [])
    {
        if (isset($this->hooks[$section][$action])) {
            foreach ($this->hooks[$section][$action] as $hooks) {
                foreach ($hooks as $hook) {
                    if (!!$input) {
                        if (is_array($hook["params"]))
                            $hook["params"]["_input"] = $input;
                        else $hook["params"] = ["param" => $hook["params"], "_input" => $input];
                    }
                    if (is_array($hook["params"]))
                        $input = call_user_func_array($hook["callable"], $hook["params"]);
                    else  $input = call_user_func($hook["callable"], $hook["params"]);
                }
            }
        }
        return $input;
    }
}