<?php
/********************************************
 *
 * Config.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts:
 * Core developer - djpitrrs@gmail.com
 * Website        - www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Config;

use Nette\Neon\Neon;
use PKRS\Core\Exception\ConfigException;
use PKRS\Core\Exception\FileException;
use PKRS\Core\Exception\ServiceException;
use PKRS\Core\Service\Service;

/**
 * Class Config
 * @package PKRS\Core\Config
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
class Config extends Service
{

    private $allowed_types = [
        "object",
        "string",
        "int",
        "array",
        "file",
        "this",
        "extern",
        "service"
    ];

    private $default = []; // default config
    private $config = []; // actual config
    private $loaded = []; // loaded files
    private $services = []; // parsed services with dependencies
    private $configFile = null; // loaded config file (gived from PKRS object)

    /**
     * @param null $configFile
     * @throws ConfigException
     */
    public function __construct($configFile = null)
    {
        $this->configFile = $configFile;
        $this->parseDefault();
        if ($configFile !== null)
            $this->loadConfig($configFile);
    }

    /**
     * @return string|null
     */
    public function getConfigFile(){
        return $this->configFile;
    }

    /**
     * @param $configFile
     * @param bool $force
     * @return array
     * @throws ConfigException
     * @throws FileException
     */
    public function parseConfig($configFile, $force = false)
    {
        if (in_array(sha1($configFile), array_keys($this->loaded)) && !$force)
            return (array)$this->loaded[sha1($configFile)];
        if (file_exists($configFile)) {
            if (substr($configFile, strlen($configFile) - 5, strlen($configFile)) == ".neon") {
                $neon = new Neon();
                $conf = (array)$neon->decode(file_get_contents($configFile));
            } else if (substr($configFile, strlen($configFile) - 4, strlen($configFile)) == ".php") {
                $file = include($configFile);
                if (is_array($file))
                    $conf = $file;
                else throw new ConfigException("Config: returned value from PHP config is not array!");
            } else {
                throw new ConfigException("Config: file extension must be only .php or .neon");
            }
            $this->loaded[sha1($configFile)] = $conf;
            return $conf;
        } else {
            throw new FileException("Config: file $configFile not exists!");
        }
    }

    /**
     * @param $configFile
     * @param null $section
     * @param bool $force
     * @throws ConfigException
     * @throws FileException
     * @throws ServiceException
     */
    public function loadConfig($configFile, $section = null, $force = false)
    {
        $conf = $this->parseConfig($configFile, $force);
        foreach ($conf as $section => $value) {
            if (!isset($this->config[$section])){
                $this->config[$section] = [];
            }
            if (!is_array($value)) {
                $this->config[$section] = $value;
            } else {
                foreach ($value as $key => $val)
                    if ($val !== null)
                        $this->config[$section][$key] = $val;
            }
        }
        $this->parseServicesOveride();
    }

    /**
     * @param $configFile
     * @throws ConfigException
     * @throws ServiceException
     */
    public function forceLoad($configFile)
    {
        if (file_exists($configFile)) {
            $this->loadConfig($configFile, null, true);
            $this->parseServicesOveride();
        }
    }

    /**
     * @param $section
     * @param $arrayOfConfigs
     * @return $this
     */
    function set($section, $arrayOfConfigs)
    {
        $this->config[$section] = $arrayOfConfigs;
        return $this;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        return $this->config;
    }

    /**
     * @return array
     */
    public function getDefault(){
        return $this->default;
    }

    /**
     * @throws ConfigException
     * @throws FileException
     */
    private function parseDefault()
    {
        if (file_exists(dirname(__FILE__) . DS . "defaultConfig.neon")) {
            $this->default = $conf = $this->parseConfig(dirname(__FILE__) . DS . "defaultConfig.neon");
            foreach ($conf as $k => $v) {
                $this->set($k, $v);
            }
        } else throw new ConfigException("File defaultConfig.neon not exists!");
        if (file_exists(dirname(__FILE__) . DS . "defaultServices.neon")) {
            $conf = $this->parseConfig(dirname(__FILE__) . DS . "defaultServices.neon");
            foreach ($conf as $name => $settings) {
                if (isset($settings["class"])) {
                    $clName = "\\" . str_replace("/", "\\", trim($settings["class"], "/\\")); // fix config escaping
                    if (class_exists($clName, true)) {
                        $cache = ["class" => $settings["class"], "name" => $name, "params" => []];
                        unset($settings["class"]);
                        if (isset($settings["params"]) && is_array($settings["params"])) {
                            foreach ($settings["params"] as $type => $dependency) {
                                if (is_array($dependency)) {
                                    $c = [];
                                    foreach ($dependency as $v) {
                                        $c[] = $this->fixDependency($conf, $name, $type, $v);
                                    }
                                    $dependency = $c;
                                    unset($c);
                                } else {
                                    $dependency = [$this->fixDependency($conf, $name, $type, $dependency)];
                                }
                                $cache["params"][] = ["type" => $type, "dependency" => $dependency];
                            }
                        }
                        $this->services[strtolower($name)] = $cache;
                    } else throw new ConfigException("Service class $clName not exists!");
                } else throw new ConfigException("File defaultServices.neon corrupted! Not defined class in $name service!");
            }
        }
    }

    /**
     * Parsing extra config
     * - services_overide
     *
     * @throws ConfigException
     */
    private function parseServicesOveride()
    {
        if (!isset($this->config["services_overide"]) || !is_array($this->config["services_overide"]) || empty($this->config["services_overide"])) return;
        foreach ($this->config["services_overide"] as $name => $settings) {
            if (!in_array($name, array_keys($this->services))) throw new ServiceException("Service $name not exits! Cannot be overiden!");
            if (isset($settings["class"])) {
                $cl_name = "\\" . str_replace("/", "\\", trim($settings["class"], "/\\")); // fix config escaping
                if (class_exists($cl_name, true)) {
                    $cache = ["class" => $settings["class"], "name" => $name, "params" => []];
                    unset($settings["class"]);
                    if (isset($settings["params"]) && is_array($settings["params"])) {
                        foreach ($settings["params"] as $type => $dependency) {
                            if (is_array($dependency)) {
                                $c = [];
                                foreach ($dependency as $v) {
                                    $c[] = $this->fixDependency($this->config["services_overide"], $name, $type, $v);
                                }
                                $dependency = $c;
                                unset($c);
                            } else {
                                $dependency = [$this->fixDependency($this->config["services_overide"], $name, $type, $dependency)];
                            }
                            $cache["params"][] = ["type" => $type, "dependency" => $dependency];
                        }
                    }
                    $cache["origin_data"] = $this->services[strtolower($name)];
                    $this->services[strtolower($name)] = $cache;
                } else throw new ConfigException("Service class $cl_name not exists!");
            } else throw new ConfigException("File defaultServices.neon corrupted! Not defined class in $name service!");
        }
    }

    /**
     * @param array $allConfig
     * @param string $serviceName
     * @param string $type
     * @param string $dependency
     * @return string
     * @throws FileException
     */
    private function fixDependency($allConfig, $serviceName, $type, $dependency)
    {
        if ($type == "file") {
            // file must be relative to APP_DIR
            if (!file_exists(APP_DIR . $dependency)) {
                throw new FileException("Service $serviceName loading: file $dependency as param not exists!");
            }
        } else if ($type == "service") {
            if (!in_array($dependency, array_keys($allConfig))) {
                throw new FileException("Service $serviceName loading: service $dependency not defined!");
            }
        } else if ($type == "class") {
            $dependency = "\\" . str_replace("/", "\\", trim($dependency, "/\\"));
            if (!class_exists($dependency, true)) {
                throw new FileException("Service $serviceName loading: dependency class $dependency not exists!");
            }
        } else {
            throw new FileException("Services loading: service type $type not allowed (only file, service, class)!");
        }
        return $dependency;
    }

    /**
     * Function: getConf
     * Since: 0.1.4
     * Description: Get config value by group and group key
     *
     * @param $group
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function getConf($group, $key, $default = null){
        if (!isset($this->config[$group])) return $default;
        if (!isset($this->config[$group][$key])) return $default;
        return $this->config[$group][$key];
    }

    /**
     * Function: getConfGroup
     * Since: 0.1.4
     * Description: Get config group
     *
     * @param $group
     * @return array
     */
    public function getConfGroup($group){
        if (isset($this->config[$group])) return (array)@$this->config[$group];
        else return [];
    }

    /**
     * Get services
     *
     * @return array
     */
    public function getServices()
    {
        return (array)$this->services;
    }

}