<?php
/**************************************************************
 *
 * DbConfig.php, created 4.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Config;

use PKRS\Core\Service\Service;

/**
 * Class DbConfig
 * @package PKRS\Core\Config
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class DbConfig extends Service
{

    /**
     * @var \PKRS\Core\Database\Database
     */
    private $db;

    /**
     * @var array
     */
    private $dbConfig = [];

    /**
     * @var string
     */
    private $table, $key, $val;

    /**
     *
     */
    public function __construct()
    {
        $this->db = self::gc()->getDatabase();
        $config = self::gc()->getConfig()->getConfGroup("application");
        $this->table = $config["db_config_table"];
        $this->key = $config["db_config_key"];
        $this->val = $config["db_config_value"];
        $db_conf = $this->db->query("SELECT * FROM " . $this->table)->all(true);
        foreach ($db_conf as $conf) {
            $this->dbConfig[$conf[$this->key]] = $conf[$this->val];
        }
    }

    /**
     * @param $key
     * @return string
     */
    public function getOne($key)
    {
        return isset($this->dbConfig[$key]) ? $this->dbConfig[$key] : "";
    }

    /**
     * @return array
     */
    function get_all()
    {
        return $this->dbConfig;
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     */
    function updateValue($key, $value)
    {
        if (in_array($key, array_keys($this->dbConfig))) {
            if (
            $this->db->query("UPDATE " . $this->table . " SET " . $this->val . "=:val WHERE " . $this->key . "=:key")
                ->str("key", $key)
                ->str("val", $value)
                ->execute()
            ) {
                $this->dbConfig[$key] = $value;
                return true;
            } else return false;
        } else {
            if (
            $this->db->query("INSERT INTO " . $this->table . " (" . $this->key . ", " . $this->val . ") VALUES (:key,:val)")
                ->str("key", $key)
                ->str("val", $value)
                ->execute()
            ) {
                $this->dbConfig[$key] = $value;
                return true;
            } else return false;
        }
    }

    /**
     * @param $array
     */
    function updateMultiple($array)
    {
        foreach ($array as $k => $v) {
            $this->updateValue($k, $v);
        }
    }
}