<?php
/**************************************************************
 *
 * Model.php, created 12.11.14
 * Since: 0.1.3
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Model;

/**
 * Class MultiModel
 * @package PKRS\Core\Model
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
abstract class MultiModel extends Model
{

}