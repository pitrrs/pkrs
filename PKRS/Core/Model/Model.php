<?php
/**************************************************************
 *
 * Models.php, created 12.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Model;

use PKRS\Core\Exception\MainException;
use PKRS\Core\Service\Service;

/**
 * Class Model
 * @package PKRS\Core\Model
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
abstract class Model extends Service
{
    /**
     * SUPPORTED DATATYPES
     */
    const INT = 1;
    const VARCHAR = 2;
    const TIMESTAMP = 3;
    const TEXT = 4;
    const FLOAT = 5;
    const BOOL = 6;
    const TINYINT = 7;
    const ENUM = 8;

    /**
     * Callback to execution methods on database for datatypes
     *
     * @var array
     */
    protected $exec = [
        self::INT => "int",
        self::TINYINT => "int",
        self::FLOAT => "float",
        self::BOOL => "bool",
        self::VARCHAR => "str",
        self::TIMESTAMP => "str",
        self::TEXT => "str",
        self::ENUM => "str"
    ];

    /**
     * Database
     *
     * @var \PKRS\Core\Database\Database
     */
    var $db;

    /**
     * Dataset of object model
     *
     * @var array
     */
    var $dataset = [];


    /**
     * Dataset for dynamically generated values
     *
     * @var array
     */
    var $dynamicDataset = [];

    /**
     * Loaded primary key
     * If not loaded from database (creation), this param is null
     * After save is setted to value returned from database->getLastId
     *
     * @var mixed
     */
    var $ID = null;

    /**
     * For strings
     * Save to database as html htmlSpecialChars (XSS protection)
     *
     * @var bool
     */
    var $exportToHtmlSpecialChars = true;

    /**
     * Call func on save
     *
     * @var callable
     */
    var $onSave = null;


    /**
     * Create object model
     * Check settings consistency
     * Set default values
     * If isset $loadPrimary -> load initial data
     *
     * @param null $loadPrimary
     */
    final function __construct($loadPrimary = null)
    {
        $this->validateAndSetDefaults();
        $this->db = self::gc()->getDatabase();
        // load initial data
        if ($loadPrimary) {
            $this->load($loadPrimary);
        }
    }



    /**
     * Set new value for key
     * Constrains data types validation
     *
     * @param $key
     * @param $value
     * @return bool
     */
    public function set($key, $value)
    {
        $value = trim($value);
        if (!in_array($key, array_keys($this->structure))) {
            return false;
        } else {
            if ($this->primary["name"] == $key) {
                return false;
            }
            if ($this->structure[$key]["type"] == self::TEXT) {
                // unlimited text
                $this->dataset[$key] = $this->exportToHtmlSpecialChars ? htmlspecialchars(strval($value)) : strval($value);
            } elseif ($this->structure[$key]["type"] == self::VARCHAR) {
                // varchar is limited... This is fix
                $value = substr(strval($value), 0, (int)$this->structure[$key]["size"]);
                $this->dataset[$key] = $this->exportToHtmlSpecialChars ? htmlspecialchars(strval($value), ENT_QUOTES) : strval($value);
            } elseif ($this->structure[$key]["type"] == self::INT || $this->structure[$key]["type"] == self::TINYINT) {
                $this->dataset[$key] = intval($value);
            } elseif ($this->structure[$key]["type"] == self::FLOAT) {
                $this->dataset[$key] = floatval($value);
            } elseif ($this->structure[$key]["type"] == self::BOOL) {
                $this->dataset[$key] = boolval($value);
            } elseif ($this->structure[$key]["type"] == self::TIMESTAMP) {
                // validate mysql timestamp
                if (!self::gc()->getValidator()->isMysqlTimestamp($value)) {
                    if ($value == "CURRENT_TIMESTAMP" || $value == "NOW" || $value == "NOW()") {
                        $this->dataset[$key] = self::gc()->getTransformDateTime()->toMysqlTimestamp(time());
                        return true;
                    } else if (is_null($value) || strtolower($value)=="null") {
                        $this->dataset[$key] = null;
                    }
                    else if (is_numeric($value)){
                        // unix timestamp value
                        $this->dataset[$key] = self::gc()->getTransformDateTime()->toMysqlTimestamp($value);
                        return true;
                    }
                    return false;
                } else $this->dataset[$key] = $value;
            } elseif ($this->structure[$key]["type"] == self::ENUM) {
                if (!in_array($value, $this->structure[$key]["allowed_values"])) {
                    return -1;
                } else {
                    $this->dataset[$key] = $value; // fixed
                }
            } else {
                // unknown type = simple set it
                $this->dataset[$key] = $value;
            }
            return true;
        }
    }

    /**
     * Function: load
     * Since: 0.1.3
     * Load data from database by primary key value
     * Returns bool
     * - False - if not loaded data
     * - True - if dataset loading is ok
     *
     * @param $primaryKeyValue
     * @return bool
     */
    public function load($primaryKeyValue)
    {
        $func = $this->exec[$this->primary["type"]];
        $data = $this->db->query("SELECT * FROM `" . $this->table . "` WHERE `" . $this->primary["name"] . "`=:" . $this->primary["name"])->$func($this->primary["name"], $primaryKeyValue)->row(true);
        if (!empty($data)) {
            $this->ID = $primaryKeyValue;
            foreach ($data as $key => $value) {
                if ($key == $this->primary["name"]) continue;
                // in array array_keys is fix for NULL valued items
                if (in_array($key, array_keys($this->dataset)))
                    $this->set($key, $value);
            }
            $this->loadForeign();
        } else $this->ID = null;
        return !empty($data);
    }

    /**
     * Function: loadByCustomSQL
     * Since: 0.1.3
     * Description: Load data by custom SQL query
     *
     * @param $sql
     * @return bool|MainException|\PDOException
     */
    public function loadByCustomSQL($sql){
        try{
            $data = $this->db->query($sql)->row(true);
            if (!empty($data)) {
                $this->ID = $data[$this->primary["name"]];
                foreach ($data as $key => $value) {
                    if ($key == $this->primary["name"]) continue;
                    if (isset($this->dataset[$key]))
                        $this->set($key, $value);
                }
                $this->loadForeign();
            } else $this->ID = null;
            return !empty($data);
        } catch (\PDOException $e){
            return $e;
        }
    }

    /**
     * @param array $searchTerms
     * @return bool
     * @throws MainException
     */
    public function loadBySearchTerms(array $searchTerms){
        if (empty($searchTerms)) return false;
        $where = [];
        foreach($searchTerms as $key=>$value){
            if (!in_array($key, array_keys($this->structure)))
                throw new MainException("Model: Key $key not exists");
            $where[] = "`$key`=:$key";
        }
        $where = implode(" AND ",$where);
        $fluent = $this->db->query("SELECT * FROM `".$this->table."` WHERE ".$where." LIMIT 1");
        foreach($searchTerms as $key=>$value){
            $func = $this->exec[$this->structure[$key]["type"]];
            $fluent->$func($key, $value);
        }
        $data = $fluent->row(true);
        if (!empty($data)){
            if (!isset($data[$this->primary["name"]])){
                throw new MainException("Model: Primary key not loaded on $this->table!");
            }
            foreach($data as $key=>$value)
                $this->set($key, $value);
            $this->ID = $data[$this->primary["name"]];
            return $this->loadForeign();
        }
        $this->validateAndSetDefaults();
        return false;
    }

    /**
     * Function: newInstance
     * Since: 0.1.3
     * Description: Create new instance of this object
     *
     * @param null $loadPrimary
     * @return mixed
     */
    public function newInstance($loadPrimary = null)
    {
        return new $this($loadPrimary);
    }

    /**
     * Function: setExportingToHtmlSpecialChars
     * Since: 0.1.3
     * Description: Set saving strings to database as HTML htmlSpecialChars (XSS protection)
     *
     * @param bool $export
     * @return void
     */
    public function setExportingToHtmlSpecialChars($export = true)
    {
        $this->exportToHtmlSpecialChars = !!$export;
    }

    /**
     * Function: setMultiple
     * Since: 0.1.3
     * Description: Set values multiple
     * If not setted all, returns false and refresh old dataset
     *
     * @param array $array
     * @return bool
     */
    public function setMultiple(array $array)
    {
        $before = $this->dataset;
        foreach ($array as $key => $value) {
            if (!$this->set($key, $value)) {
                // error ocurred - refresh old dataset
                $this->dataset = $before;
                return false;
            }
        }
        return true;
    }

    /**
     * Function: get
     * Since: 0.1.3
     * Description:
     * Get dataset value by key
     * If not exists, return null
     *
     * @param $key
     * @return mixed
     */
    public function get($key)
    {
        if (empty($this->dataset)) return false;
        if (!in_array($key, array_keys($this->structure))) {
            return null;
        } else return $this->dataset[$key];
    }

    /**
     * Function: getAll
     * @since: 0.1.3
     * Description: Get all values in dataset
     *
     * @return array|bool
     */
    public function getAll()
    {
        if (!$this->ID) return false;
        $return = [$this->primary["name"]=>$this->ID];
        foreach ($this->dataset as $key=>$value){
            $return[$key] = $value;
        }
        foreach ($this->dynamicDataset as $key=>$value){
            $return[$key] = $value;
        }
        return $return;
    }

    /**
     * Return true if dataset loaded (isset ID)
     *
     * @return bool
     */
    function isLoaded()
    {
        return $this->ID !== null;
    }

    /**
     * Check if loaded primary key is same as given param
     *
     * @param $primaryKeyValue
     * @return bool
     */
    function isLoadedPrimary($primaryKeyValue)
    {
        return $this->ID == $primaryKeyValue;
    }

    /**
     * Save dataset to database
     * If isset primary key value, dataset has been updated else dataset has been inserted
     *
     * Imitation if for development (NOT SAVE TO DATABASE)
     *
     * @var bool $imitation
     * @return bool
     */
    public function save($imitation = false)
    {
        $r = false;
        if (!empty($this->dataset)) {
            if ($imitation)
                $r = rand(0,5)>1 ? true : false;
            $r = $this->ID ? $this->update() : $this->create();
        } else $r = false;
        if ($r && is_callable($this->onSave)){
            call_user_func($this->onSave,$this->dataset);
        }
        return $r;
    }

    /**
     * Delete dataset from database
     * If deleted, this data has been
     */
    public function delete()
    {
        if ($this->ID === null) return false;
        $primary_type = $this->exec[$this->primary["type"]];
        $primary_name = $this->primary["name"];
        $fluent = $this->db->query("DELETE FROM `" . $this->table . "` WHERE `" . $primary_name . "`=:" . $primary_name);
        $fluent->$primary_type($primary_name, $this->ID);
        $result = $fluent->execute();
        if ($result) {
            $this->ID = null;
            $this->validateAndSetDefaults();
        }
        return $result;
    }

    /**
     * Validate class settings
     * Set defaults values (if isset)
     *
     * @throws MainException
     */
    private function validateAndSetDefaults()
    {
        // check consistency
        if (!isset($this->table)) {
            throw new MainException("Table not defined");
        }
        if (!isset($this->primary)) {
            throw new MainException("Primary key not defined");
        }
        if (!isset($this->structure)) {
            throw new MainException("Structure not defined");
        }
        if (!is_array($this->primary)) {
            throw new MainException("Primary must be array");
        }
        if (!isset($this->primary["name"])) {
            throw new MainException("Primary settings must contains name");
        }
        if (!isset($this->primary["type"])) {
            throw new MainException("Primary settings must contains type");
        }
        if (!is_array($this->structure)) {
            throw new MainException("Structure must be array");
        }
        if (in_array($this->primary["name"], array_keys($this->structure))) {
            throw new MainException("Primary key is redefined in structure");
        }
        // get class constants (data types validation)
        $oClass = new \ReflectionClass(__CLASS__);
        $consts = $oClass->getConstants();
        foreach ($this->structure as $key => $settings) {
            $this->dataset[$key] = null;
            if (!is_array($settings)) {
                throw new MainException("Structure: \$structure[$key] must be array");
            }
            if (!isset($settings["type"])) {
                throw new MainException("Structure: not isset \$structure[$key][type]");
            } else {
                if (!in_array($settings["type"], $consts)) {
                    throw new MainException("Structure: \$structure[$key][type] is not allowed type");
                }
            }
            // checking data validation
            if ($settings["type"] == self::VARCHAR) {
                if (!isset($settings["size"])) {
                    throw new MainException("Structure: not isset \$structure[$key][size] in VARCHAR type");
                }
            }
            if ($settings["type"] == self::ENUM) {
                if (!isset($settings["allowed_values"])) {
                    throw new MainException("Structure: not isset \$structure[$key][allowed_values] in ENUM type");
                }
                if (!is_array($settings["allowed_values"])) {
                    throw new MainException("Structure: \$structure[$key][allowed_values] is not array");
                }
            }
            // default data set
            if (isset($settings["default"])) {
                if (!$this->set($key, $settings["default"])) {
                    throw new MainException("Default value for $key id not valid");
                }
            } else {
                if ($settings["type"] == self::TEXT || $settings["type"] == self::VARCHAR) {
                    $this->dataset[$key] = "";
                } else if ($settings["type"] == self::INT || $settings["type"] == self::FLOAT || $settings["type"] == self::TINYINT) {
                    $this->dataset[$key] = 0;
                } else if ($settings["type"] == self::TIMESTAMP) {
                    $this->dataset[$key] = "0000-00-00 00:00:00";
                } else if ($settings["type"] == self::BOOL) {
                    $this->dataset[$key] = null;
                } else if ($settings["type"] == self::ENUM) {
                    $this->dataset[$key] = null;
                }
            }
        }
        if (isset($this->htmlspecialchars)) {
            $this->exportToHtmlSpecialChars = !!$this->htmlspecialchars;
        }
    }

    /**
     * Update dataset
     *
     * @return bool
     */
    private function update()
    {
        if (empty($this->dataset)) return false;
        if ($this->ID === null) return $this->create();
        $primaryName = $this->primary["name"];
        $primaryType = $this->exec[$this->primary["type"]];
        $sqlPrepare = [];
        $binding = [];
        foreach ($this->structure as $key => $settings) {
            if ($key == $primaryName) continue;
            $sqlPrepare[] = "`$key` = :$key";
            $binding[$key] = $this->exec[$settings["type"]];
        }
        $fluent = $this->db->query("UPDATE `" . $this->table . "` SET " . implode(", ", $sqlPrepare) . " WHERE `" . $this->primary["name"] . "`=:" . $this->primary["name"] . ";");
        foreach ($binding as $key => $type) {
            $fluent->$type($key, $this->dataset[$key]);
        }
        $fluent->$primaryType($primaryName, $this->ID);
        $result = $fluent->execute();
        return $result;
    }

    /**
     * Insert dataset
     *
     * @return bool
     */
    private function create()
    {
        if (empty($this->dataset)) return false;
        if ($this->ID !== null) return $this->create();
        $primaryName = $this->primary["name"];
        $sqlPrepare = [];
        $binding = [];
        foreach ($this->structure as $key => $settings) {
            if ($key == $primaryName) continue;
            $sqlPrepare[":" . $key] = "`$key`";
            $binding[$key] = $this->exec[$settings["type"]];
        }
        $fluent = $this->db->query("INSERT INTO `" . $this->table . "` (" . implode(", ", $sqlPrepare) . ") VALUES (" . implode(", ", array_keys($sqlPrepare)) . ");");
        foreach ($binding as $key => $type) {
            $fluent->$type($key, $this->dataset[$key]);
        }
        $result = $fluent->execute();
        if ($result)
            $this->ID = $this->db->getLastId();
        else $this->ID = null;
        return $result;
    }


    /**
     * @return bool
     */
    private function loadForeign(){
        if (!$this->isLoaded()) return false;
        $this->dynamicDataset = [];
        foreach($this->structure as $key => $column){
            if (isset($column["foreign_keys"]) && is_array($column["foreign_keys"])){
                foreach($column["foreign_keys"] as $foreign){
                    $fluent = $this->db->query("SELECT * FROM `".$foreign["table"]."` WHERE `".$foreign["column"]."`=:".$foreign["column"].";");
                    $func = $this->exec[$this->structure[$key]["type"]];
                    $fluent->$func($foreign["column"], $this->dataset[$key]);
                    $data = $fluent->all(true);
                    if (empty($data))
                        $data = [];
                    if (count($data)==1){
                        $data = $data[0];
                    }
                    $this->dynamicDataset[$foreign["column"]] = $data;
                }
            }
        }
        return true;
    }
}