<?php
/**************************************************************
 *
 * Generator.php, created 14.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Model;

use PKRS\Core\Exception\MainException;
use PKRS\Core\Service\Service;

/**
 * Class Generator
 * @package PKRS\Core\Model
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Generator extends Service{

    /**
     * SUPPORTED DATATYPES
     */
    const INT = 1;
    const VARCHAR = 2;
    const TIMESTAMP = 3;
    const TEXT = 4;
    const FLOAT = 5;
    const BOOL = 6;
    const TINYINT = 7;
    const ENUM = 8;

    /**
     * @var \PKRS\Core\Database\Database
     */
    private $db;

    /**
     * @var array
     */
    private $tableData = [];

    /**
     * @var array
     */
    private $primary = [];

    /**
     * @var string
     */
    private $namespace = "Models";

    /**
     * @var bool
     */
    private $htmlSpecialChars = false;

    /**
     * Set this to true if you want creating target dir
     *
     * @var bool
     */
    var $autoCreateDir = false;

    /**
     * Set this to true if you want rewrite old existing file
     *
     * @var bool
     */
    var $rewriteOldFile = false;

    /**
     * This table has foreign keys
     *
     * @var bool
     */
    private $multiMode = false;

    /**
     *
     */
    public function __construct(){
        $this->db = self::gc()->getDatabase();
    }

    /**
     * @param $namespace
     */
    public function setNamespace($namespace){
        $this->namespace = strval(trim($namespace,"\\"));
    }

    /**
     * @param bool|true $setHtmlSpecialCharsExporting
     */
    public function setHtmlSpecialChars($setHtmlSpecialCharsExporting = true){
        $this->htmlSpecialChars = !!$setHtmlSpecialCharsExporting;
    }

    /**
     * @return bool
     * @throws MainException
     */
    public function generateAllTables($stopOnError = false){
        $tables = $this->db->query("SHOW TABLES")->all(true);
        $database = self::gc()->getConfig()->getConf("database","name");
        foreach ($tables as $table){
            $table = $table["Tables_in_".$database];
            try {
                if (!$this->generate($table)) {
                    if ($stopOnError)
                        throw new MainException("Fatal error: $table not generated!");
                    else continue;
                }
            } catch (MainException $e){
                if ($stopOnError)
                    throw new MainException("Fatal error: $table not generated!");
                else continue;
            }
        }
        return true;
    }

    /**
     * @param $tableName
     * @return bool
     * @throws MainException
     */
    public function generate($tableName){
        $reflectionClass = new \ReflectionClass(__CLASS__);
        $constants = $reflectionClass->getConstants();
        $table = $this->db->query("DESCRIBE `".$tableName."`")->all(true);
        if (!$table) return false;
        else {
          //  var_dump($table);exit;
            $foreign = $this->db->query("SELECT * FROM information_schema.key_column_usage WHERE referenced_table_name IS NOT NULL AND table_schema = '".self::gc()->getConfig()->getConf("database","name")."' AND tableName = '".$tableName."'")->all(true);
            $this->multiMode = !empty($foreign);
            $tableDesc = [];
            $primary = ["name"=>"","type"=>"varchar"];
            $primarySetted = false;
            foreach($table as $field){
                $type = strtoupper(explode("(",$field["Type"])[0]);
                if ($type == "BIGINT")
                    $type = "INT";
                if ($type == "TINYINT")
                    $type = "INT";
                if (!in_array($type,array_keys($constants))) return false;//throw new MainException("Field type $type not supported");
                if ($field["Key"] == "PRI" && $primarySetted == false){
                    $primary["name"] = $field["Field"];
                    $primarySetted = true;
                    $primary["type"] = $type;
                } else {
                    $tableDesc[$field["Field"]] = [
                        "type" => $type
                    ];
                    if ($type == "VARCHAR"){
                        $tableDesc[$field["Field"]]["size"] = intval(explode("(",$field["Type"])[1]);
                    }
                    if ($type == "ENUM") {
                        $val = eval("return array(".explode("(",$field["Type"])[1].";");
                        $tableDesc[$field["Field"]]["allowed_values"] = $val;
                    }
                    // Search foreign keys
                    foreach($foreign as $f){
                        if ($f["COLUMN_NAME"]==$field["Field"]){
                            if (!isset($tableDesc[$field["Field"]]["foreign_keys"]))
                                $tableDesc[$field["Field"]]["foreign_keys"] = [];
                            $tableDesc[$field["Field"]]["foreign_keys"][] = [
                                "table"=>$f["REFERENCED_TABLE_NAME"],
                                "column"=>$f["REFERENCED_COLUMN_NAME"]
                            ];
                        }
                    }
                }
            }
            if (!$primarySetted)
                throw new MainException("Table $tableName has no primary key");
            $this->primary = $primary;
            $this->tableData = $tableDesc;
            return $this->createModel($tableName);
        }
    }

    /**
     * @param $table_name
     * @return bool
     * @throws MainException
     */
    private function createModel($table_name){
        $orig_name = $table_name;
        $table_name = ucfirst(strtolower($table_name));
        if (file_exists(APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\")).DS.$table_name.".php")){
            if (!$this->rewriteOldFile)
                throw new MainException("File ".APP_DIR."Models".DS.$table_name.".php exists!");
            else unlink(APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\")).DS.$table_name.".php");
        }
        if (!is_dir(APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\")))){
            if (!$this->autoCreateDir)
                throw new MainException("Target dir ".APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\"))." not exists");
            else {
                if (!mkdir(APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\")))){
                    return false;
                }
            }
        }
        $h = fopen(APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\")).DS.$table_name.".php", "w+");
        if (!is_resource($h)) throw new MainException("Can not create file ".APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\")).DS.$table_name.".php");
        $date = date("d.m.y");
        $namespace = $this->namespace;
        $structure = var_export($this->tableData, true);
        $htmlspecialchars = $this->htmlSpecialChars ? "true" : "false";
        $extends = $this->multiMode ? "MultiModel" : "Model";
        $primary = var_export($this->primary, true);
        $year = date("Y");
        $write = <<<FILE
<?php
/**************************************************************
 *
 * $table_name.php, created $date
 *
 * Copyright (C) $year by Petr Klimes & development team
 *
 * This file is auto generated with tool Model\Generator
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace $namespace;

use PKRS\Core\Model\\$extends;

class $table_name extends $extends{

    protected \$table = "$orig_name";
    protected \$primary = $primary;
    protected \$structure = $structure;
    protected \$htmlSpecialChars = $htmlspecialchars;


FILE;
        $oClass = new \ReflectionClass(__CLASS__);
        $consts = array_keys($oClass->getConstants());
        foreach($consts as $c){
            $write = str_replace("'".$c."'","self::$c",$write);
        }
        foreach($this->tableData as $s=>$settings){
            $func = strtolower($s);
            $type = $settings["type"];
            $type = ($type == "VARCHAR" || $type == "TEXT" || $type == "ENUM" || $type == "TIMESTAMP" ? "string" :
                ($type == "INT" || $type == "TINYINT" ? "int" :
                    ($type == "FLOAT" ? "float" :
                        ($type == "BOOL" ? "bool" :
                                    "mixed"))));
            $write .= <<<FILE
    /**
     * @param $type \$value
     * @return $table_name
     */
    function set_$func(\$value){
        \$this->set("$s", \$value);
        return \$this;
    }
    /**
     * @return $type
     */
    function get_$func(){
        return \$this->get("$s");
    }


FILE;

        }
        fwrite($h, $write.PHP_EOL."}");
        if (!fclose($h)){
            throw new MainException("Can not close file ".APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\")).DS.$table_name.".php");
        }
        include_once(APP_DIR.str_replace("\\",DS,trim($this->namespace,"\\")).DS.$table_name.".php");
        return class_exists(trim($namespace,"\\")."\\".$table_name);
    }

}