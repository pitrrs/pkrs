<?php
/********************************************
 *
 * Lang.php, created 8.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Lang;

use PKRS\Core\Exception\LangException;
use PKRS\Core\Service\Service;
use PKRS\Core\Service\ServiceContainer;

/**
 * Class Lang
 * @package PKRS\Core\Lang
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Lang extends Service
{

    private static $loaded = false;

    var $test;

    private static $lang = [];

    /**
     * @return array
     */
    public static function getAllowedLangs()
    {
        $allowed = [];
        foreach (new \DirectoryIterator(APP_DIR . DS . 'Lang') as $fileInfo) {
            if ($fileInfo->isDot()) continue;
            if (substr($fileInfo->getFilename(), strlen($fileInfo->getFilename()) - 4, 4) == ".ini")
                $allowed[] = substr($fileInfo->getFilename(), 0, strlen($fileInfo->getFilename() - 4));
        }
        return $allowed;
    }

    /**
     * @return array
     */
    public static function getAllLangs()
    {
        return self::$lang;
    }

    /**
     * @param null $force
     * @return null
     * @throws \PKRS\Core\Exception\MainException
     */
    public static function detectLang($force = null)
    {
        if ($force !== null && in_array($force, self::getAllowedLangs())) {
            $_SESSION["lang"] = $force;
            return $force;
        }
        $allowed = self::getAllowedLangs();
        if (isset($_GET["lang"])) {
            if (in_array($_GET["lang"], $allowed)) {
                $_SESSION["lang"] = $_GET["lang"];
            } else {
                ServiceContainer::getInstance()->getMessages()->add("Lang not exists!");
            }
        }
        if (isset($_SESSION["lang"]) && in_array($_SESSION["lang"], $allowed)) {
            return $_SESSION["lang"];
        }
        $languages = [];
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $langParse);
            if (count($langParse[1])) {
                $languages = array_combine($langParse[1], $langParse[4]);
                foreach ($languages as $lang => $val) {
                    if ($val === '') $languages[$lang] = 1;
                }
                arsort($languages, SORT_NUMERIC);
            }
        }
        foreach ($languages as $l) {
            if (in_array($l, $allowed)) {
                $_SESSION["lang"] = $l;
                return $l;
            }
        }
        $_SESSION["lang"] = DEFAULT_LANG;
        return DEFAULT_LANG;
    }

    /**
     * @param $language
     * @throws LangException
     */
    public static function init($language)
    {
        self::gc()->getHooks()->execute("lang", "on_create");
        if (!in_array($language, self::getAllowedLangs())) {
            $language = self::detectLang(DEFAULT_LANG);
        }
        if (file_exists(APP_DIR . DS . "Lang" . DS . $language . ".ini")) {
            self::$lang = parse_ini_file(APP_DIR . DS . "Lang" . DS . $language . ".ini");
        } else throw new LangException("Lang file " . $language . " not exists!");
    }

    /**
     * For Smarty plugin {t}LANG_KEY{/t}
     *
     * @param $params
     * @param $name
     * @param \Smarty_Internal_Template $smarty
     * @return mixed|string
     * @throws LangException
     * @throws \Exception
     * @throws \SmartyException
     */
    public static function translate($params, $name, \Smarty_Internal_Template &$smarty)
    {
        if (self::$loaded === false) {
            $config = ServiceContainer::getInstance()->getConfig();
            self::init($config->getConf("application", "default_lang", "cs"));
            self::$loaded = true;
        }
        $translation = '';
        if (!is_null($name) && is_array(self::$lang) && array_key_exists($name, self::$lang)) {
            self::gc()->getHooks()->execute("lang", "on_get", ["str"=>$name]);
            // get variables in translation text
            $translation = self::$lang[$name];
            preg_match_all('/##([^#]+)##/i', $translation, $vars, PREG_SET_ORDER);

            // replace with assigned smarty values
            foreach ($vars as $var) {
                $translation = str_replace($var[0], $smarty->getTemplateVars($var[1]), $translation);
            }

        } else {
            if (trim($name) != "")
                if (defined("DEV") && DEV) {
                    throw new LangException("Lang key $name not exists!");
                } else {
                    return $name;
                }
        }

        foreach ($params as $c) $translation = sprintf($translation, strpos($c,"{") !== false ? self::gc()->getView()->smarty()->fetch("string:".$c) : $c);
        //$translation = call_user_func_array("sprintf", $params);

        return $translation;

    }

    /**
     * Classic translate
     *
     * @param $str
     * @return string
     * @throws LangException
     */
    public static function xlate($str)
    {
        if (self::$loaded === false) {
            $config = ServiceContainer::getInstance()->getConfig();
            self::init($config->getConf("application", "default_lang", "cs"));
            self::$loaded = true;
        }
        $arg_count = func_num_args();
        self::gc()->getHooks()->execute("lang", "on_get", ["str"=>$str]);
        if ($arg_count > 1) {
            $params = func_get_args();
            array_shift($params);
        } else {
            $params = [];
        }

        $out_str = isset(self::$lang[$str]) ? self::$lang[$str] : null;

        if (!$out_str) {
            throw new LangException("Lang String Not Found in language " . $_SESSION["lang"] . " : $str");
        }
        return vsprintf($out_str, $params);
    }

}