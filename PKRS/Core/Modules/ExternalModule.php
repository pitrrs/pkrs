<?php

namespace PKRS\Core\Modules;


use PKRS\Core\Service\Service;

/**
 * Class ExternalModule
 * @package PKRS\Core\Modules
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klime� <djpitrrs@gmail.com>
 */
abstract class ExternalModule extends Service implements IExternalModule{

    /**
     * @var \PKRS\Core\Service\ServiceContainer
     */
    var $sc;

    /**
     *
     */
    public function __construct(){
        $this->sc = self::gc();
    }

    /**
     * @param $serviceName
     * @param $className
     */
    final function overideService($serviceName, $className){
        $services = $this->sc->getConfig()->getServices();
    }

}