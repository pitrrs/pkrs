<?php
/**************************************************************
 *
 * PHPModule.php, created 5.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Modules;

use PKRS\Core\Exception\ConfigException;
use PKRS\Core\Exception\ServiceException;
use PKRS\Core\Service\ServiceContainer;

/**
 * Class PHPModule
 * @package PKRS\Core\Modules
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
abstract class PHPModule extends BaseModule implements IPHPModule
{

    private static $serviceContainer;
    var $dbPrefix;
    private static $loaded = [];

    /**
     * @param ServiceContainer $serviceContainer
     * @param string $dbPrefix
     */
    final public function __construct(ServiceContainer $serviceContainer, $dbPrefix = "")
    {
        parent::__construct();
        $this->dbPrefix = $dbPrefix;
        self::$serviceContainer = $serviceContainer;
        self::$serviceContainer->getHooks()->execute("modules", "on_create");
        $this->start();
    }

    /**
     * @return ServiceContainer
     */
    final static function gc()
    {
        return self::$serviceContainer;
    }

    /**
     * @param $name
     * @param string $prefix
     * @return mixed
     * @throws ServiceException
     */
    final static function getModule($name, $prefix = "")
    {
        if (isset(self::$loaded[$name . ($prefix != "" ? "-$prefix" : "")])) {
            return self::$loaded[$name . ($prefix != "" ? "-$prefix" : "")];
        } else {
            throw new ServiceException("Get module: Module " . $name . ($prefix != "" ? "-$prefix" : "") . " not loaded and configured!");
        }
    }

    /**
     * @param ServiceContainer $serviceContainer
     * @param array $configModules
     * @throws ConfigException
     * @throws ServiceException
     */
    final static function runModules(ServiceContainer $serviceContainer, $configModules = [])
    {
        foreach ($configModules as $name => $actions) {
            if (is_array($actions)) {
                foreach ($actions as $pref => $data) {
                    if (in_array($name, array_keys(self::$loaded))) continue;
                    $data = explode(",", $data);
                    $className = "\\Modules\\PHPModules\\$name\\$name";
                    if (!class_exists($className, true)) throw new ServiceException("Module $name not exists");
                    $object = new $className($serviceContainer, $pref);
                    if (!is_subclass_of($object, "\\PKRS\\Core\\Modules\\PHPModule")) throw new ServiceException("Module $name is not instance of \\PKRS\\Core\\Modules\\PHPModule()");
                    foreach ($data as $action) {
                        if ($action == "start") continue;
                        if (method_exists($object, $action)) {
                            $res = $object->$action($pref);
                            if ($action == "_install") {
                                if ($res) self::gc()->getMessages("Module $name installed");
                                else self::gc()->getMessages("Module $name not installed", "err");
                            }
                            if ($action == "_remove") {
                                if ($res) self::gc()->getMessages("Module $name removed");
                                else self::gc()->getMessages("Module $name not removed", "err");
                            }
                        } else throw new ConfigException("Modules: action $action not exists on $name-$pref!");
                    }
                    self::$loaded[$name . "-" . $pref] = $object;
                }
            } else {
                if (in_array($name, array_keys(self::$loaded))) continue;
                $actions = explode(",", $actions);
                $className = "\\Modules\\PHPModules\\$name\\$name";
                if (!class_exists($className, true)) throw new ServiceException("Module $name not exists");
                $object = new $className($serviceContainer);
                if (!is_subclass_of($object, "\\PKRS\\Core\\Modules\\PHPModule")) throw new ServiceException("Module $name is not instance of \\PKRS\\Core\\Modules\\PHPModule()");
                foreach ($actions as $action) {
                    if ($action == "start") continue;
                    if (method_exists($object, $action)) $object->$action();
                    else throw new ConfigException("Modules: action $action not exists on $name!");
                }
                self::$loaded[$name] = $object;
            }
        }
    }

}