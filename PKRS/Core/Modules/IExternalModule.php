<?php
/**
 * Created by PhpStorm.
 * User: Pitrrs
 * Date: 22.7.2015
 * Time: 10:07
 */

namespace PKRS\Core\Modules;


/**
 * Interface IExternalModule
 * @package PKRS\Core\Modules
 */
interface IExternalModule {

    /**
     * @param $config
     * @return mixed
     */
    public function run($config);

}