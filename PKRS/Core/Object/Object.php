<?php
/********************************************
 *
 * Object.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Object;

use PKRS\Core\Exception\MainException;

/**
 * Class Object
 * @package PKRS\Core\Object
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Object
{

    /**
     * @return string
     */
    public function get_class_name()
    {
        return get_called_class();
    }

    /**
     * @return \ReflectionClass
     */
    public function getReflection(){
        return new \ReflectionClass($this->get_class_name());
    }

    /**
     * @param $method
     * @param $neco
     * @throws MainException
     */
    public function __call($method, $neco){
        if (!method_exists($this, $method)){
            // did you mean ... ?
            $mean = $this->_ClassDidYouMeanMethod($method);
            throw new MainException("Cannot call ".$this->get_class_name()."::$method() (not declared).".($mean ? " Did you mean ".$this->get_class_name()."::$mean()?" : ""), E_WARNING);
        }
    }


    /**
     * @param $property
     * @return mixed
     * @throws MainException
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        } else {
            // hawe we getter?
            $method = "get".ucfirst($property);
            if (method_exists($this, $method)){
                return $this->$method();
            } else {
                // did you mean ... ?
                $mean = $this->_ClassDidYouMeanProperty($property);
                throw new MainException("Cannot get ".$this->get_class_name()."::\$$property property (not declared).".($mean ? " Did you mean ".$this->get_class_name()."::\$$mean?" : ""), E_WARNING);
            }
        }
    }

    /**
     * @param $property
     * @param $value
     * @return bool
     * @throws MainException
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
            return true;
        } else {
            // hawe we setter?
            $method = "set".ucfirst($property);
            if (method_exists($this, $method)){
                return $this->$method($value);
            } else{
                // did you mean ... ?
                $mean = $this->_ClassDidYouMeanProperty($property);
                throw new MainException("Cannot write to ".$this->get_class_name()."::\$$property property (not declared).".($mean ? " Did you mean ".$this->get_class_name()."::\$$mean?" : ""), E_WARNING);
            }
        }
    }

    /**
     * @param $property
     * @return bool
     */
    private function _ClassDidYouMeanProperty($property){
        $properties = $this->getReflection()->getProperties();
        $newProperty = null;
        foreach($properties as $p){
            similar_text($p->getName(), $property, $percent);
            if ($newProperty == null || $newProperty["p"] < $percent){
                $newProperty = ["p"=>$percent, "n"=>$p->getName()];
            }
        }
        if (is_array($newProperty))
            return $newProperty["n"];
        return false;
    }

    /**
     * @param $method
     * @return bool
     */
    private function _ClassDidYouMeanMethod($method){
        $methods = $this->getReflection()->getMethods();
        $newMethod = null;
        foreach($methods as $m){
            similar_text($m->getName(), $method, $percent);
            if ($newMethod == null || $newMethod["p"] < $percent){
                $newMethod = ["p"=>$percent, "n"=>$m->getName()];
            }
        }
        if (is_array($newMethod))
            return $newMethod["n"];
        return false;
    }

}