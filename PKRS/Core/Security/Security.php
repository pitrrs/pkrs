<?php
/**************************************************************
 *
 * Security.php, created 21.11.14
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Security;

use PKRS\Core\Exception\MainException;
use PKRS\Core\Service\Service;

/**
 * Class Security
 * @package PKRS\Core\Security
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Security extends Service{

    /**
     * Session handler
     *
     * @var \PKRS\Core\Session\Session
     */
    var $session=null;

    /**
     * @var int
     */
    private $lifetime = 1800; // lifetime seconds (default 30 minutes)

    /**
     * @param null $lifetime
     * @throws MainException
     */
    public function __construct($lifetime = null){
        if (!is_null($lifetime)) $this->lifetime = intval($lifetime);
        // We need to use SESSIONs for storing tokens
        $this->session = self::gc()->getSession();
        // Create valid session if not exists
        if (!$this->session->start(self::gc()->getConfig()))
            throw new MainException("Session not started");
        if (!isset($_SESSION["_PKRS_PROTECTOR"])) $_SESSION["_PKRS_PROTECTOR"] = [];
        if (!isset($_SESSION["_PKRS_PROTECTOR"]["_FORMS"])) $_SESSION["_PKRS_PROTECTOR"]["_FORMS"] = [];
        // remove old tokens (older than lifetime (config))
        foreach($_SESSION["_PKRS_PROTECTOR"]["_FORMS"] as $id => $form){
            $lifetime = $form["lifetime"];
            if ($lifetime<time()){
                unset($_SESSION["_PKRS_PROTECTOR"]["_FORMS"][$id]);
            }
        }
    }

    /**
     * Function: getToken
     * Since: 0.1.4
     * Description: Create token if not exists else return existing token
     *
     * @param $identifier
     * @return string
     */
    function getToken($identifier){
        // hide human readable data
        $identifier = sha1($identifier);
        if (isset($_SESSION["_PKRS_PROTECTOR"]["_FORMS"][$identifier])){
            // yess... Token exists, return it
            return $_SESSION["_PKRS_PROTECTOR"]["_FORMS"][$identifier]["token"];
        }
        // Not exists - generate new
        $token = self::gc()->getGeneratorsRandoms()->getStringByLength(rand(15,35));
        // set session token
        $_SESSION["_PKRS_PROTECTOR"]["_FORMS"][$identifier] = [
            "lifetime"=>time()+$this->lifetime,
            "token"=> $token
        ];
        // return new token
        return $token;
    }

    /**
     * Function: validateToken
     * Since: 0.1.4
     * Description: Validate if token exists && if match
     *
     * @param $identifier
     * @param $token
     * @return bool
     */
    function validateToken($identifier, $token){
        $identifier = sha1($identifier);
        // if not exists - return false
        if (!isset($_SESSION["_PKRS_PROTECTOR"]["_FORMS"][$identifier])){
            return false;
        } else {
            $sec = $_SESSION["_PKRS_PROTECTOR"]["_FORMS"][$identifier];
            // if exists compare it and return if match
            return $sec["token"] == $token;
        }
    }


    /**
     * Function: removeToken
     * Since: 0.1.4
     * Description: Remove old token
     *
     * @param $identifier
     * @return bool
     */
    function removeToken($identifier){
        if (isset($_SESSION["_PKRS_PROTECTOR"]["_FORMS"][$identifier])){
            unset($_SESSION["_PKRS_PROTECTOR"]["_FORMS"][$identifier]);
            return true;
        }
        return false;
    }


}