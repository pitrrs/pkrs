<?php
/********************************************
 *
 * Handler.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Session;

use PKRS\Core\Config\Config;
use PKRS\Core\Exception\MainException;
use PKRS\Core\Service\Service;

/**
 * Class Session
 * @package PKRS\Core\Session
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Session extends Service
{
    private static $started = false;

    /**
     * [user] = > auth
     * [lang] = > lang
     * [messages]
     * @param Config $config
     * @return bool
     * @throws MainException
     */
    public static function start(Config $config)
    {
        if (self::$started) return true;
        if (session_status() == PHP_SESSION_NONE) {
            if (!@session_start()){
                throw new MainException("Session not started");
            }
        }
        $skey = $config->getConf("user", "session_key");
        if (!isset($_SESSION[$skey])){
            $_SESSION[$skey] = [];
        }
        if (!isset($_SESSION["lang"])){
            $_SESSION["lang"] = $config->getConf("application", "default_lang", "cs");
        }
        if (!isset($_SESSION["messages"])) {
            $_SESSION["messages"] = [
                "err" => [],
                "ok" => [],
                "warn" => []
            ];
        }
        self::gc()->getHooks()->execute("session", "on_start", $_SESSION);
        self::$started = true;
        return true;
    }

    public static function close()
    {
        self::gc()->getHooks()->execute("session", "on_close", isset($_SESSION) ? $_SESSION : null);
        @session_write_close();
    }

    /**
     * @param $key
     * @return bool
     */
    public static function is_set($key)
    {
        return isset($_SESSION[$key]) && !empty($_SESSION[$key]);
    }
}