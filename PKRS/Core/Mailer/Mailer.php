<?php
/**************************************************************
 *
 * Mailer.php, created 4.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Mailer;

use PKRS\Core\Exception\FileException;
use PKRS\Core\Service\Service;

/**
 * Class Mailer
 * @package PKRS\Core\Mailer
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Mailer extends Service
{

    private $mailer;
    private $vars = [];
    private $template = '';
    private $config = [];
    private $from = [];
    private $sendTo = [];
    private $reply_to = [];
    private $_html; // overide $smarty->fetch, if setted, $this->template not used
    private $css = [];
    private $lastMailHtml = '';
    private $subject = "";
    private $imitation = false;
    private $errors = null;
    private $tpl_dir = null;

    /**
     * @param \PHPMailer $PHPMailer
     */
    public function __construct(\PHPMailer $PHPMailer)
    {
        $this->mailer = $PHPMailer;
        self::gc()->getHooks()->execute("mailer", "on_create");
        $this->reset_mailer();
    }

    /**
     * @return $this
     */
    public function reset_mailer()
    {
        $this->mailer = new \PHPMailer();
        $this->mailer->CharSet = "UTF-8";
        $this->config = self::gc()->getConfig()->getConfGroup("mailer");
        if ($this->config["use_smtp"] == "true" || $this->config["use_smtp"] === true) {
            $this->mailer->IsSMTP();

            $this->mailer->Host = $this->config["smtp_host"];
            $this->mailer->SMTPAuth = $this->config["smtp_use_auth"] == "true" || $this->config["smtp_use_auth"]===true;
            $this->mailer->Username = $this->config["smtp_user"];
            $this->mailer->Password = $this->config["smtp_pass"];
            $this->mailer->SMTPSecure = "tls";
            $this->mailer->Port = intval($this->config["smtp_port"]);
        }
        if (isset($this->config["from"])){
            $name = isset($this->config["from_name"]) ? $this->config["from_name"] : null;
            $this->setFrom($this->config["from"], $name);
        } else $this->setFrom("info@".$_SERVER["HTTP_HOST"]);
        if (isset($this->config["reply_to"])){
            $name = isset($this->config["reply_to_name"]) ? $this->config["reply_to_name"] : null;
            $this->setReplyTo($this->config["reply_to"], $name);
        }
        return $this;
    }

    /**
     * @return \PHPMailer
     */
    public function getMailer(){
        return $this->mailer;
    }

    /**
     * @param $theme
     * @return $this
     */
    public function setTheme($theme){
        $this->tpl_dir = APP_DIR."Themes".DS.$theme;
        return $this;
    }

    /**
     * @param bool|false $set_imitation
     * @return $this
     */
    public function setSendImitation($set_imitation = false)
    {
        $this->imitation = $set_imitation;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastMailHtml()
    {
        return $this->lastMailHtml;
    }

    /**
     * @param $tpl_file
     * @return $this
     * @throws FileException
     */
    public function setTemplate($tpl_file)
    {
        if (!file_exists($tpl_file)) {
            if (file_exists($this->tpl_dir.DS.$tpl_file)) {
                $this->template = $this->tpl_dir.DS.$tpl_file;
                return $this;
            }
            throw new FileException("Mailer: template $tpl_file not exists!");
        }
        $this->template = $tpl_file;
        return $this;
    }

    /**
     * @param $array
     * @return $this
     */
    public function addVars($array)
    {
        foreach ($array as $k => $v) {
            $this->vars[$k] = $v;
        }
        return $this;
    }

    public function getVars(){
        return $this->vars;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addVar($key, $value)
    {
        $this->vars[$key] = $value;
        return $this;
    }

    public function getVar($key){
        return $this->vars[$key];
    }

    /**
     * @return $this
     */
    public function resetVars()
    {
        $this->vars = [];
        return $this;
    }

    /**
     * @return $this
     */
    public function resetAttachements()
    {
        $this->mailer->clearAttachments();
        return $this;
    }

    /**
     * @param $file
     * @param string $name
     * @return $this
     * @throws FileException
     * @throws \Exception
     * @throws \phpmailerException
     */
    public function addAttachement($file, $name = '')
    {
        if (!file_exists($file)) {
            throw new FileException("Mailer: attachement file $file not exists!");
        }
        if (!$name) $name = basename($file);
        if ($this->mailer->addAttachment($file, $name))
            return $this;
        else throw new FileException("Mailer: attachement file $file not assigned to mailer!");
    }

    /**
     * @param $imageFile
     * @param string $cid
     * @return $this
     * @throws FileException
     */
    public function addEmbeedImage($imageFile, $cid = ''){
        if (!file_exists($imageFile)) {
            throw new FileException("Mailer: image file $imageFile not exists!");
        }
        if (!$cid) $cid = basename($imageFile);
        if ($this->mailer->addEmbeddedImage($imageFile, $cid))
            return $this;
        else throw new FileException("Mailer: image file $imageFile not assigned to mailer!");
    }

    /**
     * @param $fromMail
     * @param string $fromName
     * @return $this
     */
    public function setFrom($fromMail, $fromName = '')
    {
        $this->from = ["mail" => $fromMail, "name" => $fromName];
        return $this;
    }

    public function getFrom(){
        return $this->from;
    }

    /**
     * @param $mail
     * @param string $name
     * @return $this
     */
    public function setReplyTo($mail, $name = '')
    {
        $this->reply_to = ["mail" => $mail, "name" => $name];
        return $this;
    }

    public function getReplyTo(){
        return $this->reply_to;
    }

    /**
     * @param $mail
     * @param string $name
     * @return $this
     */
    public function addCopyTo($mail, $name=''){
        $this->mailer->addCC($mail, $name);
        return $this;
    }

    /**
     * @param $mail
     * @param string $name
     * @return $this
     */
    public function addHiddenCopyTo($mail, $name=''){
        $this->mailer->addBCC($mail, $name);
        return $this;
    }

    /**
     * @param $html_smarty_string
     * @return $this
     */
    public function setHtml($html_smarty_string)
    {
        $this->_html = $html_smarty_string;
        return $this;
    }

    public function getHtml(){
        return $this->_html;
    }

    /**
     * @param $css
     * @return $this
     */
    public function setCss($css)
    {
        if (file_exists($css)) {
            $this->css[] = file_get_contents($css);
        } else $this->css[] = $css;
        return $this;
    }

    public function getSendTo(){
        return $this->sendTo;
    }

    public function getSubject(){
        return $this->subject;
    }

    /**
     * @param $subject
     * @param $toEmail
     * @param string $toName
     * @param string $alternateBody
     * @param bool|false $disableHtml
     * @return bool
     * @throws FileException
     * @throws \Exception
     * @throws \SmartyException
     * @throws \phpmailerException
     */
    public function send($subject, $toEmail, $toName = '', $alternateBody = '', $disableHtml = false)
    {
        $this->subject = $subject;
        $this->sendTo = ["mail" => $toEmail,"name"=>$toName];
        if (!self::gc()->getValidator()->isEmail($toEmail)) {
            $this->errors = "Not valid mail";
            return false;
        }
        self::gc()->getHooks()->execute("mailer", "before_send", $this);
        if (!$this->_html)
            $this->setTemplate($this->template); // fix, if template not exist or !isset - throw exception
        $smarty = self::gc()->getView()->newInstance();
        $smarty->setTemplateDir($this->tpl_dir);
        $smarty->right_delimiter = "}";
        $smarty->left_delimiter = "{";
        foreach ($this->vars as $k => $v) {
            $smarty->assign($k, $v);
        }
        if ($this->_html)
            $html = $smarty->fetch("string:" . $this->_html);
        else $html = $smarty->fetch($this->template);
        if (is_array($this->from)) {
            if (isset($this->from["mail"]))
                $this->mailer->From = $this->from["mail"];
            if (isset($this->from["name"]))
                $this->mailer->FromName = $this->from["name"];
        }
        if (is_array($this->reply_to) && isset($this->reply_to["mail"]) && isset($this->reply_to["name"])) {
            $this->mailer->addReplyTo($this->reply_to["mail"], $this->reply_to["name"]);
        }
        $this->mailer->addAddress($toEmail, $toName);
        $this->mailer->Subject = $subject;
        $this->mailer->Body = $html;
        if ($alternateBody != '')
            $this->mailer->AltBody = $alternateBody;
        $this->mailer->isHTML(!$disableHtml);
        if ($this->imitation)
            $result = true;
        else
            $result = $this->mailer->send();
        if (!$result) $this->errors = $this->mailer->ErrorInfo;
        self::gc()->getHooks()->execute("mailer", "on_send", ["email" => $toEmail, "name" => $toName, "html" => $html, "subject" => $subject]);
        if ($result) self::gc()->getHooks()->execute("mailer", "on_send_ok", ["email" => $toEmail, "name" => $toName, "html" => $html, "subject" => $subject]);
        else         self::gc()->getHooks()->execute("mailer", "on_send_err", ["email" => $toEmail, "name" => $toName, "html" => $html, "subject" => $subject]);
        self::gc()->getHooks()->execute("mailer", "after_send", ["email" => $toEmail, "name" => $toName, "html" => $html, "subject" => $subject]);
        $this->lastMailHtml = $html;
        return $result;
    }

    /**
     * @param $subject
     * @param array $receivers
     * @param string $alternateBody
     * @param bool|false $disableHtml
     * @return array
     */
    public function sendMultiple($subject, $receivers = [], $alternateBody = '', $disableHtml = false)
    {
        self::gc()->getHooks()->execute("mailer", "on_multiple", func_get_args());
        $ok = 0;
        $e = 0;
        foreach ($receivers as $r) {
            if (!isset($r["mail"])) {
                if (isset($r["email"]))
                    $r["mail"] = $r["email"];
                else {
                    $e++;
                    continue;
                }
            }
            if (!isset($r["name"])) $r["name"] = '';
            if ($this->send($subject, $r["mail"], $r["name"], $alternateBody, $disableHtml))
                $ok++;
            else $e++;
        }
        return ["ok" => $ok, "err" => $e, "result" => $e == 0];
    }

    /**
     * @return string
     */
    public function getErrorInfo(){
        return $this->errors."";
    }

}