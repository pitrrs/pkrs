<?php
/********************************************
 *
 * BasePresenter.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Presenters;

use PKRS\Core\Components\BaseComponent;
use PKRS\Core\Components\CoreComponents\Pagination;
use PKRS\Core\Components\CoreComponents\PaginationRouter;
use PKRS\Core\Config\Config;
use PKRS\Core\Exception\FileException;
use PKRS\Core\Exception\MainException;
use PKRS\Core\Headers\Redirects;
use PKRS\Core\Service\Service;
use PKRS\Core\Service\ServiceContainer;
use PKRS\Core\User\Messages;
use PKRS\Core\User\User;
use Smarty;

/**
 * Class BasePresenter
 * @package PKRS\Core\Presenters
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
abstract class BasePresenter extends Service implements IBasePresenter
{
    /**
     * @var Smarty
     */
    var $smarty;

    /**
     * template file
     * @var string
     */
    var $template;

    /**
     * Theme directory
     * Can be
     * A) FULL path to dir
     * B) only dirname in APP_DIR/Themes/
     * @var string
     */
    var $theme;

    /**
     * Is protected presenter?
     * @var bool
     */
    var $needLogin = false;

    /**
     * @var Config
     */
    var $config;

    /**
     * Page title
     * @var string
     */
    var $pageTitle = "";

    /**
     * Used JS modules
     * @var array
     */
    var $jsModules =  [];

    /**
     * JS to bind to template
     * @var array
     */
    protected $headerJs =  [];

    /**
     * CSS to bind to template
     * @var array
     */
    protected $headerCss =  [];

    /**
     * @var null|User
     */
    protected $user = null;

    /**
     * If user NOW loged -> this be TRUE
     * @var bool
     */
    protected $now_logged = false;

    /**
     * Created forms on page
     * @var array
     */
    protected $forms =  [];

    /**
     * Assigned components to page
     * @var array
     */
    protected $components = [];

    /**
     * Now route name
     * @var string
     */
    var $route;

    /**
     * @var ServiceContainer
     */
    var $gc;

    /**
     * Params from route
     * @var array
     */
    var $params = [];

    /**
     * @param array $params
     * @return BasePresenter|bool
     */
    public static function _create(array $params = []){
        try{
            return new static($params);
        } catch (\Exception $e){
            self::handleError($e);
        }
        return false;
    }

    /**
     * @param \Exception $e
     * @throws MainException
     */
    public static function _handleError(\Exception $e = null){
        throw new MainException($e->getMessage(), $e->getCode(), $e);
    }

    /**
     * @param array $params
     * @throws MainException
     */
    public function __construct(array $params = [])
    {
        $this->gc = self::gc();
        self::gc()->getHooks()->execute("presenters", "on_create");
        $this->smarty = self::gc()->getView()->smarty();
        $this->config = self::gc()->getConfig();
        $this->route = self::gc()->getRouter()->match()["name"];
        $this->params = $params;
        $need_login = $this->needLogin();
        $this->user = self::gc()->getUser();
        if (!is_null($need_login))
            $this->needLogin = boolval($need_login);
        if ($this->needLogin) {
            if (method_exists($this->user, "checkLogin")) {
                $this->now_logged = $this->user->checkLogin();
                if ($this->user->logged)
                    $this->smarty->assign("user", $this->user->getRow());
                else {
                    if (defined("IS_AJAX") && IS_AJAX == true) {
                        echo $this->config->getConf("ajax","login_err_message");
                        exit;
                    }
                    $login_target = $this->config->getConf("application","login_redirect", "/login");
                    if ($this->config->getConf("application","login_redirect_use_return", false)){
                        $return_param = $this->config->getConf("application","login_redirect_return_param", "return");
                        $login_target.="?".$return_param."=".$_SERVER["REQUEST_URI"];
                    }
                    Redirects::redirect($login_target);
                }
            } else throw new MainException("Method checkLogin() not exists in user class");
        } else {
            if (method_exists($this->user, "checkLogin")) {
                $this->now_logged = $this->user->checkLogin();
            } else throw new MainException("Method checkLogin() not exists in user class");
        }
        $theme = $this->setTheme();
        if (!is_null($theme))
            $this->theme = $theme;
        $this->afterSetTheme();
        $this->jsModules = $this->getAvailableJsModules();
        $this->addComponent(new PaginationRouter());
        $this->addComponent(new Pagination());
    }

    /**
     * @function: getLink
     * @since: 0.1.5
     * @description: Reversed routing
     * @param $name
     * @param array $params
     * @return string
     */
    function getLink($name, array $params = []){
        return self::gc()->getRouter()->generate($name, $params);
    }

    /**
     * @param $path
     * @param null $code
     */
    final function redirect($path, $code = null)
    {
        Redirects::redirect($path, $code);
    }


    /**
     * @param $name
     * @param array $params
     * @param null $code
     */
    function redirectToLink($name, array $params = [], $code = null){
        $this->redirect($this->getLink($name, $params), $code);
    }

    /**
     * Shorthand for messages
     * @param string $message
     * @param string $type
     * @throws MainException
     */
    function flashMessage($message, $type=Messages::TYPE_OK){
        self::gc()->getMessages()->add($message, $type);
    }

    /**
     * @return array
     */
    final public function getAvailableJsModules()
    {
        $modules = [];
        $search_dir = APP_DIR . "Modules" . DS . "JSModules" . DS;
        if (!defined("JS_MODULES_PATH"))
            define("JS_MODULES_PATH","/app/Modules/JSModules/"); // TODO: Dynamic
        if (is_dir($search_dir)) {
            $search = (array)@glob($search_dir . "*", GLOB_ONLYDIR);
            foreach ($search as $dir) {
                $dir = str_replace($search_dir, '', $dir);
                if (file_exists($search_dir . $dir . DS . "_load.csv")) {
                    $load = file($search_dir . $dir . DS . "_load.csv");
                    if (!empty($load)) {
                        $add = ["css" => [], "js" => []];
                        foreach ($load as $l) {
                            $c = explode(";", $l);
                            if (is_array($c) && isset($c[1]) && in_array($c[0], array_keys($add))) {
                                $add[$c[0]][] = JS_MODULES_PATH . $dir . "/" . $c[1];
                            }
                        }
                        $modules[$dir] = $add;
                    }
                }
            }
        }
        return $modules;
    }

    /**
     * @param BaseComponent $component
     */
    protected function addComponent(BaseComponent $component){
        $this->components[] = $component;
        self::gc()->getView()->registerPlugin("function", $component->name, [$component,"fullRender"]);
    }

    /**
     * @param $tpl_output
     * @return mixed
     */
    final public static function minifyHtml($tpl_output)
    {
        $tpl_output = preg_replace('![\t ]*[\r\n]+[\t ]*!', '', $tpl_output);
        return $tpl_output;
    }

    final function privatePostConstruct()
    {
        $this->postConstruct();
    }

    private function afterSetTheme()
    {
        if (!defined("JS_PATH")) {
            define("JS_PATH", THEMES_PATH . $this->theme . DS . "js".DS);
        }
        if (!defined("CSS_PATH")) {
            define("CSS_PATH", THEMES_PATH . $this->theme . DS . "css".DS);
        }
        $theme_path = THEMES_PATH. $this->theme;
        if (substr($this->theme,0,1)==DS){
            $theme_path = "/".str_replace(ROOT_DIR, "",$this->theme)."/";
        }
        $theme_path = "/".trim(str_replace("//","/",$theme_path),"/")."/";
        $this->smarty->assign("TPL_PATH",  str_replace("//","/",$theme_path . "/"));
    }

    function postConstruct()
    {

    }

    /**
     * @param $title
     */
    final public function pageTitle($title)
    {
        $this->pageTitle = $title;
    }

    /**
     * @param $moduleName
     * @throws FileException
     * @throws MainException
     */
    final public function addJsModule($moduleName)
    {
        if (in_array($moduleName, array_keys($this->jsModules))) {
            foreach ($this->jsModules[$moduleName]["css"] as $css) {
                $this->addCss(trim($css));
            }
            foreach ($this->jsModules[$moduleName]["js"] as $js) {
                $this->addJs(trim($js));
            }
        } else {
            throw new MainException("JS Module $moduleName not exists!");
        }
    }

    /**
     * @param $cssFile
     * @throws FileException
     */
    public function addCss($cssFile)
    {
        $cssFile = trim($cssFile);
        if (substr($cssFile, 0, 4) == "http" || substr($cssFile, 0, 1) == "/") {
            if (!in_array($cssFile, $this->headerCss))
                $this->headerCss[] = $cssFile;
        } else if (file_exists(ROOT_DIR . CSS_PATH . $cssFile)) {
            if (!in_array(CSS_PATH . $cssFile, $this->headerCss))
                $this->headerCss[] = CSS_PATH . $cssFile;
        } else throw new FileException("CSS File $cssFile not exists in path " . CSS_PATH);

    }

    /**
     * @param $jsFile
     * @throws FileException
     */
    public function addJs($jsFile)
    {
        $jsFile = trim($jsFile);
        if (substr($jsFile, 0, 4) == "http" || substr($jsFile, 0, 1) == "/") {
            if (!in_array($jsFile, $this->headerJs))
                $this->headerJs[] = $jsFile;
        } else if (file_exists(ROOT_DIR . JS_PATH . $jsFile)) {
            if (!in_array(JS_PATH . $jsFile, $this->headerJs))
                $this->headerJs[] = JS_PATH . $jsFile;
        } else throw new FileException("JS File $jsFile not exists in path " . JS_PATH);
    }

    final public function display()
    {
        if (self::gc()->getConfig()->getConf("application", "consolidate_scripts") == "true") {
            if (!empty($this->headerCss))
                $this->headerCss = [APP_WEB_PATH."?__load_consolidated_=css&__files_=" . implode(",", $this->headerCss)];
            if (!empty($this->headerJs))
                $this->headerJs = [APP_WEB_PATH."?__load_consolidated_=js&__files_=" . implode(",", $this->headerJs)];
        }
        $this->smarty->assign("pageTitle", $this->pageTitle);
        $this->smarty->assign("headerCss", $this->headerCss);
        $this->smarty->assign("headerJs", $this->headerJs);

        $this->smarty->assign("user", $this->user ? $this->user->data : []);
        $this->smarty->assign("messages", Messages::getAllImploded());
        if (defined("DEV") && DEV){
            _d($this->smarty->getTemplateVars(),"Smarty assigned vars");
        }
        $this->smarty->setTemplateDir((substr($this->theme,0,1)!=DS ? THEMES_DIR :"") . $this->theme);
        $tplDir = (substr($this->theme,0,1)!=DS ? THEMES_DIR :"") . $this->theme . DS;
        if (!file_exists($this->template)) // fix absolute path
            $tplFile = $tplDir . $this->template;
        else $tplFile = $this->template;
        if (!is_file($tplFile) || !file_exists($tplFile)) {
            if ($this->template == "" || $this->template == null){
                $this->template = $this->route.".tpl";
                if (is_file($tplDir . $this->template)){
                    $this->_fetch($tplDir . $this->template);
                    return;
                }
            }
            throw new FileException(__CLASS__ . ": Template " . (substr($this->theme,0,1)!=DS ? THEMES_DIR :"") . $this->theme . DS . $this->template . " not exists!");
        } else {
            $this->_fetch($tplFile);
        }
    }

    /**
     * @param $tplFile
     * @throws \Exception
     * @throws \SmartyException
     */
    protected function _fetch($tplFile, $echo = true){
        $output = $this->smarty->fetch($tplFile);
        @ob_get_clean();
        $output = self::gc()->getHooks()->execute("view","on_display_modifier", ["html"=>$output]);
        if ($echo)
            echo $output["html"];
        return $output["html"];
    }

    function beforeDisplay()
    {

    }
}
