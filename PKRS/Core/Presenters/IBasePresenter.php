<?php
/********************************************
 *
 * IBasePresenter.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Presenters;

/**
 * Interface IBasePresenter
 * @package PKRS\Core\Presenters
 */
interface IBasePresenter
{

    /**
     * Set need login with:
     * A) $this->needLogin = true;
     * B) return true;
     *
     * @return null|bool
     */
    function needLogin();

    /*
     * Select used theme directory in this namespace
     * Set theme with:
     * A) $this->theme = "Administration";
     * B) return "Administration";
     *
     * return void|string
     */
    function setTheme();

}