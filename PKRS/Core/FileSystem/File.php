<?php
/**************************************************************
 *
 * File.php, created 16.11.14
 * Since: 0.1.3
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\FileSystem;

use PKRS\Core\Object\Object;

/**
 * Class File
 * @package PKRS\Core\FileSystem
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class File extends Object{

    var $filePath;
    var $handle;
    var $handle_type;

    /**
     * @param $file_path
     */
    public function __construct($file_path){
        $this->filePath = $file_path;
    }

    /**
     * Close file if opened
     */
    public function __destruct(){
        if (is_resource($this->handle))
            $this->close();
    }

    /**
     * Function: exists
     * Since: 0.1.3
     * Description: Check if file exists
     *
     * @return bool
     */
    public function exists(){
        return file_exists($this->filePath);
    }

    /**
     * Function: delete
     * Since: 0.1.3
     * Description: Delete file if exists
     *
     * @return bool
     */
    public function delete(){
        if ($this->exists()){
            return unlink($this->filePath);
        }
        return true;
    }

    /**
     * Function: chmod
     * Since: 0.1.3
     * Description: Change file permission if exists
     *
     * @param string $mode
     * @return bool
     */
    public function chmod($mode = "0644"){
        if (!$this->exists()) return false;
        return chmod($this->filePath, $mode);
    }

    /**
     * Function: write
     * Since: 0.1.3
     * Description: Write content to file
     *
     * @param $content
     * @return bool|int
     */
    public function write($content){
        if (!is_resource($this->handle)) {
            if (!$this->open()){
                return false;
            }
        }
        return fwrite($this->handle, $content);
    }

    /**
     * Function: open
     * Since: 0.1.3
     * Description: Open file for writing
     *
     * @param string $mode
     * @return bool
     */
    public function open($mode = "w+"){
        if (is_resource($this->handle)) $this->close();
        $this->handle = fopen($this->filePath, $mode);
        $this->handle_type = $mode;
        return is_resource($this->handle);
    }

    /**
     * Function: close
     * Since: 0.1.3
     * Description: Close file handle
     *
     * @return bool
     */
    public function close(){
        $res = true;
        if (is_resource($this->handle)) $res = fclose($this->handle);
        $this->handle = null;
        $this->handle_type = "w+";
        return $res;
    }

    /**
     * Function: rename
     * Since: 0.1.3
     * Description: rename or move file
     *
     * @param $newName
     * @param bool $sameDir
     * @param bool $autoCreateTargetDir
     * @return bool
     */
    public function rename($newName, $sameDir = false, $autoCreateTargetDir = false){
         if ($this->exists()){
             if ($sameDir){
                 $file = basename($newName);
                 $dir = dirname($this->filePath);
                 $newName = $dir.DS.$file;
             }
             $target = dirname($newName);
             if (!is_dir($target)){
                 if ($autoCreateTargetDir){
                     if (!mkdir($target)) return false;
                 }
                 else return false;
             }
             if( rename($this->filePath, $newName)){
                 $this->filePath = $newName;
                 return true;
             }
         }
        return false;
    }

    /**
     * Function: move
     * Since: 0.1.3
     * Description: Move file to another directory
     *
     * @param $newTargetDir
     * @param bool $autoCreateTargetDir
     * @return bool
     */
    public function move($newTargetDir, $autoCreateTargetDir = false){
        $filename = basename($this->filePath);
        if (is_dir($newTargetDir)){
            return $this->rename($newTargetDir.DS.$filename);
        } else {
            if ($autoCreateTargetDir){
                return $this->rename($newTargetDir.DS.$filename, false, true);
            } else return false;
        }
    }

    /**
     * Function: content
     * Since: 0.1.3
     * Description: Get file contents
     *
     * @return bool|string
     */
    public function content(){
        if($this->exists()) return file_get_contents($this->filePath);
        else return false;
    }

    /**
     * Function: file
     * Since: 0.1.3
     * Description: Get file contents as array (by line)
     *
     * @return array|bool
     */
    public function file(){
        if ($this->exists()) return file($this->filePath);
        else return false;
    }
}