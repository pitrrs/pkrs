<?php
/********************************************
 *
 * User.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\User;

use PKRS\Core\Exception\MainException;
use PKRS\Core\Model\Model;
use PKRS\Core\Service\Service;

/**
 * Class User
 * @package PKRS\Core\User
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class User extends Service
{

    private static $self;
    var $config;
    var $data = [];
    var $session_key=null;
    var $logged = false;
    var $model = null;
    var $db;

    /**
     * @param Model $user_model
     * @throws MainException
     */
    function __construct(Model $user_model)
    {
        if (!$user_model instanceof Model)
            throw new MainException("User: User model is not instance of Model");
        $this->config = self::gc()->getConfig();
        $this->session_key = $this->config->getConf("user", "session_key");
        $this->model = $user_model;
        $this->db = self::gc()->getDatabase();
        $session = self::gc()->getSession();
        if (!$session::start($this->config)){
            throw new MainException("Session cant not start");
        }
        self::$self = $this;
    }

    /**
     * Function: getInstance
     * Since: 0.1.4
     * Description: Get instance of this class
     *
     * @return null|User
     */
    public static function getInstance()
    {
        if (self::$self instanceof User) {
            return self::$self;
        } else {
            return null;
        }
    }

    /**
     * Function: checkLogin
     * Since: 0.1.4
     * Description: If user NOW logged return TRUE else return false
     *
     * @return bool
     * @throws \PKRS\Core\Exception\MainException
     */
    function checkLogin()
    {
        if (!self::gc()->getSession()->start($this->config)){
            throw new MainException("Session cant not start");
        }
        $data = null;
        $new_login = false;
        if (!isset($_SESSION["_PKRS_LOGIN_ATTEMPT"]))
            $_SESSION["_PKRS_LOGIN_ATTEMPT"] = 0;
        if (!isset($_SESSION["_PKRS_LOGIN_ATTEMPT_TIME"]))
            $_SESSION["_PKRS_LOGIN_ATTEMPT_TIME"] = time();
        $form = self::gc()->getForm($this->config->getConf("user", "login_form_identifier_value"), $this->config->getConf("user", "login_form_identifier"));
        if (trim($this->config->getConf("user", "login_form_target_route")) != "")
            $form->setTarget($this->config->getConf("user", "login_form_target_route"));
        $form->addItem($form::I_TEXT,$this->config->getConf("user", "login_form_login_field"),false,$this->config->getConf("user", "login_form_login_label"));
        $form->addItem($form::I_PASSWORD,$this->config->getConf("user", "login_form_pass_field"),false, $this->config->getConf("user", "login_form_pass_label"));
        $result = $form->doProcess($this->config->getConf("user", "login_form_submit_label"));
        if ($result){
            $result = $result->getAll();
            $_SESSION["_PKRS_LOGIN_ATTEMPT"] = $_SESSION["_PKRS_LOGIN_ATTEMPT"]+1;
            $_SESSION["_PKRS_LOGIN_ATTEMPT_TIME"] = time();
            if ($this->config->getConf("user","use_password_salt") == true) // login with salt
                $this->model->loadBySearchTerms(["login"=>$result[$this->config->getConf("user", "login_form_login_field")],"pass"=>sha1( $this->config->getConf("user","password_salt"). $result[$this->config->getConf("user", "login_form_pass_field")])]);
            else
                $this->model->loadBySearchTerms(["login"=>$result[$this->config->getConf("user", "login_form_login_field")],"pass"=>sha1($result[$this->config->getConf("user", "login_form_pass_field")])]);
            $data = $this->model->getAll();
            if (!$data) {
                self::gc()->getMessages()->add(
                    $this->config->getConf("user", "login_form_message_not_valid_translate")
                        ? self::gc()->getLang()->xlate($this->config->getConf("user", "login_form_message_not_valid_string"))
                        : $this->config->getConf("user", "login_form_message_not_valid_string")
                    , "err");
            } else {
                $_SESSION["_PKRS_LOGIN_ATTEMPT"] = 0;
                $_SESSION["_PKRS_LOGIN_ATTEMPT_TIME"] = 0;
                $new_login = true;
            }
        } else {
            // neni odeslany _post zkusim session
            if (isset($_SESSION[$this->session_key]) && is_array($_SESSION[$this->session_key]) && isset($_SESSION[$this->session_key]["login"]) && isset($_SESSION[$this->session_key]["pass"])) {
                if ($this->model->loadBySearchTerms(["login"=>$_SESSION[$this->session_key]["login"],"pass"=>$_SESSION[$this->session_key]["pass"]])){
                    $data = $this->model->getAll();
                }
            }
        }
        if (!empty($data) && is_array($data)) {
            $this->data = $data;
            if ($this->blocked()) {
                $_SESSION[$this->session_key] = [];
                $this->data = null;
                $this->logged = false;
                self::gc()->getHooks()->execute("user", "on_login_err");
            } else {
                $_SESSION[$this->session_key] = $data;
                $this->logged = true;
                $_SESSION["_PKRS_LOGIN_ATTEMPT"] = 0;
                $_SESSION["_PKRS_LOGIN_ATTEMPT_TIME"] = 0;
                self::gc()->getHooks()->execute("user", "on_login_ok", $data);
            }
        } else {
            $_SESSION[$this->session_key] = [];
            $this->data = null;
            $this->logged = false;
            self::gc()->getHooks()->execute("user", "on_login_err");
        }
        return $new_login;
    }

    /**
     * Function: blocked
     * Since: 0.1.4
     * Description: If setted user BLOCK (disabling) field, check if is blocked. Return true if blocked
     *
     * @return bool
     */
    function blocked()
    {
        if (isset($this->data[$this->config->getConf("user", "user_blocked_field")]) && $this->data[$this->config->getConf("user","user_blocked_field")] == $this->config->getConf("user", "user_blocked_field_value")) return true;
        else return false;
    }

    /**
     * Function: logout
     * Since: 0.1.4
     * Description: Static destroy user session. Return true if destroyed
     *
     * @return bool
     */
    public function logout()
    {
        $_SESSION[self::gc()->getConfig()->getConf("user", "session_key")] = [];
        $this->logged = false;
        $this->data = null;
        return empty($_SESSION[self::gc()->getConfig()->getConf("user", "session_key")]);
    }

    /**
     * Function: uid
     * Since: 0.1.4
     * Description: Get user ID by model primary key
     *
     * @return int
     */
    function uid()
    {
        if ($this->logged && $this->model->ID !== null)
            return intval($this->model->ID);
        else return 0;
    }

    /**
     * Function: getRow
     * Since: 0.1.3
     * Description: Get user data
     *
     * @return array
     */
    function getRow()
    {
        if ($this->logged)
            return $this->model->getAll();
        else return null;
    }

}