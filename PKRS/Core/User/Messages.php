<?php
/********************************************
 *
 * Messages.php, created 8.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\User;

use PKRS\Core\Exception\MainException;
use PKRS\Core\Lang\Lang;
use PKRS\Core\Service\Service;
use PKRS\Core\Session\Session;

/**
 * Class Messages
 * @package PKRS\Core\User
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Messages extends Service
{

    const TYPE_OK = "ok";

    const TYPE_ERR = "err";

    const TYPE_WARN = "warn";

    /**
     * @param Session $session
     * @throws MainException
     */
    public function __construct(Session $session){
        $session->start(self::gc()->getConfig());
    }

    /**
     * types = ok | err | warn
     *
     * @param string $message
     * @param string $type
     * @return bool
     * @throws MainException
     */
    public static function add($message, $type = "ok")
    {
        if (!$message || !is_string($message)) throw new MainException("Messages: Message is empty or is not string");
        if (!isset($_SESSION)) throw new MainException("Messages: Session not started");
        if (trim($message) == "") throw new MainException("Messages: Message is empty");
        if (!in_array($type, [self::TYPE_OK, self::TYPE_ERR, self::TYPE_WARN]))
            throw new MainException("Message type has been only ok, err and warn");
        if (!isset($_SESSION["messages"])) $_SESSION["messages"] = [];
        if (!isset($_SESSION["messages"][$type]) || !is_array($_SESSION["messages"][$type])) $_SESSION["messages"][$type] = [];
        if (!in_array($message, $_SESSION["messages"][$type]))
            return ($_SESSION["messages"][$type][] = $message) == $message;
        return false;
    }

    /**
     * @param string $glue
     * @return array
     * @throws MainException
     */
    public static function getAllImploded($glue = "<br><br>")
    {
        if (!$_SESSION) Session::start(self::gc()->getConfig());
        if (!isset($_SESSION["messages"])) return [];
        $arr = [];
        if (isset($_SESSION["messages"][self::TYPE_OK])   && !empty($_SESSION["messages"][self::TYPE_OK]))   $arr[self::TYPE_OK]   = implode($glue, $_SESSION["messages"][self::TYPE_OK]);
        if (isset($_SESSION["messages"][self::TYPE_ERR])  && !empty($_SESSION["messages"][self::TYPE_ERR]))  $arr[self::TYPE_ERR]  = implode($glue, $_SESSION["messages"][self::TYPE_ERR]);
        if (isset($_SESSION["messages"][self::TYPE_WARN]) && !empty($_SESSION["messages"][self::TYPE_WARN])) $arr[self::TYPE_WARN] = implode($glue, $_SESSION["messages"][self::TYPE_WARN]);
        self::reset();
        return $arr;
    }

    /**
     * @param $message
     * @param string $type
     * @param null $_params
     * @return bool
     * @throws MainException
     */
    public static function addForTranslateWithParams($message, $type = "ok", $_params = null){
        $pars = func_get_args();
        unset($pars[0]);
        unset($pars[1]);
        $give = [$message];
        foreach($pars as $p) $give[] = $p;
        $mess = call_user_func_array(["\\PKRS\\Core\\Lang\\Lang","xlate"],$give);
        return self::add($mess, $type);
    }

    /**
     * @param $message
     * @param string $type
     * @throws MainException
     * @throws \PKRS\Core\Exception\LangException
     */
    public static function addForTranslateSimple($message, $type = "ok")
    {
        $message = Lang::xlate($message);
        if (!in_array($type, [self::TYPE_OK, self::TYPE_ERR, self::TYPE_WARN]))
            throw new MainException("Message type has been only ok, err and warn");
        if (!isset($_SESSION["messages"])) $_SESSION["messages"] = [];
        if (!isset($_SESSION["messages"][$type])) $_SESSION["messages"][$type] = [];
        if (!in_array($message, $_SESSION["messages"][$type]))
            $_SESSION["messages"][$type][] = $message;
    }

    /**
     * @param $type
     * @param string $glue
     * @return string
     * @throws MainException
     */
    public static function getAsString($type, $glue = "<br>")
    {
        if (!in_array($type, [self::TYPE_OK, self::TYPE_ERR, self::TYPE_WARN]))
            throw new MainException("Message type has benn only ok, err and warn");
        if (!isset($_SESSION["messages"])) return "";
        if (!isset($_SESSION["messages"][$type])) return "";
        return implode($glue, $_SESSION["messages"][$type]);
    }

    public static function reset()
    {
        $_SESSION["messages"] = [
            self::TYPE_ERR => [],
            self::TYPE_OK => [],
            self::TYPE_WARN => []
        ];
    }

    /**
     * @param $type
     * @return bool
     */
    public static function hasMessages($type)
    {
        return !empty($_SESSION["messages"][$type]);
    }


}