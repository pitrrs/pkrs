<?php
/**************************************************************
 *
 * Example.php, created 20.1.15
 * Since: 0.1.4
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\User\Model;

use PKRS\Core\Model\Model;

/**
 * Class Example
 * @package PKRS\Core\User\Model
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Example extends Model{
    protected $table = "users";
    protected $primary = [
        'name' => 'user_id',
        'type' => self::INT,
    ];
    protected $structure = [
        'login' =>
            [
                'type' => self::VARCHAR,
                'size' => 64,
            ],
        'pass' =>
            [
                'type' => self::VARCHAR,
                'size' => 128,
            ],
        'mail' =>
            [
                'type' => self::VARCHAR,
                'size' => 128,
            ],
    ];
    protected $htmlspecialchars = true;

    // Setter login
    /**
     * @param $value
     * @return $this
     */
    function set_login($value){
        $this->set("login", $value);
        return $this;
    }
    // Getter login
    /**
     * @return bool|null
     */
    function get_login(){
        return $this->get("login");
    }
    // Setter pass
    /**
     * @param $value
     * @return $this
     */
    function set_pass($value){
        $this->set("pass", $value);
        return $this;
    }
    // Getter pass
    /**
     * @return bool|null
     */
    function get_pass(){
        return $this->get("pass");
    }
    // Setter mail
    /**
     * @param $value
     * @return $this
     */
    function set_mail($value){
        $this->set("mail", $value);
        return $this;
    }
    // Getter mail
    /**
     * @return bool|null
     */
    function get_mail(){
        return $this->get("mail");
    }

}