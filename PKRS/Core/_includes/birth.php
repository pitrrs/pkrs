<?php
/********************************************
 *
 * birth.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
@ob_start();
if (!defined("APP_START"))
    define("APP_START", microtime(true));
if (defined("DEV") && DEV === true) {
    error_reporting(E_ALL);
    ini_set("display_error", 1);
} else {
    error_reporting(false);
    ini_set("display_error", 0);
}
// Maintenance
if (defined("MAINTENANCE") && MAINTENANCE === true) {
    die("Maintenance mode. Please try again later.");
}
// autoload app dir
/**
 * @param $class
 */
function pkrsAutoloader($class)
{
    $filename = APP_DIR .DIRECTORY_SEPARATOR. str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    if (file_exists($filename)) include($filename);
}
spl_autoload_register('pkrsAutoloader');
include(dirname(__FILE__) . DS . "phpCompatibility.php");
mb_internal_encoding("UTF-8");
/**
 * @param $err
 * @param $str
 * @param $file
 * @param $line
 * @param $context
 * @return bool
 */
function pkrsErrorHandler($err, $str, $file, $line, $context)
{
    if (!IS_AJAX) \PKRS\Core\Debug\Debug::handler($err, $str, $file, $line, $context);
    else \PKRS\Core\Debug\Debug::handlerAjax($err, $str, $file, $line, $context);
    return true;
}


/**
 * Send to debug
 *
 * @param $var
 * @param $name
 * @param bool $extra
 */
function _d($var, $name = null, $extra = false)
{
    if ($name === null) {
        $name = "Undefined variable";
    }
    \PKRS\Core\Debug\Debug::addDump($name, $var, !!$extra);
}

/**
 * Function: _l
 * Since: 0.1.4
 * Description: Log messages to file
 *
 * @param $file_line
 * @param null $name
 * @return void
 */
function _l($file_line, $name = null){
    $h = fopen(APP_DIR."PKRS_log.txt","a");
    fwrite($h, date("d.m.y H:i:s")." - ".($name ? $name." - ":"").round(memory_get_usage(true)/1024/1024,2)." MB - ".$file_line.PHP_EOL);
    fclose($h);
}

