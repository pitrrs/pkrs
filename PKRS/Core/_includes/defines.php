<?php
/********************************************
 *
 * defines.php, created 5.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
define("DS", DIRECTORY_SEPARATOR);
// Application directories
define("ROOT_DIR", dirname($_SERVER["SCRIPT_FILENAME"]).DS);
define("INCLUDE_DIR", PKRS_DIR . "Core" . DS . "_includes" . DS);
define("WEB_PATH",str_replace(ROOT_DIR, "",$_SERVER["DOCUMENT_ROOT"]));
define("THEMES_DIR", APP_DIR . "Themes" . DS);

define("APP_PATH", realpath(dirname($_SERVER["SCRIPT_FILENAME"]) . "/"));
define("APP_WEB_PATH", str_replace("//","/","/" . implode("/", explode(DS, $_SERVER["SCRIPT_NAME"], -1)) . "/"));
define("THEMES_PATH", "/".trim(str_replace("\\","/",str_replace(APP_PATH,"",APP_DIR)),"/")."/Themes/" );
define("PKRS_WEB_PATH", "/".str_replace(ROOT_DIR,"",PKRS_DIR));
// detect jQuery ajax
define("IS_AJAX", (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == "XMLHttpRequest"));