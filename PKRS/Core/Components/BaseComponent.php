<?php
/**************************************************************
 *
 * BaseComponent.php
 * Since: 0.1.5 (01.2015)
 *
 * Copyright (C) 2014 - 2015 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Components;

use PKRS\Core\Service\Service;

/**
 * Class BaseComponent
 * @package PKRS\Core\Components
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
abstract class BaseComponent extends Service implements IComponent{

    var $smarty, $name, $db, $user;
    var $customTpl;

    var $route;

    protected $reflection;

    /**
     * @param null $name
     */
    final function __construct($name = null){
        $this->route = self::gc()->getRouter()->match()["name"];
        $this->db = self::gc()->getDatabase();
        $this->user = self::gc()->getUser();
        $this->smarty = self::gc()->getView()->newInstance();
        $this->smarty->registered_plugins = self::gc()->getView()->smarty()->registered_plugins; // copy plugins to new instance
        $this->smarty->tpl_vars = self::gc()->getView()->smarty()->tpl_vars; // copy variables to new instance
        $this->reflection = new \ReflectionClass(get_class($this));
        $this->smarty->setTemplateDir(dirname($this->reflection->getFileName()));
        $this->customTpl = $this->reflection->getShortName().".tpl";
        if ($name)
            $this->setName($name);
        else
            $this->setName($this->reflection->getShortName());
    }

    /**
     * @param $componentName
     * @return $this
     */
    public function setName($componentName)
    {
        $this->smarty->assign("component", $componentName);
        $this->name = $componentName;
        return $this;
    }

    /**
     * @param $args
     * @param \Smarty_Internal_Template $template
     * @return string|void
     * @throws \Exception
     * @throws \SmartyException
     */
    public function fullRender($args, \Smarty_Internal_Template $template){
        foreach($args as $k=>$v)
            $this->smarty->assign($k, $v);
        if (isset($args["customTpl"])) {
            if (file_exists(dirname($this->reflection->getFileName()).DS.$args["customTpl"]))
                $this->customTpl = $args["customTpl"];
            else if (file_exists(dirname($this->reflection->getFileName()).DS.$args["customTpl"].".tpl"))
                $this->customTpl = $args["customTpl"].".tpl";
        }
        $result = $this->render($args);
        if ($result == -1) return;
        return $this->smarty->fetch($this->customTpl);
    }

}