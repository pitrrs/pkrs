<?php
namespace PKRS\Core\Components\CoreComponents;


use PKRS\Core\Components\BaseComponent;
use PKRS\Core\Exception\MainException;

class Pagination extends BaseComponent
{

    /**
     * @param $params
     * @return mixed
     */
    public function render($params)
    {
        $p = \PKRS\Core\Database\Pagination::instance($params["name"]);
        if (!$p){
            throw new MainException("Pagination name: ".$params["name"]." not defined");
        }
        $p->toSmarty($this->smarty); // prepise pagina info
    }

}
