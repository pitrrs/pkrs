<?php
namespace PKRS\Core\Components\CoreComponents;


use PKRS\Core\Components\BaseComponent;
use PKRS\Core\Router\Router;
use Tools\PagiInfo;

class PaginationRouter extends BaseComponent
{

    /**
     * @param $params
     * @return mixed
     */
    public function render($params)
    {
        $r = self::gc()->getRouter()->generate($params["route"], $params["params"]);
        $this->smarty->assign("_link_", $r);
    }

}
