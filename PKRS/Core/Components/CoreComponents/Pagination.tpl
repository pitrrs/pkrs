{if $_p_pages > 1}
    <nav>
        <ul class="pagination">
            {if $_p_pageNow > 1}
                <li>
                    <a href="{PaginationRouter params=$_p_route_params route=$_p_route}" aria-label="První">
                        <span aria-hidden="true">První</span>
                    </a>
                </li>
                <li>
                    <a href="{PaginationRouter params=$_p_route_params route=$_p_route}?page={($_p_pageNow-1)}" aria-label="Zpět">
                        <span aria-hidden="true"><<</span>
                    </a>
                </li>
            {/if}
            {if $_p_pages < 15}
                {for $x=1 to $_p_pages}
                    <li {if $x==$_p_pageNow}class="active"{/if}><a href="{PaginationRouter params=$_p_route_params route=$_p_route}{if $x>1}?page={$x}{/if}">{$x}</a></li>
                {/for}
            {elseif $_p_pageNow > 7}
                {for $x=$_p_pageNow-7 to $_p_pageNow+7}
                    {if $x <= $_p_pages}<li {if $x==$_p_pageNow}class="active"{/if}><a href="{PaginationRouter params=$_p_route_params route=$_p_route}?page={$x}">{$x}</a></li>{/if}
                {/for}
            {else}
                {for $x=1 to $_p_pageNow+7}
                    {if $x <= $_p_pages}<li {if $x==$_p_pageNow}class="active"{/if}><a href="{PaginationRouter params=$_p_route_params route=$_p_route}?page={$x}">{$x}</a></li>{/if}
                {/for}
            {/if}
            {if $_p_pageNow < $_p_pages}
                <li>
                    <a href="{PaginationRouter params=$_p_route_params route=$_p_route}?page={($_p_pageNow+1)}" aria-label="Další">
                        <span aria-hidden="true">>></span>
                    </a>
                </li>
                <li>
                    <a href="{PaginationRouter params=$_p_route_params route=$_p_route}?page={$_p_pages}" aria-label="Poslední ({$_p_pages})">
                        <span aria-hidden="true">Poslední ({$_p_pages})</span>
                    </a>
                </li>
            {/if}
        </ul>
    </nav>
{/if}
