<?php
/**************************************************************
 *
 * IComponent.php
 * Since: 0.1.5 (01.2015)
 *
 * Copyright (C) 2014 - 2015 by Petr Klimes & development team
 *
 ***************************************************************
 *
 * Contacts: 
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Components;

/**
 * Interface IComponent
 * @package PKRS\Core\Components
 */
interface IComponent{

    /**
     * @param $params
     * @return mixed
     */
    public function render($params);

}