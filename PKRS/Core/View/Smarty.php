<?php
/**************************************************************
 *
 * Smarty.php, created 4.11.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\View;

use PKRS\Core\Service\Service;

/**
 * Class Smarty
 * @package PKRS\Core\View
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Smarty extends Service
{

    var $smarty;
    static $registeredPlugins = [];

    /**
     * @param \Smarty $smarty
     */
    public function __construct(\Smarty $smarty)
    {
        self::$registeredPlugins = array_merge(self::$registeredPlugins,self::gc()->getHooks()->execute("view","on_create"));
        $this->smarty = $this->newInstance($smarty);
    }

    /**
     * @return \Smarty
     */
    public function smarty()
    {
        return $this->smarty;
    }

    /**
     * Function: registerPlugin
     * Since: 0.1.4
     * Description: Register smarty plugin
     *
     * @param $type (one of: [function, block, modifier, compiler])
     * @param $tag
     * @param $callable
     * @return void
     */
    public function registerPlugin($type, $tag, $callable){
        $this->smarty->registerPlugin($type, $tag, $callable, false);
        self::$registeredPlugins[] = ["type"=>$type, "tag"=>$tag, "callable"=>$callable];
    }

    /**
     * @param \Smarty|null $smarty
     * @return \Smarty
     * @throws \SmartyException
     */
    public function newInstance(\Smarty $smarty = null)
    {
        if (!$smarty instanceof \Smarty)
            $smarty = new \Smarty();
        $smarty->debugging = false;
        $smarty->caching = false;
        $smarty->force_compile = true;
        if (!is_dir(ROOT_DIR .  self::gc()->getConfig()->getConf("application","cache_dir"))) @mkdir(ROOT_DIR .  self::gc()->getConfig()->getConf("application","cache_dir"));
        $smarty->setCompileDir(ROOT_DIR .  self::gc()->getConfig()->getConf("application","cache_dir"));
        $smarty->setCacheDir(ROOT_DIR .  self::gc()->getConfig()->getConf("application","cache_dir"));
        $smarty->registerPlugin('block', 't', ["\\PKRS\\Core\\Lang\\Lang", "translate"], false);
        $smarty->registerPlugin('block', 'a', ["\\PKRS\\Core\\Router\\Router", "reverse_smarty"], false);
        $regPlugins = self::gc()->getHooks()->execute("view","on_smarty_create");
        if (is_array($regPlugins))
            self::$registeredPlugins = @array_merge(self::$registeredPlugins, $regPlugins);
        if (is_array(self::$registeredPlugins)) {
            foreach (self::$registeredPlugins as $p) {
                $smarty->registerPlugin($p["type"], $p["tag"], $p["callable"], false);
            }
        }
        $smarty->registerFilter('variable',[$this, 'escFilter']);
        return $smarty;
    }

    /**
     * @param $content
     * @param $smarty
     * @return string
     */
    function escFilter($content,$smarty) {
        // pouzit {$var nofilter} kdyz nechci filtrovat
        return htmlspecialchars($content,ENT_QUOTES,"UTF-8");
    }

}