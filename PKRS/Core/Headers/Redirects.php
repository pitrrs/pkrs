<?php
/********************************************
 *
 * Redirects.php, created 6.8.14
 *
 * Copyright (C) 2014 by Petr Klimes & development team
 *
 *
 ***************************************************************
 *
 * Contacts:
 * @author: Petr Klimeš <djpitrrs@gmail.com>
 * @url: http://www.pkrs.eu
 * @url: https://github.com/pitrrs/PKRS
 *
 ***************************************************************
 *
 * Compatibility:
 * PHP     v. 5.4 or higher
 * MySQL   v. 5.5 or higher
 * MariaDB v. 5.5 or higher
 *
 **************************************************************/
namespace PKRS\Core\Headers;

use PKRS\Core\Object\Object;

/**
 * Class Redirects
 * @package PKRS\Core\Headers
 *
 * @added ${DATE}
 * @since 0.2.0
 * @author Petr Klimeš <djpitrrs@gmail.com>
 */
class Redirects extends Object
{

    /**
     * @param string $base_path
     * @param string $route
     */
    public static function e404($base_path = "/", $route = "404")
    {
        ob_get_clean();
        self::logRedirect($base_path . $route . " (HTTP code 404)");
        Status::send_code(404);
        header("location:" . $base_path . $route);
        exit();
    }

    /**
     * @param $path
     * @param null $code
     */
    public static function redirect($path, $code = null)
    {
        ob_get_clean();
        self::logRedirect($path.($code ? " (HTTP code ".$code : ""));
        if ($code)
            Status::send_code($code);
        header("location:" . $path,true,$code);
        exit;
    }

    /**
     * @param $path
     */
    private static function logRedirect($path)
    {
        if (!IS_AJAX) {
            if (isset($_SESSION)) {
                if (!isset($_SESSION["redirect_log"]))
                    $_SESSION["redirect_log"] = [];
                $assign = $_SESSION;
                if (isset($assign["redirect_log"])) unset($assign["redirect_log"]);
                $_SESSION["redirect_log"][] = [
                    "from_path" => $_SERVER["REQUEST_URI"],
                    "to_path" => $path,
                    "SERVER" => $_SERVER,
                    "SESSION" => $assign,
                    "POST" => $_POST
                ];
            }
        }
    }

}